const mongoose = require('mongoose');

const ActivitySchema = new mongoose.Schema({
	user: {
		type: mongoose.Types.ObjectId,
		ref: 'user'
	},
	class_id: {
		type: mongoose.Types.ObjectId,
		ref: 'class'
	},
	enrollment: {
		type: mongoose.Types.ObjectId,
		ref: 'enrollment'
	},
	order_id: {
		type: mongoose.Types.ObjectId,
		ref: 'order'
	},
	video_name: {
		type: String
	},
	video_type: {
		type: String,
		enum: ['LIVE', 'RECORDED', 'RECORDED_LIVE']
	},
	event_type: {
		type: String,
		enum: ['WATCH_VIDEO', 'ORDER'],
		default: 'WATCH_VIDEO'
	},
	description: {
		type: String
	},
	picture: {
		type: String
	}
}, {
	timestamps: true
});

let activityModel = mongoose.model('activity', ActivitySchema);

module.exports = activityModel;
