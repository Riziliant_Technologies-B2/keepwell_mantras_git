const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	first_name: {
		type: String,
		required: true
	},
	last_name: {
		type: String
	},
	email: {
		type: String,
		required: true,
		unique: true,
		index: true
	},
	mobile: {
		type: String,
	},
	bio: {
		type: String
	},
	profile_picture: {
		type: String
	},
	gender: {
		type: String,
		enum: ['MALE', 'FEMALE', 'OTHER', 'RATHER_NOT_SAY']
	},
	hash: {
		type: String,
	},
	status: {
		type: String,
		enum: ['REGISTERED', 'VERIFIED', 'SUSPENDED']
	},
	stats: {
		type: [{
			weight_in_kg: Number,
			waist_in_inch: Number,
			date: Date
		}],
		default: []
	},
	initial_weight: {
		type: Number
	},
	initial_waist: {
		type: Number
	},
	weight: {
		type: Number
	},
	waist: {
		type: Number
	},
	emailVerify: {
		_id: false,
		emailCapStartTime: {
			type: Date
		},
		emailsLeft: {
			type: Number
		}
	},
	reset_active: {
		type: Boolean,
		default: false
	},
	admin: {
		type: Boolean,
		default: false
	},
	disabled: {
		type: Boolean,
		default: false
	},
	referral_code: {
		type: String,
		unique: true
	},
	referred_by: {
		type: mongoose.Types.ObjectId
	},
	points: {
		type: Number,
		default: 0
	}
}, {
	timestamps: true
});

let userModel = mongoose.model('user', userSchema);

module.exports = userModel;
