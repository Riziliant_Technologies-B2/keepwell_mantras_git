const mongoose = require('mongoose');

const WebContentSchema = new mongoose.Schema({
	home_headline: {
		type: [String]
	},
});

let webContentModel = mongoose.model('web_content', WebContentSchema);

module.exports = webContentModel;
