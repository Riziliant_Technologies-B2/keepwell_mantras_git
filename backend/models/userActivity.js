const mongoose = require('mongoose');

const ClassSchema = new mongoose.Schema({
	user: {
		type: mongoose.Types.ObjectId,
		ref: 'user'
	},
	activity: [{
		text: {
			type: String
		},
		ref_id: {
			type: mongoose.Types.ObjectId
		},
		_id: false
	}],
	overview: {
		live_classes_watched: {
			type: Number
		},
		recorded_classes_watched: {
			type: Number
		}
	}
}, {
	timestamps: true
});

let classModel = mongoose.model('activity', ClassSchema);

module.exports = classModel;
