const mongoose = require('mongoose');
var moment = require('moment');

const enrollmentSchema = new mongoose.Schema({
	user: {
		type: mongoose.Types.ObjectId,
		required: true,
		ref: 'user'
	},
	order_id: {
		type: mongoose.Types.ObjectId,
		ref: 'order'
	},
	type: {
		type: String,
		enum: ['LIVE', 'RECORDED', '6_WEEKS', '60_MINS']
	},
	category_id: {
		type: mongoose.Types.ObjectId,
		ref: 'category'
	},
	class_count: {
		type: Number,
		default: 0
	},
	classes: {
		type: [{
			_id: {
				type: mongoose.Types.ObjectId,
				ref: 'class',
				required: true
			},
			watch_start: {
				type: Date
			},
			type: {
				type: String,
				enum: ['LIVE', 'RECORDED', 'RECORDED_LIVE']
			}
		}],
		default: []
	},
	files: {
		type: [{
			name: { type: String },
			url: { type: String },
			date: {type: Date, default: new Date()}
		}],
		default: []
	},
	valid_till: {
		type: Date,
		default: moment().add(6, 'week')
	},
	extended: {
		type: Boolean,
		default: false
	},
	call_schedule: {
		type: String,
	},
	details: {
		type: Object
	}
}, {
	timestamps: true
});

let enrollmentModel = mongoose.model('enrollment', enrollmentSchema);

module.exports = enrollmentModel;
