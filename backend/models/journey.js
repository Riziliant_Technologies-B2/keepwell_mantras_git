const mongoose = require('mongoose');

const journeySchema = new mongoose.Schema({
	user: {
		type: mongoose.Types.ObjectId,
		required: true,
		ref: 'user'
	},
	link: {
		type: String
	},
	text: {
		type: String
	}
}, {
	timestamps: true
});

journeySchema.index({ createdAt: 1}, { expireAfterSeconds: 6 * 7 * 24 * 3600 });

let journeyModel = mongoose.model('journey', journeySchema);

module.exports = journeyModel;
