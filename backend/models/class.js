const mongoose = require('mongoose');
const { class_type } = require('../const');

const ClassSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	description: {
		type: String
	},
	date: {
		type: Date
	},
	duration_in_minutes: {
		type: Number
	},
	meeting_link: {
		type: String
	},
	recording_link: {
		type: String
	},
	type: {
		type: String,
		enum: class_type,
		required: true
	},
	category: {
		type: mongoose.Types.ObjectId,
		required: true,
		ref: 'category'
	},
	order: {
		type: Number
	},
	enabled: {
		type: Boolean,
		default: true
	}
}, {
	timestamps: true
});

let classModel = mongoose.model('class', ClassSchema);

module.exports = classModel;
