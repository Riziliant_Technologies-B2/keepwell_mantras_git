const mongoose = require('mongoose');

const ClassSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String,
		required: true
	},
	thumbnail: {
		type: String,
		required: true
	},
	enabled: {
		type: Boolean,
		default: true
	},
	classes: {
		type: [mongoose.Types.ObjectId],
		default: []
	},
	price_per_class: {
		type: Number,
		required: true
	},
	duration: {
		min: {
			type: Number,
			default: 35
		},
		max: {
			type: Number,
			default: 60
		}
	}
}, {
	timestamps: true
});

let classModel = mongoose.model('category', ClassSchema);

module.exports = classModel;
