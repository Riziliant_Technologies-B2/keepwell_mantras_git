const mongoose = require('mongoose');

const live_class_price = parseInt(process.env.LIVE_CLASS_PRICE, 10);

const orderSchema = new mongoose.Schema({
	user: {
		type: mongoose.Types.ObjectId,
		required: true,
		ref: 'user'
	},
	type: {
		type: String,
		enum: ['PURCHASE', 'EXTENSION']
	},
	live_classes: {
		count: {
			type: Number,
			default: 0
		},
		price_per_class: {
			type: Number,
			default: live_class_price
		}
	},
	recorded_classes: {
		type: [{
			category_id: {
				type: mongoose.Types.ObjectId,
				ref: 'category'
			},
			count: {
				type: Number
			},
			price_per_class: {
				type: Number
			}
		}],
		default: []
	},
	diet_package: {
		plan: {
			type: String,
			enum: ['60_MINS', '6_WEEKS']
		},
		price: {
			type: Number,
			default: 0
		},
		details: {
			type: Object,
			default: {}
		}
	},
	extension: {
		enrollment_id: {
			type: mongoose.Types.ObjectId
		},
		total_classes: {
			type: Number
		},
		total_price: {
			type: Number
		},
		title: {
			type: String
		}
	},
	invoice_id: {
		type: String,
		unique: true,
		required: true,
		index: true
	},
	invoice_total: {
		type: Number
	},
	live_class_discount: {
		type: Number,
		default: 0
	},
	recorded_class_discount: {
		type: Number,
		default: 0
	},
	diet_plan_discount: {
		type: Number,
		default: 0
	},
	promo_code: {
		type: mongoose.Types.ObjectId,
		ref: 'promo_code'
	},
	status: {
		type: String,
		enum: ['INITIATED', 'INCOMPLETE', 'COMPLETED', 'FAILED'],
		default: 'INITIATED'
	}
}, {
	timestamps: true
});

let orderModel = mongoose.model('order', orderSchema);

module.exports = orderModel;
