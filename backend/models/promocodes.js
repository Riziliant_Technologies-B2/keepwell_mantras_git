let mongoose = require('mongoose');

let promoSchema = new mongoose.Schema({
	name: {
		type: String,
		index: true,
		unique: true,
		required: true
	},
	discount: {
		type: Number,
		required: true
	},
	valid_from: {
		type: Date,
		default: new Date()
	},
	valid_upto: {
		type: Date
	},
	active: {
		type: Boolean,
		default: true
	},
	min_class_count: {
		type: Number,
		required: true
	},
	max_class_count: {
		type: Number,
		required: true
	},
	class_type: {
		type: String,
		default: 'ANY',
		enum: ['LIVE', 'RECORDED', '6_WEEKS', '60_MINS', 'ANY']
	},
	user: {
		type: mongoose.Types.ObjectId,
		ref: 'user'
	}
}, {
	timestamps: true
});

const promoCodeModel = mongoose.model('promo_code', promoSchema);

module.exports = promoCodeModel;
