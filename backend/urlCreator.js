let crypto = require('crypto');
var base64 = require('base-64');
var log = require('./logs');

module.exports.getURL = (resource, time_in_minutes = 5) => {

	const expires = new Date().valueOf() + time_in_minutes * 60 * 60 * 1000;
	const policy = {
		Statement: [{
			Resource: `${process.env.AWS_CLOUDFRONT_URL}${resource}`,
			Condition: {
				DateLessThan: {
					'AWS:EpochTime': Math.floor(expires.valueOf() / 1000)
				}
			}
		}]
	};
	const data = base64.decode(process.env.AWS_CLOUDFRONT_KEY_BASE64);
	const policy_str = JSON.stringify(policy);
	const plus = /\+/gi;
	const equal = /=/gi;
	const backslash = /\//gi;

	let hmac = crypto.createSign('sha1');

	log.debug(policy_str);

	hmac.update(policy_str);
	hmac.end();
	const signed = hmac.sign(data).toString('base64').replace(plus, '-').replace(equal, '_').replace(backslash, '~');

	return `${process.env.AWS_CLOUDFRONT_URL}${resource}?Expires=${Math.floor(expires.valueOf() / 1000)}&Signature=${signed}&Key-Pair-Id=${process.env.AWS_CLOUDFRONT_KEY_ID}`;

};
