var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var mongoose = require('mongoose');
var helmet = require('helmet');
const jwt = require('jsonwebtoken');

const userModel = require('./models/userModel');
const { error } = require('./response');

var log = require('./logs');

var usersRouter = require('./routes/users');
var mediaRouter = require('./routes/media');
var classesRouter = require('./routes/classes');
var categoryRouter = require('./routes/category');
var enrollmentRouter = require('./routes/enrollment');
var promoCodeRouter = require('./routes/promocode');
var orderRouter = require('./routes/order');
var indexRouter = require('./routes/index');
var activityRouter = require('./routes/activity');

var app = express();

app.use((req, res, next) => {
	console.log('ok');
	next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors({ origin: process.env.APP_HOME, credentials: true }));
app.use(helmet());

mongoose.connect(process.env.DBURL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false })
	.then(() => {
		log.info('mongodb connected successfully');
	})
	.catch(err => {
		log.error('mongodb connection error: ', err);
	});

app.use(async (req, res, next) => {

	try {
		const { jwt: token } = req.cookies;

		if (!token) {
			return next();
		}


		const decoded = jwt.verify(token, process.env.JWT_SECRET);

		const user = await userModel.findById(decoded.payload);

		if (!user) return error(res, 404, 'User not found');
		else if (user.status !== 'VERIFIED') return error(res, 404, 'Please verify email id first');

		log.info(`user id : ${user._id}`);
		req.user = user;

		next();

	} catch (e) {
		log.error(e);
		res.clearCookie('jwt');
		return error(res, 401, 'Invalid token');
	}
});

app.use('/api', indexRouter);
app.use('/api/user', usersRouter);
app.use('/api/media', mediaRouter);
app.use('/api/classes', classesRouter);
app.use('/api/category', categoryRouter);
app.use('/api/enrollment', enrollmentRouter);
app.use('/api/promo_code', promoCodeRouter);
app.use('/api/order', orderRouter);
app.use('/api/activity', activityRouter);
app.use('*', (req, res) => res.redirect(process.env.APP_HOME));

module.exports = app;
