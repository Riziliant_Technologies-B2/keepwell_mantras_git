const { error } = require('./response');

function jwtVerify(req, res, next) {
	if (req.user) next();
	else return error(res, 401, 'Invalid token');

}

module.exports = jwtVerify;
