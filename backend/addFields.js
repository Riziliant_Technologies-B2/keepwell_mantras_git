require('dotenv-safe').config();
var mongoose = require('mongoose');
var userModel = require('./models/userModel');
var randomstring = require('randomstring');
var log = require('./logs');

mongoose.connect(process.env.DBURL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false })
	.then(async (db) => {
		try {
			let users = await userModel.find({});
			log.info('mongodb connected successfully');
			for (let i = 0; i < users.length; i++) {
				let e = users[i];

				if (e.referral_code) continue;

				let referral_code = randomstring.generate(8);
				console.log(referral_code);
				let u = await userModel.findOne({ referral_code });
				while (u) {
					referral_code = randomstring.generate(8);
					console.log(referral_code);
					u = await userModel.findOne({ referral_code });
				}
				e.referral_code = referral_code;
				e.points = 0;
				await e.save();
			}
		} catch (err) {
			log.error(err);
		} finally {
			db.disconnect();
		}
	})
	.catch(err => {
		log.error('mongodb connection error: ', err);
	});
