var aws = require('aws-sdk');
const jwt = require('jsonwebtoken');

var ses = new aws.SES({
	credentials: {
		accessKeyId: process.env.AWS_ACCESS_KEY_ID,
		secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
	},
	region: process.env.AWS_REGION
});

function send(email, subject, data, dataType, source) {
	let body = {};
	if (dataType === 'HTML')
		body = {
			Html: {
				Data: data
			}
		};
	else if (dataType === 'TEXT')
		body = {
			Text: {
				Data: data
			}
		};
	var params = {
		Source: source,
		Destination: {
			ToAddresses: email,
		},
		Message: {
			Subject: { /* required */
				Data: subject
			},
			Body: body
		}
	};

	return ses.sendEmail(params).promise();
}

function verificationEmail(id, toAddress, name) {

	let token_data = {
		payload: id,
		type: 'EMAIL_VERIFICATION'
	};
	let expiry = process.env.VERIFICATION_EMAIL_EXPIRY_IN_MINUTES * 60;
	const token = jwt.sign(token_data, process.env.JWT_SECRET, { expiresIn: expiry });
	let data = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"/><meta http-equiv="X-UA-Compatible" content="IE=edge"/><meta name="viewport" content="width=device-width, initial-scale=1.0"/><title>Verify Email</title><style>html,body{height:100%;width:100%;padding:0;margin:0}</style></head><body style="background-color: #fff; color: #2F4858; "><div style="color: #5e72e4; font-size: 35px; padding: 20px 0; width: 100%;"><div style="border-bottom: 4px #5e72e4 solid; padding: 0 0 10px 10px; margin: 0 20px;"> <img src='https://kwm-user-data.s3.eu-west-2.amazonaws.com/kwm_web/logo-keepwell.png' width="150px" height='auto' /></div></div><div style="padding: 20px; font-size: 20px;"><div style="margin-top: 30px;"> Hello ${name},<br/>Congratulations! Your KeepWell Mantras account has been created successfully.<br/>Please click the button below to verify your email address.<br/><br/>Hurry, This link will expire in 24 hours.</div><div style="display: flex; justify-content: flex-start; cursor: pointer;"><a style="margin: 20px 0; padding: 10px 30px; color: #fff; background-color: #5e72e4; font-size: 20px; text-decoration: none;" href='${process.env.APP_API}/user/verify_email?tok=${token}'> Verify Email </a></div><div>Cheers,<br/>Team KeepWell Mantras</div></div></body></html>`;

	return send([toAddress], '✓ Hurray! Verify your email address and start using', data, 'HTML', process.env.SES_EMAIL_NOREPLY);

}

function welcomeEmail(toAddress, name) {

	let data = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"/><meta http-equiv="X-UA-Compatible" content="IE=edge"/><meta name="viewport" content="width=device-width, initial-scale=1.0"/><title>Verify Email</title><style>html,body{height:100%;width:100%;padding:0;margin:0}</style></head><body style="background-color: #fff; color: #2F4858; "><div style="color: #5e72e4; font-size: 35px; padding: 20px 0; width: 100%;"><div style="border-bottom: 4px #5e72e4 solid; padding: 0 0 10px 10px; margin: 0 20px;"> <img src='https://kwm-user-data.s3.eu-west-2.amazonaws.com/kwm_web/logo-keepwell.png' width="150px" height='auto' /></div></div><div style="padding: 20px; font-size: 20px;"><div style="margin-top: 30px;"> Hello ${name},<br/>Welcome to KeepWell Mantras.<br/> Your account is now all set to be used. Now you can purchase live classes and workout videos. And start your fitness journey with us.<br/> All the best for your new fitness journey.<br/><br/></div><div>Cheers,<br/>Team KeepWell Mantras</div></div></body></html>`;
	return send([toAddress], '✓ Hurray! Verify your email address and start using', data, 'HTML', process.env.SES_EMAIL_NOREPLY);

}

function passwordResetEmail(id, toAddress, name) {

	let token_data = {
		payload: id,
		type: 'PASSWORD_RESET'
	};
	let expiry = process.env.PASSWORD_RESET_EXPIRY_IN_MINUTES * 60;
	const token = jwt.sign(token_data, process.env.JWT_SECRET, { expiresIn: expiry });
	let data = `<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"\/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"\/><title>Password Reset<\/title><style>html,body{height:100%;width:100%;padding:0;margin:0}<\/style><\/head><body style=\"background-color: #F7F6F5; color: #2F4858; \"><div style=\"color: #5e72e4; font-size: 35px; padding: 20px 0; width: 100%;\"><div style=\"border-bottom: 4px #5e72e4 solid; padding: 0 0 10px 10px; margin: 0 20px;\"> <img src='https://kwm-user-data.s3.eu-west-2.amazonaws.com/kwm_web/logo-keepwell.png' width=\"150px\" height='auto' \/><\/div><\/div><div style=\"padding: 20px; font-size: 20px;\"><div style=\"margin-top: 30px;\"> Hi ${name},<br\/> Please use the button below to reset your password.<\/div><div style=\"display: flex; justify-content: flex-start; cursor: pointer;\"> <a style=\"margin: 30px 0; padding: 10px 30px; color: #fff; background-color: #5e72e4; font-size: 20px; text-decoration: none;\" href='${process.env.APP_HOME}\/reset_pass?tok=${token}'> Reset Password <\/a><\/div><div> With Best Compliments<br\/>Team KeepWell Mantras<\/div><\/div><\/body><\/html>`;

	return send([toAddress], 'KeepWell Mantras password reset', data, 'HTML', process.env.SES_EMAIL_NOREPLY);

}

function getDiscountText(discount_type) {
	switch (discount_type) {
		case 'LIVE':
			return '<div style="font-size: 14px;">Only applicable on live classes</div>';
		case 'RECORDED':
			return '<div style="font-size: 14px;">Only applicable on workout videos</div>';
		case '6_WEEKS':
			return '<div style="font-size: 14px;">Only applicable on 6 week nutrition support plan</div>';
		case '60_MINS':
			return '<div style="font-size: 14px;">Only applicable on 60 minutes nutrition support consultation call</div>';
		default:
			return '';
	}
}

function invoiceEmail(toAddress, name, items, total, discount, discount_type, expiry) {
	let data = `<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><title>Verify Email</title><style>html,body{height:100%;width:100%;padding:0;margin:0}</style></head><body style="background-color: #fff; color: #2F4858; "><div style="color: #5e72e4; font-size: 35px; padding: 20px 0; width: 100%;"><div style="border-bottom: 4px #5e72e4 solid; padding: 0 0 10px 10px; margin: 0 20px;"> <img src='https://kwm-user-data.s3.eu-west-2.amazonaws.com/kwm_web/logo-keepwell.png' width="150px" height='auto' /></div></div><div style="padding: 20px; font-size: 20px;"><div style="margin-top: 30px;"> Hello ${name},<br /> Thank you for your interest in our live classes and workout videos.<br /> Here is a summary of your purchase with us:<br /><div style="width: 100%; display: flex; margin: 30px 0 20px 0; max-width: 600px;"><div style="width: 50%"> <strong>Product</strong></div><div style="width: 25%"> <strong>Quantity</strong></div><div style="width: 25%"> <strong>Total</strong><br/></div></div> ${items.map(e => `<div style="width: 100%; display: flex; max-width: 600px;"><div style="width: 50%"> ${e.name}</div><div style="width: 25%"> ${e.qty}</div><div style="width: 25%"> £${e.total}</div></div> `).join('')} ${discount ? `<div style="width: 100%; display: flex; max-width: 600px; margin: 10px 0;"><div style="width: 50%"> <strong>Discount</strong>${getDiscountText(discount_type)}</div><div style="width: 25%"></div><div style="width: 25%"> ${discount}%</div></div>` : ''}<div style="width: 100%; display: flex; margin-bottom: 30px; max-width: 600px;"><div style="width: 50%"> <strong>Total</strong></div><div style="width: 25%"></div><div style="width: 25%"> <strong>£${total}</strong></div></div><div>Your live classes and workout videos will expires on ${expiry}.</div><div>All the best for your upcoming fitness journey.</div> <br /></div><div>Cheers,<br />Team KeepWell Mantras</div></div></body></html>`;
	return send([toAddress], 'Thanks for purchasing with us', data, 'HTML', process.env.SES_EMAIL_NOREPLY);
}

module.exports = {
	passwordResetEmail,
	verificationEmail,
	send,
	welcomeEmail,
	invoiceEmail
};
