var express = require('express');
const { query, body } = require('express-validator');
var moment = require('moment');
var router = express.Router();

var { success, error } = require('../response');
const jwtVerify = require('../jwtVerify');
const enrollmentModel = require('../models/enrollment');
const log = require('../logs');

router.use(jwtVerify);

router.route('/')
	.get([
		query('id')
			.isMongoId()
			.withMessage('id is required to get enrollment'),
	], async (req, res) => {
		try {
			const { id } = req.query;

			let query = {
				user: req.user._id,
				valid_till: { $gt: moment().toDate() }
			};

			if (id) {
				query._id = id;
				let enrollment = await enrollmentModel.findOne(query).populate('category_id').populate('classes._id', 'title description');
				if (!enrollment) return error(res, 404, 'Enrollment not found');
				return success(res, enrollment);

			} else {
				let query = { user: req.user._id, $expr: { $gt: ['$valid_till', moment().toDate()] } };

				let classes = await enrollmentModel.aggregate([
					{
						$facet: {
							recorded_classes: [
								{
									$match: {
										type: 'RECORDED',
										...query
									}
								}, {
									$lookup: {
										from: 'categories',
										localField: 'category_id',
										foreignField: '_id',
										as: 'category_id'
									}
								}, {
									$set: {
										category_id: {
											$arrayElemAt: ['$category_id', 0]
										}
									}
								}
							],
							live_classes: [
								{
									$match: {
										type: 'LIVE',
										...query
									}
								}
							],
							diet: [
								{
									$match: {
										type: '6_WEEKS',
										...query
									}
								}
							],
							consultation_call: [
								{
									$match: {
										type: '60_MINS',
										...query
									}
								}
							],
							expired: [
								{
									$match: {
										user: req.user._id,
										$expr: {
											$and: [
												{
													$and: [
														{
															$gt: ['$valid_till', moment().subtract(2, 'weeks').toDate()]
														}, {
															$lt: ['$valid_till', moment().toDate()]
														}
													]
												},
												{
													$in: ['$type', ['RECORDED', 'LIVE']]
												}
											]
										}
									}
								}, {
									$lookup: {
										from: 'categories',
										localField: 'category_id',
										foreignField: '_id',
										as: 'category_id'
									}
								}, {
									$set: {
										category_id: {
											$arrayElemAt: ['$category_id', 0]
										}
									}
								}
							]
						}
					}
				]);

				classes = classes[0];

				log.debug(classes);

				return success(res, classes);
			}

		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

router.route('/files')
	.patch([
		body('url')
			.exists({ checkFalsy: true, checkNull: true })
			.isString()
			.withMessage('url of uploaded file is required'),
		body('name')
			.exists({ checkFalsy: true, checkNull: true })
			.isString()
			.withMessage('name of uploaded file is required'),
	], async (req, res) => {
		try {
			let diet = await enrollmentModel.findOneAndUpdate({ type: '6_WEEKS', user: req.user._id }, { $push: { files: req.body } }, { new: true });
			return success(res, diet);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.delete([
		body('eid')
			.exists({ checkFalsy: true, checkNull: true })
			.isString()
			.withMessage('url of uploaded file is required'),
		body('fid')
			.exists({ checkFalsy: true, checkNull: true })
			.isString()
			.withMessage('name of uploaded file is required'),
	], async (req, res) => {
		if (!req.user.admin) return error(res, 401, 'Unauthorized');
		const { eid, fid } = req.body;
		try {

			let enrollment = await enrollmentModel.findOne({ _id: eid });
			if (!enrollment || !enrollment.files) return error(res, 400, 'File not found');

			let file = enrollment.files.findIndex(e => e._id.toString() === fid);
			if (file === -1) return error(res, 400, 'File not found');

			enrollment.files.splice(file, 1);
			await enrollment.save();

			success(res);

		} catch (err) {
			log.error(err);
			return error(res);
		}

	});

router.use((req, res, next) => {
	if (!req.user.admin) {
		return error(res, 401, 'Unauthorized');
	}
	next();
});

router.route('/admin/6_week_diet')
	.get(async (req, res) => {
		try {
			let enrollments = await enrollmentModel.find({ type: '6_WEEKS' }).sort({ createdAt: -1 }).populate('user', 'first_name last_name email mobile');
			success(res, enrollments);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.patch(async (req, res) => {
		try {
			const { _id, url, name } = req.body;
			await enrollmentModel.findByIdAndUpdate(_id, { $push: { files: { url, name } } });
			success(res);
		} catch (err) {
			log.error(err);
			return error(res);
		}

	});

router.route('/admin/60_mins')
	.get(async (req, res) => {
		try {
			let enrollments = await enrollmentModel.find({ type: '60_MINS' }).sort({ createdAt: -1 }).populate('user', 'first_name last_name email mobile');
			success(res, enrollments);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

module.exports = router;
