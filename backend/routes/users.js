var express = require('express');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var { body, query } = require('express-validator');
var { OAuth2Client } = require('google-auth-library');
var axios = require('axios');
var randomstring = require('randomstring');

const userModel = require('../models/userModel');

var { success, error: sendError, checkError, error } = require('../response.js');
const jwtVerify = require('../jwtVerify');
const log = require('../logs');
const { verificationEmail, passwordResetEmail, send, welcomeEmail } = require('../emailService');
const { default: validator } = require('validator');

var router = express.Router();
const client = new OAuth2Client(process.env.GOOGLE_OAUTH_CLIENT_ID, process.env.GOOGLE_OAUTH_CLIENT_SECRET);

const saltRounds = 10;

function prepareUserData(user) {
	return ({
		_id: user._id,
		first_name: user.first_name,
		last_name: user.last_name,
		email: user.email,
		initial_weight: user.initial_weight,
		initial_waist: user.initial_waist,
		weight: user.weight,
		waist: user.waist,
		admin: user.admin,
		verified: true,
		profile_picture: user.profile_picture,
		mobile: user.mobile,
		gender: user.gender,
		live_classes_count: user.live_classes_count,
		classes: user.classes,
		bio: user.bio,
		stats: user.stats,
		points: user.points,
		referral_code: user.referral_code
	});
}

router
	.route('/')
	.post([
		body('email')
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('email id is required for register')
			.trim()
			.isEmail()
			.withMessage('email id is required for register')
			.normalizeEmail(),
		body('first_name')
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('first name is required for register')
			.isString()
			.withMessage('first name is required to register'),
		body('last_name')
			.optional()
			.isString()
			.withMessage('name is required to register')
	], checkError, async (req, res) => {
		try {
			let { password, email, first_name, last_name, referral_code } = req.body;

			if (password) password = password.trim();

			if (!password || !validator.isStrongPassword(password)) {
				return error(res, 422, 'Password should be at least 8 character long and must contain at least 1 lowercase character, 1 upper case character, 1 numeric and 1 special symbol');
			}


			let user = await userModel.findOne({ email: email });
			if (!user) {
				bcrypt.hash(password, saltRounds, async (err, hash) => {

					if (err) {
						log.error(err);
						return sendError(res, 500, 'Some internal error occurred');
					}

					let referred_by = null;
					if (referral_code)
						referred_by = await userModel.findOne({ referral_code });

					try {

						// generate referral code for new user
						let referral_code = randomstring.generate(8);
						let u = await userModel.findOne({ referral_code });
						while (u) {
							referral_code = randomstring.generate(8);
							u = await userModel.findOne({ referral_code });
						}

						user = await userModel.create({
							first_name,
							last_name,
							email: email,
							hash: hash,
							status: 'REGISTERED',
							referred_by: referred_by ? referred_by._id : null,
							referral_code,
							points: 0
						});
						if (user) {
							log.debug(user);
							let data = await verificationEmail(user._id, email, `${first_name} ${last_name}`);
							log.debug(data);
							success(res, { registered: true, message: 'Please verify your email address by clicking on the link sent to your email ' });
						}
					} catch (err) {
						log.error(err);
						sendError(res);
					}
				});
			} else {
				error(res, 400, 'An account with this email address already exists');
			}
		} catch (e) {
			log.error(e);
			sendError(res);
		}
	})
	.get(jwtVerify, async (req, res) => {
		try {
			success(res, prepareUserData(req.user));
		} catch (err) {
			log.error(err);
			sendError(res);
		}
	})
	.patch(jwtVerify, [
		body('first_name')
			.optional()
			.isString()
			.isLength({ min: 3 })
			.withMessage('First name is required and should be at least 3 character long')
	], checkError, async (req, res) => {
		try {

			delete req.body._id;
			delete req.body.hash;
			delete req.body.email;

			const { weight, waist } = req.body;

			let update = { $set: req.body };

			if (weight) {
				update.$set = {
					...update.$set,
					weight
				};

				if (!req.user.initial_weight) {
					update.$set = {
						...update.$set,
						initial_weight: weight
					};
				}

			}

			if (waist) {
				update.$set = {
					...update.$set,
					waist
				};

				if (!req.user.initial_waist) {
					update.$set = {
						...update.$set,
						initial_waist: waist
					};
				}

			}

			if (weight || waist) {
				update.$push = {
					stats: {
						weight_in_kg: weight || req.user.weight,
						waist_in_inch: waist || req.user.waist,
						date: new Date()
					}
				};
			}

			if (req.body.hasOwnProperty('first_name') && !req.body.first_name)
				delete req.body.first_name;

			const user = await userModel.findByIdAndUpdate(req.user._id, update, { new: true });

			success(res, prepareUserData(user));
		} catch (err) {
			log.error(err);
			sendError(res);
		}
	});

router.route('/login')
	.post([
		body('email')
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('email id is required for login')
			.isEmail()
			.withMessage('email id is required for login')
			.normalizeEmail(),
		body('password')
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('password is required for register')
			.trim()
			.withMessage('password is required for register')
	], checkError, async (req, res) => {
		try {

			const user = await userModel.findOne({ email: req.body.email });

			if (!user) {
				return sendError(res, 400, 'Account does not exist');
			}

			log.debug(user);


			if (user.status !== 'VERIFIED') {
				return success(res, { verified: false });
			}

			if (!user.hash)
				return error(res, 400, 'You do not have a password setup, please use forgot password to create password.');

			bcrypt.compare(req.body.password, user.hash, async (err, result) => {

				if (err) {
					log.error(err);
					return sendError(res, 500, 'Some internal error occurred');
				}

				if (!result) return sendError(res, 400, 'Incorrect Password');

				const token = jwt.sign({ payload: user._id }, process.env.JWT_SECRET, { expiresIn: `${process.env.ACCESS_TOKEN_EXPIRY_IN_DAYS}d` });

				res.cookie('jwt', token, {
					maxAge: (process.env.ACCESS_TOKEN_EXPIRY_IN_DAYS * 86400000),
					httpOnly: process.env.COOKIE_HTTP_ONLY,
					secure: process.env.COOKIE_SECURE
				});

				success(res, prepareUserData(user));

			});

		} catch (e) {
			log.error(e);
			sendError(res);
		}
	})
	.delete(jwtVerify, async (req, res) => {
		res.clearCookie('jwt');
		success(res, 'logged out successfully');
	});

router.post('/google', async (req, res) => {
	try {
		const { id_token } = req.body;
		const tokenInfo = await client.verifyIdToken({ idToken: id_token, audience: process.env.GOOGLE_OAUTH_CLIENT_ID });

		const { picture, given_name, family_name, email } = tokenInfo.getPayload();

		let user = await userModel.findOne({ email: email });

		if (!user) {

			// generate referral code for new user
			let referral_code = randomstring.generate(8);
			let u = await userModel.findOne({ referral_code });
			while (u) {
				referral_code = randomstring.generate(8);
				u = await userModel.findOne({ referral_code });
			}

			let userData = {
				email: email,
				last_name: family_name,
				first_name: given_name,
				status: 'VERIFIED',
				profile_picture: picture,
				referral_code,
				points: 0
			};

			user = await userModel.create(userData);

			welcomeEmail(email, `${given_name} ${family_name}`);

		}

		const token = jwt.sign({ payload: user._id }, process.env.JWT_SECRET, { expiresIn: `${process.env.ACCESS_TOKEN_EXPIRY_IN_DAYS}d` });

		res.cookie('jwt', token, {
			maxAge: (process.env.ACCESS_TOKEN_EXPIRY_IN_DAYS * 86400000),
			httpOnly: process.env.COOKIE_HTTP_ONLY,
			secure: process.env.COOKIE_SECURE
		});

		return success(res, prepareUserData(user));

	} catch (err) {
		log.error(err);
		return error(res);
	}
});

router.post('/facebook', async (req, res) => {
	try {

		const { access_token } = req.body;
		const { data } = await axios({
			url: 'https://graph.facebook.com/me',
			method: 'get',
			params: {
				fields: ['id', 'email', 'first_name', 'last_name', 'picture'].join(','),
				access_token,
			},
		});

		if (!data.email) return error(res, 400, 'Invalid token');

		let user = await userModel.findOne({ email: data.email });

		if (!user) {

			// generate referral code for new user
			let referral_code = randomstring.generate(8);
			let u = await userModel.findOne({ referral_code });
			while (u) {
				referral_code = randomstring.generate(8);
				u = await userModel.findOne({ referral_code });
			}

			let userData = {
				email: data.email,
				last_name: data.last_name,
				first_name: data.first_name,
				status: 'VERIFIED',
				referral_code,
				points: 0
			};

			log.debug(data.picture);

			if (data.picture) {
				userData.profile_picture = data.picture.data.url;
			}

			user = await userModel.create(userData);

			welcomeEmail(data.email, `${data.first_name} ${data.last_name}`);
		}

		const token = jwt.sign({ payload: user._id }, process.env.JWT_SECRET, { expiresIn: `${process.env.ACCESS_TOKEN_EXPIRY_IN_DAYS}d` });

		res.cookie('jwt', token, {
			maxAge: (process.env.ACCESS_TOKEN_EXPIRY_IN_DAYS * 86400000),
			httpOnly: process.env.COOKIE_HTTP_ONLY,
			secure: process.env.COOKIE_SECURE
		});

		return success(res, prepareUserData(user));

	} catch (err) {
		log.error(err);
		return error(res);
	}
});

router.route('/verify_email')
	.get([
		query('tok')
			.exists({ checkNull: true, checkFalsy: true })
			.withMessage('Invalid link')
	], checkError, async (req, res) => {
		let message = 'This link is no longer valid please login again and use resend verification email button';
		let title = 'Invalid link';
		try {
			const { tok } = req.query;
			const decoded = jwt.verify(tok, process.env.JWT_SECRET);
			log.debug(decoded);
			if (decoded.type === 'EMAIL_VERIFICATION') {
				let user = await userModel.findById(decoded.payload);
				res.redirect(301, `${process.env.APP_HOME}/login?verified=true`);
				if (user.status !== 'VERIFIED') {
					welcomeEmail(user.email, `${user.first_name} ${user.last_name}`);
					user.status = 'VERIFIED';
					await user.save();
				}
			} else res.redirect(301, `${process.env.APP_HOME}/error?status=400&title=${title}&message=${message}`);
		} catch (e) {
			if (e.name) {
				switch (e.name) {
					case 'TokenExpiredError':
						log.error('token expired error');
						break;
					case 'JsonWebTokenError':
						log.error('Invalid token');
						break;
					default:
						log.debug(e);
				}
			}
			return res.redirect(301, `${process.env.APP_HOME}/error?status=400&title=${title}&message=${message}`);
		}
	})
	.post([
		body('email')
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('email id is required for login')
			.isEmail()
			.withMessage('email id is required for login'),
	], checkError, async (req, res) => {
		try {

			let user = await userModel.findOne({ email: req.body.email });
			if (!user) return sendError(res, 400, 'Account does not exist.');
			await verificationEmail(user._id, req.body.email, `${user.first_name} ${user.lsat_name}`);
			return success(res);

		} catch (e) {
			log.error(e);
			sendError(res);
		}
	});

router.patch('/password', checkError, jwtVerify, async (req, res) => {
	try {
		let { password, current_password } = req.body;

		if (password) password = password.trim();
		if (!password || !validator.isStrongPassword(password)) {
			return error(res, 422, 'Password should be at least 8 character long and must contain at least 1 lowercase character, 1 upper case character, 1 numeric and 1 special symbol');
		}

		bcrypt.compare(current_password, req.user.hash, (err, same) => {

			if (err) {
				return sendError(res);
			}

			if (!same) {
				return sendError(res, 400, 'Invalid current password');
			}

			bcrypt.hash(password, saltRounds, async (err, enc) => {
				if (err) {
					log.error(err);
					return sendError(res);
				}
				req.user.hash = enc;
				await req.user.save();
				success(res);
			});
		});

	} catch (err) {
		log.error(err);
		sendError(res);
	}
});

router
	.route('/forgot_password')
	.get([
		query('email')
			.exists({ checkFalsy: true, checkNull: true })
			.isEmail()
			.withMessage('Email id is required to use forgot password')
			.normalizeEmail()
	], checkError, async (req, res) => {
		try {

			let email = req.query.email;
			let acc = await userModel.findOne({ email });

			if (!acc) return sendError(res, 400, 'Account doesn\'t exist');
			if (acc.status !== 'VERIFIED') return sendError(res, 400, 'Please verify your account first');

			let data = await passwordResetEmail(acc._id, email, `${acc.first_name} ${acc.last_name}`);
			log.debug(data);
			await acc.updateOne({ $set: { reset_active: true } });
			success(res);

		} catch (err) {
			log.error(err);
			sendError(res);
		}
	})
	.patch([
		body('token')
			.isJWT()
			.withMessage('Invalid token')
	], checkError, async (req, res) => {
		try {
			let { token, password } = req.body;
			if (password) password = password.trim();
			if (!password || !validator.isStrongPassword(password)) {
				return error(res, 422, 'Password should be at least 8 character long and must contain at least 1 lowercase character, 1 upper case character, 1 numeric and 1 special symbol');
			}
			jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
				if (err && err.name === 'TokenExpiredError') {
					return error(res, 400, 'Link has expired, please follow forgot password process again to create a new link.');
				}
				if (decoded.type === 'PASSWORD_RESET') {
					let acc = await userModel.findById(decoded.payload);
					if (!acc || !acc.reset_active) {
						return sendError(res, 400, 'Invalid token');
					}
					bcrypt.hash(password, saltRounds, async (err, enc) => {
						if (err) {
							log.error(err);
							return sendError(res);
						}
						acc.reset_active = false;
						acc.hash = enc;
						await acc.save();
						success(res);
					});
				} else sendError(res, 400, 'Invalid Link');
			});
		} catch (e) {
			log.error(e);
			return sendError(res);
		}
	});

router.post('/support_ticket', [
	body(['first_name', 'message'])
		.exists({ checkFalsy: true, checkNull: true })
		.withMessage('first name, last name and message are required'),
	body('email')
		.isEmail()
		.withMessage('email is required')
], checkError, async (req, res) => {
	const { phone, message, first_name, last_name, email } = req.body;
	try {
		let temp = await send([process.env.SES_EMAIL_NOREPLY], 'User contact request', `E-Mail = ${email}\nName = ${first_name} ${last_name}\nphone = ${phone}\nFeedback = ${message}`, 'TEXT', process.env.SES_EMAIL_NOREPLY);
		log.debug(temp);
		success(res, 'Thanks for reaching out to us.');
	} catch (err) {
		log.error(err);
		sendError(res, 500, 'Some internal server error occurred');
	}
});

module.exports = router;
