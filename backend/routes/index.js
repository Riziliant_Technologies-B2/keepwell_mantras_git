var express = require('express');

var router = express.Router();

var { success, error } = require('../response');
const webContentModel = require('../models/web_content');
const log = require('../logs');


router
	.get('/', async (req, res, next) => {		
		try {
			const content = await webContentModel.findOne({});
			return success(res, content);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

module.exports = router;
