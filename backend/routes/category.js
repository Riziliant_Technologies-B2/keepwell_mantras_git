var express = require('express');

var router = express.Router();

var { success, error } = require('../response');
const jwtVerify = require('../jwtVerify');
const categoryModel = require('../models/class_category');
const log = require('../logs');
const { query } = require('express-validator');


router.route('/')
	.get([
		query('ids')
			.optional()
			.customSanitizer((val) => JSON.parse(val))
			.isArray({ min: 1 })
			.withMessage('ids must be a array and must contain at least one id'),
	], async (req, res, next) => {
		try {
			const { ids } = req.query;
			let query = { enabled: true };
			if (ids) query._id = { $in: ids };
			let categories = await categoryModel.find(query);
			return success(res, categories);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

router.use(jwtVerify, (req, res, next) => {
	if (!req.user.admin) return error(res, 401, 'Unauthorized');
	next();
});

router.route('/')
	.post(async (req, res, next) => {
		try {
			const { name, description, thumbnail, price_per_class, duration } = req.body;
			let category = await categoryModel.create({ name, description, thumbnail, price_per_class, duration });
			return success(res, category);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.patch(async (req, res) => {
		try {

			const { _id } = req.body;

			let category = await categoryModel.findByIdAndUpdate(_id, { $set: req.body });
			return success(res, category);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

module.exports = router;
