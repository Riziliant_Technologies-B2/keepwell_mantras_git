var { body, query } = require('express-validator');
var moment = require('moment');
var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();

var classModel = require('../models/class');
var categoryModel = require('../models/class_category');
const enrollmentModel = require('../models/enrollment');

var jwtVerify = require('../jwtVerify');
const { error, success, checkError } = require('../response');
const log = require('../logs');
const { class_type } = require('../const');
const classType = require('../const').class_type;
const { getURL } = require('../urlCreator');
const { log_video_view } = require('../services');

router.route('/')
	.get([
		query('type')
			.optional()
			.isIn(classType)
			.withMessage('type must be a string'),
		query('ids')
			.optional()
			.customSanitizer((val) => JSON.parse(val))
			.isArray({ min: 1 })
			.withMessage('ids must be a array and must contain at least one id'),
		query('limit')
			.optional()
			.customSanitizer((val) => JSON.parse(val))
			.isInt({ min: 1 })
			.withMessage('limit must be a integer greater than 0')
	], async (req, res) => {

		const { type, ids, limit, upcoming, date, include_enrollment } = req.query;

		let classes;
		let start;
		let end;

		let query = {
		};

		if (type) {
			query.type = type;
			if (type === 'LIVE' && upcoming) query.date = { $gt: new Date() };
		}

		if (ids) {
			query._id = { $in: ids };
		}

		if (req.user && !req.user.admin) {
			query.enabled = true;
		}

		if (date) {
			start = new Date(date);
			start.setHours(0);
			start.setMinutes(0);
			start.setSeconds(0);
			end = new Date(start.valueOf() + 3600 * 1000 * 24);
			query.date = { $gt: start, $lt: end };
		}

		const project = {
			title: 1,
			description: 1,
			date: 1,
			duration_in_minutes: 1,
			type: 1,
			category: 1,
			recording_link: 1
		};

		if (req.user.admin) {
			project.meeting_link = 1;
		}

		let db_query = classModel.find(query, project).sort({ date: -1 }).populate('category', 'name');

		if (limit) {
			db_query = db_query.limit(limit).lean();
		}

		db_query = db_query.lean();
		classes = await db_query.exec();

		if (req.user.admin && include_enrollment && classes.length) {

			let enrollments = await enrollmentModel.aggregate([
				{
					$match: {
						$expr: {
							$lt: [moment().toDate(), '$valid_till']
						}
					}
				}, {
					$project: {
						user: 1,
						classes: 1,
						_id: 0
					}
				}, {
					$unwind: {
						path: '$classes',
						preserveNullAndEmptyArrays: false
					}
				}, {
					$group: {
						_id: '$classes._id',
						users: { $addToSet: '$user' }
					}
				}, {
					$project: {
						v: { $size: '$users' },
						_id: { $toString: '$_id' }
					}
				}, {
					$group: {
						_id: null,
						classes: { $push: { k: '$_id', v: '$v' } }
					}
				}, {
					$project: {
						classes: { $arrayToObject: '$classes' }
					}
				}
			]);

			log.debug(enrollments);

			if (enrollments.length)
				classes = classes.map(e => ({
					userCount: enrollments[0].classes[e._id] || 0,
					...e,
				}));

		}

		success(res, classes);
	})
	.post(jwtVerify, [
		body(['title', 'description'])
			.isString()
			.withMessage('title, date, description and thumbnail are required'),
		body('type')
			.isIn(class_type)
			.withMessage('class type is required'),
		body('category')
			.isMongoId()
			.withMessage('Exercise category is required and should be a mongo id'),
		body('duration_in_minutes')
			.isInt({ min: 0 })
			.withMessage('duration of session is required'),
		body('order')
			.optional()
			.isInt({ min: 0 })
			.withMessage('order of class is required')
	], checkError, async (req, res, next) => {
		try {
			if (!req.user.admin) return error(res, 401, 'Unauthorized');

			const { title, description, date, duration_in_minutes, meeting_link, recording_link, type, category, order } = req.body;

			if (type === 'RECORDED' && !recording_link) return error(res, 400, 'Recording link is required');
			if (type === 'RECORDED' && isNaN(order)) return error(res, 400, 'order of class is required');
			if (type === 'LIVE' && !meeting_link) return error(res, 400, 'meeting link is required');

			let _category = await categoryModel.findById(category);
			if (!_category) return error(res, 404, 'Invalid category');

			if (type === 'RECORDED') {
				let _class = await classModel.findOne({ category, order });
				if (_class) return error(res, 400, 'A class with similar order and category already exists');
			}

			let _class = await classModel.create({
				title, description, date, duration_in_minutes, meeting_link, recording_link, type, category, order
			});

			if (type === 'RECORDED') {
				_category.classes.push(_class._id);
				await _category.save();
			}

			success(res, _class, 201);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.patch(jwtVerify, async (req, res) => {
		try {
			if (!req.user.admin) return error(res, 401, 'Unauthorized');

			const { _id, ...update_obj } = req.body;

			const _class = await classModel.findByIdAndUpdate(_id, { $set: update_obj }, { new: true });
			if (!_class) return error(res, 404, 'Class does not exists');

			success(res, _class);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.delete([
		query('id')
			.isMongoId()
			.withMessage('id must be a mongo id'),
	], checkError, async (req, res) => {
		try {

			if (!req.user.admin)
				return error(res, 401, 'Unauthorized');

			let _class = await classModel.findOne({ _id: req.query.id });
			if (!_class) return error(res, 404, 'Class not found');

			let enrollments = await enrollmentModel.findOne({
				classes: {
					$elemMatch: {
						_id: { $eq: new mongoose.Types.ObjectId(req.query._id) }
					}
				},
				valid_till: { $gt: moment().toDate() }
			});
			if (enrollments) return error(res, 400, 'Cannot delete class, some users are still enrolled');

			await classModel.findByIdAndDelete(req.query.id);
			return success(res);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

router.get('/meeting_link', [
	query('class_id')
		.isMongoId()
		.withMessage('class_id is required to get video link')
], jwtVerify, async (req, res, next) => {
	try {

		const { class_id } = req.query;
		const _class = await classModel.findOne({ _id: class_id });

		if (!_class) return error(res, 404, 'Class not found');
		if (_class.type !== 'LIVE') return error(res, 404, 'Meeting link for class is not available for these type of classes');
		if (!_class.meeting_link) return error(res, 404, 'Meeting link for class is not yet available');
		if (!moment().isBetween(moment(_class.date).subtract(30, 'minute'), moment(_class.date).add(_class.duration_in_minutes, 'minute'))) {
			return error(res, 404, 'Class has not started yet or is no longer available.');
		}

		let enrolled = await enrollmentModel.findOne({
			user: req.user._id,
			type: 'LIVE',
			'classes._id': new mongoose.Types.ObjectId(req.query.class_id),
			'classes.type': 'LIVE',
			valid_till: { $gt: moment().toDate() }
		});

		if (enrolled) {
			log_video_view(req.user._id, _class._id, enrolled._id, 'LIVE', _class.title);
			return success(res, _class.meeting_link);
		}

		let enrollments = await enrollmentModel.aggregate([
			{
				$match: {
					user: req.user._id,
					type: 'LIVE',
					$expr: {
						$and: [
							{
								$lt: [{ $size: '$classes' }, '$class_count']
							}, {
								$lt: [moment().toDate(), '$valid_till']
							}
						]
					}
				}
			}, {
				$sort: {
					createdAt: 1
				}
			}
		]);

		if (enrollments.length) {
			let enrollment = enrollments[0];
			await enrollmentModel.updateOne(
				{
					_id: enrollment._id },
				{
					$push: {
						classes: {
							_id: _class._id,
							watch_start: new Date(),
							type: 'LIVE'
						}
					}
				}
			);
			log_video_view(req.user._id, _class._id, enrollment._id, 'LIVE', _class.title);
			return success(res, _class.meeting_link);

		} else if (req.user.points && req.user.points > 50) {
			req.user.points -= 50;
			await req.user.save();
			log_video_view(req.user._id, _class._id, null, 'LIVE', _class.title);
			return success(res, _class.meeting_link);
		} else {
			return error(res, 402, 'Please purchase more live classes to watch recorded live classes');
		}

	} catch (err) {
		log.error(err);
		error(res);
	}
});

router.get('/recording_url', jwtVerify, async (req, res) => {
	try {
		let { class_id } = req.query;
		let enrollments;
		const _class = await classModel.findOne({ _id: class_id, enabled: true }).populate('category');

		if (!_class)
			return error(res, 404, 'Class not found');
		if (!_class.recording_link)
			return error(res, 400, 'recording is not yet available');

		if (_class.type === 'RECORDED') {

			enrollments = await enrollmentModel.aggregate([
				{
					$match: {
						user: req.user._id,
						type: 'RECORDED',
						category_id: _class.category._id,
						$expr: {
							$lt: [moment().toDate(), '$valid_till']
						}
					}
				}, {
					$unwind: {
						path: '$classes',
						preserveNullAndEmptyArrays: false,
						includeArrayIndex: 'ind'
					}
				}, {
					$match: {
						$expr: {
							$and: [
								{ $eq: ['$classes._id', _class._id] },
								{
									$or: [
										{
											$ifNull: ['$classes.watch_start', true]
										}, {
											$gt: ['$classes.watch_start', moment().subtract(1, 'd').toISOString()]
										}
									]
								}
							]
						}
					}
				}
			]);

			log.debug(enrollments);

			if (enrollments.length && enrollments[0]._id) {

				if (!enrollments[0].classes.watch_start) {
					await enrollmentModel.updateOne(
						{ _id: enrollments[0]._id },
						{
							$set: {
								[`classes.${enrollments[0].ind}.watch_start`]: new Date(),
							}
						}
					);
					log_video_view(req.user._id, _class._id, enrollments[0]._id, 'RECORDED', _class.title);
				}
			} else if (req.user.points && req.user.points > 50) {
				req.user.points -= 50;
				await req.user.save();
			} else {
				return error(res, 402, 'Please purchase more workout videos of this category.');
			}

		} else {

			enrollments = await enrollmentModel.aggregate([
				{
					$match: {
						user: req.user._id,
						type: 'LIVE',
						$expr: {
							$lt: [moment().toDate(), '$valid_till']
						}
					}
				}, {
					$unwind: {
						path: '$classes',
						preserveNullAndEmptyArrays: false,
						includeArrayIndex: 'ind'
					}
				}, {
					$match: {
						$expr: {
							$and: [
								{ $eq: ['$classes._id', _class._id] },
								{ $gt: ['$classes.watch_start', moment().subtract(1, 'd').toDate()] }
							]
						}
					}
				}
			]);

			if (!enrollments.length) {
				enrollments = await enrollmentModel.aggregate([
					{
						$match: {
							user: req.user._id,
							type: 'LIVE',
							$expr: {
								$and: [
									{
										$lt: [{ $size: '$classes' }, '$class_count']
									}, {
										$lt: [moment().toDate(), '$valid_till']
									}
								]
							}
						}
					}, {
						$sort: {
							createdAt: 1
						}
					}
				]);

				if (enrollments.length && enrollments[0]._id) {

					if (!enrollments[0].classes.watch_start) {
						await enrollmentModel.updateOne(
							{ _id: enrollments[0]._id },
							{
								$push: {
									classes: {
										_id: _class._id,
										watch_start: new Date(),
										type: 'RECORDED_LIVE'
									}
								}
							}
						);
						log_video_view(req.user._id, _class._id, enrollments[0]._id, 'RECORDED_LIVE', _class.title);
					}
				} else if (req.user.points && req.user.points > 50) {
					req.user.points -= 50;
					await req.user.save();
				} else {
					return error(res, 402, 'Please purchase more live classes to watch recorded live classes');
				}

			}
		}

		const class_recording = _class.type === 'LIVE' ? _class.recording_link : getURL(_class.recording_link);
		return success(res, { class_recording, cat_name: _class.category.name, title: _class.title, description: _class.description, type: _class.type });

	} catch (err) {
		log.error(err);
		error(res);
	}
});

router.get('/my_classes', jwtVerify, async (req, res) => {
	try {
		let data = await enrollmentModel.aggregate([
			{
				$match: {
					user: req.user._id,
					type: 'RECORDED',
					$expr: {
						$lt: [moment().toDate(), '$valid_till']
					}
				}
			}, {
				$unwind: {
					path: '$classes',
					preserveNullAndEmptyArrays: false
				}
			}, {
				$lookup: {
					from: 'classes',
					let: { c_id: '$classes._id', e_valid_till: '$valid_till', c_start_time: '$classes.watch_start' },
					as: 'class',
					pipeline: [
						{
							$match: {
								$expr: {
									$eq: ['$_id', '$$c_id']
								}
							}
						}, {
							$project: {
								title: 1,
								description: 1,
								duration_in_minutes: 1,
								start_time: '$$c_start_time',
								expires: {
									$cond: {
										if: '$$c_start_time',
										then: {
											$add: ['$$c_start_time', 24 * 3600 * 1000]
										},
										else: '$$e_valid_till'
									}
								}
							}
						}
					]
				}
			}, {
				$unwind: {
					path: '$class',
					preserveNullAndEmptyArrays: false
				}
			}, {
				$group: {
					_id: '$category_id',
					classes: {
						$push: '$class'
					}
				}
			}, {
				$lookup: {
					from: 'categories',
					let: { cat_id: '$_id' },
					as: 'category',
					pipeline: [
						{
							$match: {
								$expr: {
									$eq: ['$_id', '$$cat_id']
								}
							}
						}, {
							$project: {
								name: 1,
								_id: 0
							}
						}
					]
				}
			}, {
				$unwind: {
					path: '$category',
					preserveNullAndEmptyArrays: false
				}
			}
		]);
		log.debug(data);

		success(res, data);

	} catch (err) {
		log.error(err);
		error(res);
	}
});

module.exports = router;
