var express = require('express');

var router = express.Router();

var { success, error, checkError } = require('../response');
const jwtVerify = require('../jwtVerify');
const activityModel = require('../models/activity_log');
const log = require('../logs');
const { body } = require('express-validator');

router.use(jwtVerify);

router.route('/')
	.get(async (req, res, next) => {
		try {
			let activities = await activityModel.find({ user: req.user._id }).populate('class_id', 'date').sort({createdAt: -1});
			return success(res, activities);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.patch([
		body('activity_id')
			.isMongoId()
			.withMessage('activity_id must be a mongo id')
	], checkError, async (req, res) => {
		try {
			const { picture, activity_id } = req.body;
			await activityModel.findOneAndUpdate({ _id: activity_id, user: req.user._id }, { $set: { picture } });
			return success(res);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

module.exports = router;
