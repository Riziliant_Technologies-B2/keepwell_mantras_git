var express = require('express');
const { query } = require('express-validator');

var router = express.Router();

var { success, error } = require('../response');
const jwtVerify = require('../jwtVerify');
const promoCodeModel = require('../models/promocodes');
const log = require('../logs');

router.use(jwtVerify);

/* upload public image */
router.route('/')
	.post(async (req, res) => {
		try {
			if (!req.user.admin) return error(res, 401, 'Unauthorized');
			const { valid_upto, valid_from, discount, name, max_class_count, min_class_count, class_type } = req.body;
			await promoCodeModel.create({
				valid_from, valid_upto, discount, name, max_class_count, min_class_count, class_type
			});
			return success(res);
		} catch (err) {
			log.error(err);
			return error(res);
		}
	})
	.get([
		query('promo_code')
			.isString()
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('promo code is required')
	], async function (req, res) {
		try {
			let code = await promoCodeModel.findOne({ name: req.query.promo_code, valid_upto: { $gt: new Date() }, valid_from: { $lte: new Date() } });
			if (code)
				return success(res, code);
			else return error(res, 404, 'Promo code not found');
		} catch (err) {
			log.error(err);
			error(res);
		}
	});

module.exports = router;
