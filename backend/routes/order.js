var express = require('express');
const { body, query } = require('express-validator');
var moment = require('moment');
var mongoose = require('mongoose');
var util = require('util');
var crypto = require('crypto');

var router = express.Router();

const promoCodeModel = require('../models/promocodes');
const categoryModel = require('../models/class_category');
const orderModel = require('../models/order');
const enrollmentModel = require('../models/enrollment');
const classModel = require('../models/class');
const userModel = require('../models/userModel');

var { success, error, checkError } = require('../response');
const jwtVerify = require('../jwtVerify');
const log = require('../logs');
const { invoiceEmail, send } = require('../emailService');
const { log_order_confirmed } = require('../services');

const live_class_price = parseInt(process.env.LIVE_CLASS_PRICE, 10);
const _60_min_consultation_price = parseInt(process.env._60_MIN_DIET_CONSULTATION_PRICE, 10);
const _6_week_diet_price = parseInt(process.env._6_WEEK_DIET_PACKAGE_PRICE, 10);

async function assignClasses(uid, cid, count) {

	// assign classes assuming classes are in a priority queue

	log.debug(count);

	let enrollments = await enrollmentModel.find({ user: uid, category_id: cid });

	let existing_classes = enrollments
		.map(e => e.classes
			.filter(c => !c.watch_start || moment(e.watch_start).add(1, 'day').isAfter(moment()))
			.map(c => c._id.toString())
		)
		.flat();

	log.debug(existing_classes);

	let classes = await classModel.find({ category: cid, type: 'RECORDED' }).sort({ order: 1 }).lean();
	let class_count = classes.length;

	if (existing_classes.length === 0)
		return classes.splice(0, count).map(e => ({
			_id: e._id,
			type: 'RECORDED'
		}));
	else {
		let start = classes.findIndex(e => existing_classes.includes(e._id.toString()));
		let end = (start + existing_classes.length) % class_count;

		let new_end = end + count;

		log.debug(start, end, new_end, class_count);

		let new_classes;

		if (new_end >= class_count) {
			if (end > 0) {
				new_classes = classes.slice(end);
				log.debug(new_classes);
			}
			log.debug(count, class_count, end, count - (class_count - end));
			new_classes = [...new_classes, ...classes.slice(0, count - (class_count - end))];
			log.debug(new_classes);
		} else {
			new_classes = classes.slice(end, count);
		}
		return new_classes.map(e => ({
			_id: e._id,
			type: 'RECORDED'
		}));
	}

}

function createHash(order, user) {

	let { TAKEPAYMENT_PRESHARED_KEY: presharedkey, TAKEPAYMENT_MID: mid, TAKEPAYMENT_PASSWORD: pw } = process.env;
	let amt = order.invoice_total;
	let orderid = order.invoice_id;
	let transactiontype = 'SALE';
	let currencycode = '826';
	let transactiondatetime = moment(order.createdAt).utc().format('YYYY-MM-DD HH:mm:ss Z');
	let orderdesc = 'KeepWell Mantras';
	let customername = `${user.first_name}${user.last_name ? ` ${user.last_name}` : ''}`;
	let emailaddress = user.email;
	let address1 = '';
	let address2 = '';
	let address3 = '';
	let address4 = '';
	let city = '';
	let state = '';
	let postcode = '';
	let countrycode = '';
	let callbackurl = `${process.env.APP_HOME}/dashboard`;
	let resultdeliverymethod = 'SERVER';
	let serverresulturl = `${process.env.APP_API}/order/response`;
	let PaymentFormDisplaysResult = 'false';
	let pt = `PreSharedKey=${presharedkey}&MerchantID=${mid}&Password=${pw}&Amount=${amt}&CurrencyCode=${currencycode}&OrderID=${orderid}&TransactionType=${transactiontype}&TransactionDateTime=${transactiondatetime}&CallbackURL=${callbackurl}&OrderDescription=${orderdesc}&CustomerName=${customername}&Address1=${address1}&Address2=${address2}&Address3=${address3}&Address4=${address4}&City=${city}&State=${state}&PostCode=${postcode}&CountryCode=${countrycode}&EmailAddress=${emailaddress}&ResultDeliveryMethod=${resultdeliverymethod}&ServerResultURL=${serverresulturl}&PaymentFormDisplaysResult=${PaymentFormDisplaysResult}`;

	log.debug(pt);

	const hash = crypto.createHash('sha1');
	hash.update(pt);
	let hashesText = hash.digest('hex');

	log.debug(hashesText);
	return hashesText;

}

function generateDescription(recorded_classes, live_classes, diet_package, extension) {
	let str = '';
	recorded_classes.forEach((e, i) => {
		str += `${e.category_id.name} X ${e.count}`;
		if (i !== (recorded_classes.length - 1) || live_classes.count) {
			str += ', ';
		}
	});

	if (live_classes.count) {
		str += `Live Classes X ${live_classes.count} `;
	}

	if (diet_package && diet_package.plan === '6_WEEKS') {
		str += '6 Weeks nutrition support plan';
	}

	if (diet_package && diet_package.plan === '60_MINS') {
		str += '60 minutes consultation call for nutrition support';
	}

	if (extension) {
		str += `extension of ${extension}`;
	}

	return str;
}

async function assignPoints(user) {
	let orders = await orderModel.find({ user: user, status: 'COMPLETED' }).populate('user', 'referred_by');
	if (orders.length === 1) {
		let o = orders[0];
		await userModel.findOneAndUpdate({ _id: o.user._id }, { $inc: { points: 50 } });
		await userModel.findOneAndUpdate({ _id: o.user.referred_by }, { $inc: { points: 100 } });
	}
}

async function verifyHash(req, res, next) {
	let { TAKEPAYMENT_PRESHARED_KEY: presharedkey, TAKEPAYMENT_MID: mid, TAKEPAYMENT_PASSWORD: pw } = process.env;

	const message = req.body.Message;
	const previousstatuscode = req.body.PreviousStatusCode;
	const previousmessage = req.body.PreviousMessage;
	const StatusCode = req.body.StatusCode;
	const crossreference = req.body.CrossReference;
	const amount = req.body.Amount;
	const currencycode = req.body.CurrencyCode;
	const orderid = req.body.OrderID;
	const transactiontype = req.body.TransactionType;
	const transactiondatetime = req.body.TransactionDateTime;
	const orderdescription = req.body.OrderDescription;
	const customername = req.body.CustomerName;
	const address1 = req.body.Address1;
	const address2 = req.body.Address2;
	const address3 = req.body.Address3;
	const address4 = req.body.Address4;
	const city = req.body.City;
	const state = req.body.State;
	const postcode = req.body.PostCode;
	const EmailAddress = req.body.EmailAddress;
	const HashDigest = req.body.HashDigest;

	const countrycode = req.body.CountryCode;

	const pt = `PreSharedKey=${presharedkey}&MerchantID=${mid}&Password=${pw}&StatusCode=${StatusCode}&Message=${message}&PreviousStatusCode=${previousstatuscode}&PreviousMessage=${previousmessage}&CrossReference=${crossreference}&Amount=${amount}&CurrencyCode=${currencycode}&OrderID=${orderid}&TransactionType=${transactiontype}&TransactionDateTime=${transactiondatetime}&OrderDescription=${orderdescription}&CustomerName=${customername}&Address1=${address1}&Address2=${address2}&Address3=${address3}&Address4=${address4}&City=${city}&State=${state}&PostCode=${postcode}&CountryCode=${countrycode}&EmailAddress=${EmailAddress}`;

	log.debug('string to hash', pt);

	const hash = crypto.createHash('sha1');
	hash.update(pt);

	let calculated_hash = hash.digest('hex');

	log.debug('calculated hash: ', calculated_hash);

	if (HashDigest !== calculated_hash) {
		log.debug('failed to match hash');
		return res.send('StatusCode=5&Message=Unable to process – more detail on the error');
	}
	next();
}

async function processPurchaseOrder(order) {
	await orderModel.findOneAndUpdate({ invoice_id: order.invoice_id }, { $set: { status: 'COMPLETED' } });

	let enrollments = [];
	let items = [];
	let total_session_count = 0;

	if (order.live_classes && order.live_classes.count) {
		total_session_count += order.live_classes.count;
		items.push({
			name: 'Live Classes',
			qty: order.live_classes.count,
			total: order.live_classes.count * order.live_classes.price_per_class
		});
		enrollments.push({
			user: order.user._id,
			order_id: order._id,
			type: 'LIVE',
			class_count: order.live_classes.count,
			valid_till: moment().add(6, 'week')
		});
	}

	if (order.recorded_classes && order.recorded_classes.length) {
		for (let i = 0; i < order.recorded_classes.length; i++) {
			let e = order.recorded_classes[i];
			total_session_count += e.count;
			items.push({
				name: e.category_id.name,
				qty: e.count,
				total: e.count * e.price_per_class
			});
			enrollments.push({
				user: order.user._id,
				order_id: order._id,
				category_id: e.category_id._id,
				type: 'RECORDED',
				classes: await assignClasses(order.user._id, e.category_id._id, e.count),
				class_count: e.count,
				valid_till: moment().add(6, 'week')
			});
		}
	}

	if (order.user.referred_by && total_session_count >= 15) {
		assignPoints(order.user._id);
	}

	if (order.diet_package.plan) {

		let {
			mobile, first_name, last_name, email, city, country, age, gender, weight,
			height, activity_level, preferred_day, preferred_time, selected_plan, message
		} = order.diet_package.details;

		enrollments.push({
			user: order.user._id,
			order_id: order._id,
			type: order.diet_package.plan,
			details: order.diet_package.details,
			valid_till: moment().add(6, 'week')
		});

		items.push({
			name: selected_plan,
			qty: 1,
			total: order.diet_package.price
		});

		try {
			let temp = await send(
				[process.env.SES_EMAIL_NOREPLY],
				'Nutrition Support Plan contact request',
				`<div>
				E-Mail = ${email}<br/>
				Name = ${first_name} ${last_name}<br/>
				Phone = ${mobile}<br/>
				City = ${city}<br/>
				Country = ${country}<br/>
				Age = ${age}<br/>
				Gender = ${gender}<br/>
				Weight = ${weight}<br/>
				Activity level = ${activity_level}<br/>
				Height = ${height}<br/>
				Preferred day = ${preferred_day}<br/>
				Preferred time = ${preferred_time}<br/>
				Selected plan = ${selected_plan}<br/>
				Message = ${message}<br/>
				</div>
			`,
				'HTML',
				process.env.SES_EMAIL_NOREPLY
			);
			log.debug(temp);
		} catch (err) {
			log.error(err);
		}
	}

	log.debug(util.inspect(enrollments, false, 5));

	await enrollmentModel.create(enrollments);

	let u = order.user;
	let total = order.invoice_total / 100;
	let discount = order.promo_code;

	invoiceEmail(u.email, `${u.first_name} ${u.last_name}`, items, total, discount ? discount.discount : 0, discount ? discount.class_type : '', moment(order.valid_till).add(6, 'weeks').utcOffset('+01:00').format('lll'));

	log.debug('order completed successfully');
	log_order_confirmed(order.user._id, order._id, generateDescription(order.recorded_classes, order.live_classes, order.diet_package));
}

async function processExtensionOrder(order) {

	await orderModel.findOneAndUpdate({ invoice_id: order.invoice_id }, { $set: { status: 'COMPLETED' } });
	const enrollment = await enrollmentModel.findOne({ _id: order.extension.enrollment_id }).populate('category_id');

	let items = [{
		name: enrollment.type === 'LIVE' ? 'Extension of live sessions expiry date' : `Extension of ${enrollment.category_id.name} workout videos expiry date`,
		qty: order.extension.total_classes,
		total: order.invoice_total / 100
	}];

	enrollment.valid_till = (moment(enrollment.valid_till).isBefore(moment()) ? moment() : moment(enrollment.valid_till)).add(6, 'weeks').toDate();
	enrollment.extended = true;

	await enrollment.save();

	let u = order.user;
	let total = order.invoice_total / 100;

	invoiceEmail(u.email, `${u.first_name} ${u.last_name}`, items, total, 0, '', moment(enrollment.valid_till).utcOffset('+01:00').format('lll'));

	log.debug('order completed successfully');
	log_order_confirmed(order.user._id, order._id, generateDescription(order.recorded_classes, order.live_classes, order.diet_package));
}

router.post('/response', verifyHash, async (req, res) => {
	try {

		const orderid = req.body.OrderID;
		const StatusCode = req.body.StatusCode;
		const message = req.body.Message;
		log.debug(req.body);


		const order = await orderModel.findOne({ invoice_id: orderid })
			.populate('user', 'email first_name last_name referred_by')
			.populate('recorded_classes.category_id', 'name')
			.populate('promo_code', 'class_type discount');

		if (!order) {
			log.fatal('take payments hook called for non existent order.');
			return res.send('StatusCode=5&Message=order doesn\'t exist');
		}

		switch (StatusCode) {
			case '0':
				if (order.type === 'PURCHASE') {
					await processPurchaseOrder(order);
				} else {
					await processExtensionOrder(order);
				}
				break;
			case '3':
				await orderModel.findOneAndUpdate({ invoice_id: orderid }, { $set: { status: 'INCOMPLETE', pg_message: message } });
				break;
			case '4':
				await orderModel.findOneAndUpdate({ invoice_id: orderid }, { $set: { status: 'INCOMPLETE', pg_message: message } });
				break;
			case '5':
				await orderModel.findOneAndUpdate({ invoice_id: orderid }, { $set: { status: 'FAILED', pg_message: message } });
				break;
			case '30':
				await orderModel.findOneAndUpdate({ invoice_id: orderid }, { $set: { status: 'FAILED', pg_message: message } });
				break;
		}

		return res.send('StatusCode=0&Message=result processed successfully');
	} catch (err) {
		log.error(err);
		return res.send('StatusCode=5&Message=Unable to process – more detail on the error');
	}
});

router.use(jwtVerify);

function twoDigitRand() {
	let num = Math.ceil((Math.random() * 10000) % 100);
	return num > 10 ? num : `0${num}`;

}

router.route('/')
	.get([
		query('invoice_id')
			.exists({ checkFalsy: true, checkNull: true })
			.withMessage('Invoice id is required')
	], checkError, async (req, res) => {
		try {
			let order = await orderModel.findOne({ invoice_id: req.query.invoice_id, user: req.user._id });
			if (!order) return error(res, 404, 'Order not found');

			success(res, order.status, 200);

		} catch (err) {
			log.error(err);
			error(res);
		}
	})
	.post([
		body('live_classes_count')
			.optional()
			.isInt({ min: 1 })
			.withMessage('live classes count must be a integer'),
		body('recorded_classes')
			.isArray({ min: 0 })
			.withMessage('recorded classes must be a array'),
		body('recorded_classes.*.quantity')
			.isInt({ min: 1 })
			.withMessage('you must buy at least 1 class from the category'),
		body('recorded_classes.*.category_id')
			.isMongoId()
			.withMessage('category id is required and should be a mongo id'),
		body('promo_code')
			.optional()
			.isString()
			.withMessage('promo code must be a string'),
		body('diet_plan')
			.optional()
			.isString()
			.isIn(['6_WEEKS', '60_MINS'])
			.withMessage('diet plan must be a string')
	], checkError, async (req, res) => {
		try {

			const { live_classes_count = 0, recorded_classes = [], promo_code = '', diet_plan = '', details } = req.body;

			if (!live_classes_count && !(recorded_classes.length) && !diet_plan) {
				return error(res, 400, 'Please select at least 1 live class or workout videos or a diet plan');
			}

			let recorded_classes_count = 0;
			let recorded_classes_total_price = 0;
			let obj = {};
			let categories = [];

			recorded_classes.forEach(e => {
				obj[e.category_id] = e.quantity;
				recorded_classes_count += e.quantity;
				categories.push(new mongoose.Types.ObjectId(e.category_id));
			});

			// check that category ids are valid and enough classes are available to be assigned
			if (categories.length) {

				categories = await categoryModel.aggregate([
					{
						$match: {
							_id: {
								$in: categories
							}
						}
					}, {
						$lookup: {
							from: 'enrollments',
							let: { cid: '$_id', uid: req.user._id },
							as: 'available_classes',
							pipeline: [
								{
									$match: {
										$expr: {
											$and: [
												{
													$eq: ['$category_id', '$$cid']
												}, {
													$eq: ['$user', '$$uid']
												}
											]
										}
									}
								}, {
									$project: {
										classes: {
											$filter: {
												input: '$classes',
												as: 'c',
												cond: {
													$or: [
														{
															$ifNull: ['$$c.watch_start', true]
														}, {
															$lt: ['$$c.watch_start', moment().subtract(1, 'd').toDate()]
														}
													]
												}
											}
										}
									}
								}, {
									$group: {
										_id: null,
										classes: {
											$sum: { $size: '$classes' }
										}
									}
								}, {
									$project: {
										count: '$classes',
										_id: 0
									}
								}
							]
						}
					}, {
						$unwind: {
							path: '$available_classes',
							preserveNullAndEmptyArrays: true
						}
					}
				]);

				log.debug(categories);

				log.debug(recorded_classes);

				if (categories.length !== recorded_classes.length) {
					return error(res, 400, 'One or more category ids are invalid');
				}

				let arr = [];

				for (let i = 0; i < categories.length; i++) {
					let e = categories[i];
					let quantity = obj[e._id.toString()];
					recorded_classes_total_price += quantity * e.price_per_class;
					let max = e.classes.length;

					if (e.available_classes && e.available_classes.count) {
						if (max < e.available_classes.count + quantity)
							return error(res, 400, `You cannot purchase more than ${max - e.available_classes.count} class of ${e.name}. Since you have already purchased ${e.available_classes.count} out of total ${max} classes.`);
					} else {
						if (max < quantity)
							return error(res, 400, `You cannot purchase more classes than available for ${e.name} category.`);
					}

					arr.push({
						...e,
						quantity
					});
				}

				categories = arr;

			}

			// check that a user should not be able to buy 6 week diet plan 2 times
			if (diet_plan === '6_WEEKS') {
				let enrollment = await enrollmentModel.findOne({ user: req.user._id, type: '6_WEEKS' });
				if (enrollment) return error(res, 400, 'You are already enrolled in a 6 week nutrition support plan');
			}

			let total_class_count = live_classes_count + recorded_classes_count;
			let live_class_discount = 0;
			let recorded_class_discount = 0;
			let diet_plan_discount = 0;
			let diet_price = 0;
			let promoCode;
			let diet_package = null;

			if (promo_code) {
				promoCode = await promoCodeModel.findOne({ name: promo_code, valid_upto: { $gt: new Date() }, valid_from: { $lte: new Date() } });
				if (!promoCode) return error(res, 400, 'Invalid promo code');
				if (promoCode.user && !promoCode.user.toString() !== req.user._id.toString()) {
					return error(res, 400, 'Invalid promo code');
				}

				switch (promoCode.class_type) {
					case 'LIVE':
						if (promoCode.min_class_count <= live_classes_count && promoCode.max_class_count >= live_classes_count) {
							live_class_discount = promoCode.discount;
						} else return error(res, 400, `This promo code cannot be applied. Promo code can be applied on live classes only if total live classes are between ${promoCode.min_class_count} and ${promoCode.max_class_count}.`);
						break;
					case 'RECORDED':
						if (promoCode.min_class_count <= recorded_classes_count && promoCode.max_class_count >= recorded_classes_count) {
							recorded_class_discount = promoCode.discount;
						} else return error(res, 400, `This promo code cannot be applied. Promo code can be applied on workout videos only if total workout videos are between ${promoCode.min_class_count} and ${promoCode.max_class_count}.`);
						break;
					case '6_WEEKS':
						if (diet_plan !== '6_WEEKS') {
							return error(res, 400, 'This promo code can be applied only on 6 week diet package plan');
						}
						diet_plan_discount = promoCode.discount;
						break;
					case '60_MINS':
						if (diet_plan !== '60_MINS') {
							return error(res, 400, 'This promo code can be applied only on 60 minutes diet consultation call');
						}
						diet_plan_discount = promoCode.discount;
						break;
					default:
						if (promoCode.min_class_count <= total_class_count && promoCode.max_class_count >= total_class_count) {
							live_class_discount += promoCode.discount;
							recorded_class_discount += promoCode.discount;
						} else return error(res, 400, `This promo code cannot be applied. Promo code can be applied only if total classes are between ${promoCode.min_class_count} and ${promoCode.max_class_count}.`);
				}

			}

			if (diet_plan === '60_MINS') {
				diet_price = _60_min_consultation_price;
				diet_package = {
					plan: diet_plan,
					price: diet_price,
					details
				};
			} else if (diet_plan === '6_WEEKS') {
				diet_price = _6_week_diet_price;
				diet_package = {
					plan: diet_plan,
					price: diet_price,
					details
				};
			}

			let invoice_total = ((live_classes_count * live_class_price) * (100 - live_class_discount) + recorded_classes_total_price * (100 - recorded_class_discount) + diet_price * (100 - diet_plan_discount)) / 100;
			let invoice_id = `${twoDigitRand()}${twoDigitRand()}${twoDigitRand()}${twoDigitRand()}`;

			let order_obj = {
				user: req.user._id,
				live_classes: {
					count: live_classes_count,
					price_per_class: live_class_price
				},
				recorded_classes: categories.map(e => ({
					category_id: e._id,
					count: e.quantity,
					price_per_class: e.price_per_class
				})),
				invoice_id,
				invoice_total: invoice_total * 100,
				live_class_discount,
				recorded_class_discount,
				diet_plan_discount,
				type: 'PURCHASE'
			};

			if (diet_package) {
				order_obj.diet_package = diet_package;
			}

			if (promoCode) order_obj.promo_code = promoCode._id;

			await orderModel.create(order_obj);

			let order = await orderModel.findOne({ invoice_id }).populate('promo_code').populate('recorded_classes.category_id', 'name');

			let pi = createHash(order, req.user);

			return success(res, { ...order.toObject(), pi: pi });

		} catch (err) {
			log.error(err);
			return error(res);
		}
	});

router.post('/extension', async (req, res) => {
	try {

		const { enrollment_id } = req.body;

		if (!enrollment_id) {
			return error(res, 400, 'Please select at least 1 live class or workout videos or a diet plan');
		}

		const enrollment = await enrollmentModel.findOne({ _id: enrollment_id }).populate('category_id', 'name');

		if (!enrollment) {
			return error(res, 404, 'Enrollment not found');
		}

		if (enrollment && enrollment.extended) {
			return error(res, 400, 'This enrollment has already been extended once, and cannot be extended again');
		}

		if (moment(enrollment.valid_till).isBefore(moment().subtract(2, 'weeks'))) {
			return error(res, 400, 'This enrollment has been expired for 2 weeks and cannot be extended now.');
		}

		log.debug(enrollment);

		let per_class_price = 0;
		let left_classes = 0;

		if (enrollment.type === 'LIVE') {
			per_class_price = live_class_price;
			left_classes = enrollment.class_count - enrollment.classes.length;
		} else if (enrollment.type === 'RECORDED') {
			const category = await categoryModel.findOne({ _id: enrollment.category_id });
			if (!category) return error(res, 400, 'These classes cannot be extended');
			per_class_price = category.price_per_class;
			left_classes = enrollment.classes.reduce((p, e) => e.watch_start ? p : p + 1, 0);
		} else {
			return error(res, 400, 'Invalid enrollment id');
		}

		let invoice_total = per_class_price * left_classes * parseInt(process.env.ENROLLMENT_EXTENSION_PERCENTAGE_COST, 10) / 100;
		let invoice_id = `${twoDigitRand()}${twoDigitRand()}${twoDigitRand()}${twoDigitRand()}`;

		let order_obj = {
			user: req.user._id,
			invoice_id,
			invoice_total: invoice_total * 100,
			extension: {
				enrollment_id: enrollment._id,
				total_classes: left_classes,
				price_per_class: per_class_price * parseInt(process.env.ENROLLMENT_EXTENSION_PERCENTAGE_COST, 10) / 100,
				title: enrollment.type === 'LIVE' ? `Extension of remaining ${left_classes} live sessions` : `Extension of remaining ${left_classes} ${enrollment.category_id.name} workout videos`
			},
			type: 'EXTENSION'
		};

		log.debug(order_obj);

		await orderModel.create(order_obj);

		let order = await orderModel.findOne({ invoice_id });

		let pi = createHash(order, req.user);

		return success(res, { ...order.toObject(), pi: pi });

	} catch (err) {
		log.error(err);
		return error(res);
	}
});

module.exports = router;
