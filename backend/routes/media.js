var express = require('express');
var crypto = require('crypto');
var base64 = require('base-64');

var router = express.Router();
var { success, error } = require('../response');
const log = require('../logs');
const jwtVerify = require('../jwtVerify');

function pad(num) {
	return String(num).padStart(2, '0');
}

function getImageUploadPolicyStringPublicBucket(user) {
	let date = new Date();
	let date_str = `${date.getFullYear()}${pad(date.getMonth() + 1)}${pad(date.getDate())}`;
	let policy =
		`{ "expiration": "${new Date(date.valueOf() + 5 * 60 * 1000).toISOString()}",
	"conditions": [
	  {"bucket": "${process.env.AWS_BUCKET_PUBLIC}"},
	  ["starts-with", "$key", "${user._id.toString()}"],
	  {"acl": "public-read"},
	  {"x-amz-credential": "${process.env.AWS_ACCESS_KEY_ID}/${date_str}/${process.env.AWS_REGION}/s3/aws4_request"},
	  {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
	  {"x-amz-date": "${date.getUTCFullYear()}${pad(date.getUTCMonth() + 1)}${pad(date.getUTCDate())}T${pad(date.getUTCHours())}${pad(date.getUTCMinutes())}${pad(date.getUTCSeconds())}Z"},
	  ["starts-with", "$Content-Type", "image/"]
	]
  }`;


	log.debug(policy);
	let policy_str = base64.encode(policy);
	// generate signing key
	let arr = [date_str, process.env.AWS_REGION, 's3', 'aws4_request'];
	let key = `AWS4${process.env.AWS_SECRET_ACCESS_KEY}`;

	for (let i = 0; i < arr.length; i++) {
		let hash = crypto.createHmac('sha256', key);
		hash.update(arr[i]);
		key = hash.digest();
	}

	let hash = crypto.createHmac('sha256', key);
	hash.update(policy_str);
	return ({
		'x-amz-signature': hash.digest('hex'),
		policy: policy_str,
		'x-amz-credential': `${process.env.AWS_ACCESS_KEY_ID}/${date_str}/${process.env.AWS_REGION}/s3/aws4_request`,
		'x-amz-algorithm': 'AWS4-HMAC-SHA256',
		'x-amz-date': `${date.getUTCFullYear()}${pad(date.getUTCMonth() + 1)}${pad(date.getUTCDate())}T${pad(date.getUTCHours())}${pad(date.getUTCMinutes())}${pad(date.getUTCSeconds())}Z`
	});
}

function getFileUploadPolicyStringPublicBucket(user) {
	let date = new Date();
	let date_str = `${date.getFullYear()}${pad(date.getMonth() + 1)}${pad(date.getDate())}`;
	let policy =
		`{ "expiration": "${new Date(date.valueOf() + 5 * 60 * 1000).toISOString()}",
	"conditions": [
	  {"bucket": "${process.env.AWS_BUCKET_PUBLIC}"},
	  ["starts-with", "$key", "${user._id.toString()}/diet"],
	  {"acl": "public-read"},
	  {"x-amz-credential": "${process.env.AWS_ACCESS_KEY_ID}/${date_str}/${process.env.AWS_REGION}/s3/aws4_request"},
	  {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
	  {"x-amz-date": "${date.getUTCFullYear()}${pad(date.getUTCMonth() + 1)}${pad(date.getUTCDate())}T${pad(date.getUTCHours())}${pad(date.getUTCMinutes())}${pad(date.getUTCSeconds())}Z"}
	]
  }`;


	log.debug(policy);
	let policy_str = base64.encode(policy);
	// generate signing key
	let arr = [date_str, process.env.AWS_REGION, 's3', 'aws4_request'];
	let key = `AWS4${process.env.AWS_SECRET_ACCESS_KEY}`;

	for (let i = 0; i < arr.length; i++) {
		let hash = crypto.createHmac('sha256', key);
		hash.update(arr[i]);
		key = hash.digest();
	}

	let hash = crypto.createHmac('sha256', key);
	hash.update(policy_str);
	return ({
		'x-amz-signature': hash.digest('hex'),
		policy: policy_str,
		'x-amz-credential': `${process.env.AWS_ACCESS_KEY_ID}/${date_str}/${process.env.AWS_REGION}/s3/aws4_request`,
		'x-amz-algorithm': 'AWS4-HMAC-SHA256',
		'x-amz-date': `${date.getUTCFullYear()}${pad(date.getUTCMonth() + 1)}${pad(date.getUTCDate())}T${pad(date.getUTCHours())}${pad(date.getUTCMinutes())}${pad(date.getUTCSeconds())}Z`
	});
}

router.use(jwtVerify);

/* upload images to S3 */
router.get('/image_signed_url', async function (req, res) {
	try {
		return success(res, getImageUploadPolicyStringPublicBucket(req.user));
	} catch (err) {
		log.error(err);
		error(res);
	}
});

/* upload files(word, excel, pdf) to S3 */
router.get('/file_signed_url', async function (req, res) {
	try {
		return success(res, getFileUploadPolicyStringPublicBucket(req.user));
	} catch (err) {
		log.error(err);
		error(res);
	}
});

router.get('/admin/file_signed_url', async function (req, res) {
	try {
		if (!req.user.admin) return error(res, 401, 'Unauthorized');
		let { _id } = req.query;
		return success(res, getFileUploadPolicyStringPublicBucket({ _id }));
	} catch (err) {
		log.error(err);
		error(res);
	}
});

module.exports = router;
