const activityModel = require('./models/activity_log');
var log = require('./logs');

module.exports.log_video_view = async function (user, class_id, enrollment, video_type, video_name) {
	try {
		await activityModel.create({
			user, class_id, enrollment, video_type, video_name, event_type: 'WATCH_VIDEO'
		});
	} catch (err) {
		log.error(err);
	}
};

module.exports.log_order_confirmed = async function (user, order_id, description) {
	try {
		await activityModel.create({
			user, order_id, description, event_type: 'ORDER'
		});
	} catch (err) {
		log.error(err);
	}
};
