const { validationResult } = require('express-validator');
var log = require('./logs');

function success(res, result = 'OK', status = 200) {
	res.status(status).json({ status: status, success: true, response: result });
}

function error(res, status = 500, error = 'Some internal server error occurred') {
	res.status(status).json({ status: status, success: false, response: error });
}

function checkError(req, res, next) {
	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		log.debug(errors);
		return error(res, 422, errors.array()[0].msg || 'invalid parameters');
	}

	next();
}

function checkDisabled(req, res, next) {
	if (req.user.disabled) return error(res, 402, 'Your account has been disabled. Please contact support.');
	next();
}

module.exports = { success, error, checkError, checkDisabled };
