import React, { useEffect, useState } from "react";
import { useLocation, Route, Switch, Redirect } from "react-router-dom";
import { Button, Container, Modal, ModalBody, ModalFooter, Row } from "reactstrap";
import { useHistory } from 'react-router-dom';

import Footer from "../components/Footers/Footer.js";
import Sidebar from "../components/Sidebar/UserSidebar.js";

import UserNavBar from "../components/Navbars/UserNavBar.js";
import AuthNavbar from "../components/Navbars/AuthNavbar.js";
import AuthFooter from "../components/Footers/AuthFooter.js";

import Login from "../views/Auth/Login";
import Register from "../views/Auth/Register.js";
import Reset from "../views/Auth/ResetPassword";
import ForgotPassword from "../views/Auth/ForgotPassword";
import Home from "../views/Home/index.js";
import Contact from "../views/Contact/index.js";
import About from "../views/About/index.js";
import FitnessMakeover from "../views/FitnessMakeovers/index.js";
import FitnessStars from "../views/FitnessStars/index.js";
import FitnessChallenges from "../views/FitnessChallenges";
import Privacy from "../views/PrivacyPolicy/index.js";
import TermsAndConditions from "../views/TermsAndConditions/index.js";
import UserDashboard from "../views/UserDashboard.js";
import Watch from "../views/Watch";
import LiveClasses from "../views/LiveClasses/LiveClasses.js";
import Profile from "../views/Profile";
import { useSelector } from "react-redux";
import Recordings from "../views/Recordings/index.js";
import Cart from "../views/Cart/index.js";
import Support from "../views/Support/index.js";
import Classes from "../views/enrollment/index.js";
import MyClasses from "../views/MyClasses/index.js";
import AddClass from "../views/Admin/AddClass/index.js";
import AllClasses from "../views/Admin/AllClasses/index.js";
import AdminNavbar from "../components/Navbars/AdminNavBar";
import Tour from "reactour";
import { useCookies } from "react-cookie";
import DietPackage from "../views/DietPackage/index.js";
import SixWeekNSPEnrollments from "../views/Admin/6WeekEnrollments/index.js";
import SixtyMinNSPCall from "../views/Admin/60MinEnrollments/index.js";
import Testimonial from "../views/Testimonial/index.js";
import Packages from "../views/Packages/index.js";
import Services from "../views/Services/index.js";
import ClassesDetail from "../views/Classes/index.js";
import ClientTransfomation from "../views/ClientTransfomation/index.js";

const Wrapper = ({ Component, mainContent }) => {

	useEffect(() => {
		document.body.classList.add("bg-default");
		return () => {
			document.body.classList.remove("bg-default");
		};
	}, []);

	return (
		<>
			<div className="main-content" ref={mainContent}>
				<AuthNavbar />
				<div className="header bg-gradient-info py-7 py-lg-8">
					<div className="separator separator-bottom separator-skew zindex-100">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							preserveAspectRatio="none"
							version="1.1"
							viewBox="0 0 2560 100"
							x="0"
							y="0"
						>
							<polygon
								className="fill-default"
								points="2560 0 2560 100 0 100"
							/>
						</svg>
					</div>
				</div>
				{/* Page content */}
				<Container className="mt--8 pb-5">
					<Row className="justify-content-center">
						<Component />
					</Row>
				</Container>
			</div>
			<AuthFooter />
		</>
	)
}

const Wrapper2 = ({ Component, mainContent }) => {
	return (
		<div className='h-100 w-100' style={{ background: '#fafafa' }} ref={mainContent}>
			<Component />
		</div>
	)
}

const AuthWrapper = ({ Component, mainContent }) => {
	const { _id } = useSelector(st => st.user);

	if (!_id) return <Redirect to='/' />

	return (
		<React.Fragment>
			<Sidebar
				logo={{
					innerLink: "/",
					imgSrc: `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo-keepwell.png`,
					imgAlt: "Keep Well Mantras",
				}}
			/>
			<div className="main-content" ref={mainContent}>
				<UserNavBar />
				<Component />
				<Container fluid>
					<Footer />
				</Container>
			</div>
		</React.Fragment>
	)
}

const AuthAdminWrapper = ({ Component, mainContent }) => {
	const { _id } = useSelector(st => st.user);

	if (!_id) return <Redirect to='/' />

	return (
		<React.Fragment>
			<Sidebar
				logo={{
					innerLink: "/",
					imgSrc: `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo-keepwell.png`,
					imgAlt: "Keep Well Mantras",
				}}
			/>
			<div className="main-content" ref={mainContent}>
				<AdminNavbar />
				<Component />
				<Container fluid>
					<Footer />
				</Container>
			</div>
		</React.Fragment>
	)
}

const steps = [{
	selector: '.stats-card-1',
	content: 'Click here to update your current weight on your profile. Update every 2 weeks!'
}, {
	selector: '.stats-card-2',
	content: 'Click here to update your current waist on your profile. Update every 2 weeks!'
}, {
	selector: '.stats-card-3',
	content: 'The no. of live classes available out of total booked. Click here to book and attend the LIVE classes & their recordings from the calendar'
}, {
	selector: '.stats-card-4',
	content: 'The no. of workout recordings available out of total booked. Click here to book and access your workouts'
}, {
	selector: '.live-calendar',
	content: 'The calendar shows all current and upcoming live classes. You can either join the classes live or watch their recordings. No prior booking needed'
}, {
	selector: '.live-class-checkout',
	content: 'Click here to go to your cart and buy live classes'
}, {
	selector: '.cat-card',
	content: 'You can create your own package by selecting from a list of goal specific workouts and adding them to your cart.  If you need help in creating a package, get in touch.'
}, {
	selector: '.recorded-class-checkout',
	content: 'Use this button to check out and buy your selection of workout videos'
}, {
	selector: '.my-recorded-class',
	content: 'Click here to see the list of all workout videos purchased'
}, {
	selector: '.nav-profile',
	content: 'Click here to view and update your profile and a track of your activity'
}, {
	selector: '.nav-contact',
	content: 'Click here to contact us for any further support'
}, {
	selector: '.nav-tour',
	content: 'To take this tour again, click here!'
}];

const UserLayout = () => {

	const mainContent = React.useRef(null);
	const location = useLocation();


	const { admin, _id, first_name, last_name } = useSelector(st => st.user);


	React.useEffect(() => {
		document.documentElement.scrollTop = 0;
		document.scrollingElement.scrollTop = 0;
		if (mainContent?.current)
			mainContent.current.scrollTop = 0;
	}, [location]);


	const [cookie, setCookie] = useCookies(['tour']);
	const [step, setStep] = useState(0);
	let history = useHistory();

	function next() {
		switch (step) {
			case 4:
				history.push('/live-sessions');
				break;
			case 6:
				history.push('/recorded-sessions');
				break;
			default:
				break;
		}
		setStep(step + 1);
	}

	function end() {
		setStep(0);
		setCookie('tour', 'completed', { maxAge: 10 * 365 * 24 * 3600 })
	}

	return (
		<>

			<Switch>

				{/* un authenticated */}
				<Route path='/dashboard' exact>
					<AuthWrapper Component={UserDashboard} />
				</Route>
				<Route path={['/', '/home']} exact>
					<Home />
				</Route>
				<Route path='/register' exact>
					{
						_id ?
							<AuthWrapper Component={UserDashboard} />
							:
							<Wrapper Component={Register} mainContent={mainContent} />
					}
				</Route>
				<Route path='/reset_pass' exact>
					{
						_id ?
							<AuthWrapper Component={UserDashboard} />
							:
							<Wrapper Component={Reset} mainContent={mainContent} />
					}
				</Route>
				<Route path='/forgot-password' exact>
					{
						_id ?
							<AuthWrapper Component={UserDashboard} />
							:
							<Wrapper Component={ForgotPassword} mainContent={mainContent} />
					}
				</Route>
				<Route path='/login' exact>
					{
						_id ?
							<AuthWrapper Component={UserDashboard} />
							:
							<Wrapper Component={Login} mainContent={mainContent} />
					}
				</Route>
				<Route path='/about' exact>
					<Wrapper2 Component={About} mainContent={mainContent} />
				</Route>
				<Route path='/privacy' exact>
					<Wrapper2 Component={Privacy} mainContent={mainContent} />
				</Route>
				<Route path='/terms' exact>
					<Wrapper2 Component={TermsAndConditions} mainContent={mainContent} />
				</Route>
				<Route path='/contact' exact>
					<Wrapper2 Component={Contact} mainContent={mainContent} />
				</Route>
				<Route path='/fitness-makeover' exact>
					<Wrapper2 Component={FitnessMakeover} mainContent={mainContent} />
				</Route>
				<Route path='/fitness-stars' exact>
					<Wrapper2 Component={FitnessStars} mainContent={mainContent} />
				</Route>
				<Route path='/fitness-challenges' exact>
					<Wrapper2 Component={FitnessChallenges} mainContent={mainContent} />
				</Route>
				<Route path='/testimonials' exact>
					<Wrapper2 Component={Testimonial} mainContent={mainContent} />
				</Route>
				<Route path='/packages' exact>
					<Wrapper2 Component={Packages} mainContent={mainContent} />
				</Route>
				<Route path='/services' exact>
					<Wrapper2 Component={Services} mainContent={mainContent} />
				</Route>
				<Route path='/classes' exact>
					<Wrapper2 Component={ClassesDetail} mainContent={mainContent} />
				</Route>
				<Route path='/client-transformation' exact>
					<Wrapper2 Component={ClientTransfomation} mainContent={mainContent} />
				</Route>

				{/* authenticated */}
				<Route path='/profile' exact>
					<AuthWrapper Component={Profile} mainContent={mainContent} />
				</Route>
				<Route path='/live-sessions' exact>
					<AuthWrapper Component={LiveClasses} mainContent={mainContent} />
				</Route>
				<Route path='/recorded-sessions' exact>
					<AuthWrapper Component={Recordings} mainContent={mainContent} />
				</Route>
				<Route path='/buy-classes' exact>
					<AuthWrapper Component={Cart} mainContent={mainContent} />
				</Route>
				<Route path='/my-classes' exact>
					<AuthWrapper Component={MyClasses} mainContent={mainContent} />
				</Route>
				<Route path='/watch/:id' exact>
					<AuthWrapper Component={Watch} mainContent={mainContent} />
				</Route>
				<Route path='/classes/:id' exact>
					<AuthWrapper Component={Classes} mainContent={mainContent} />
				</Route>
				<Route path='/contact-us' exact>
					<AuthWrapper Component={Support} mainContent={mainContent} />
				</Route>
				<Route path='/diet-package' exact>
					<AuthWrapper Component={DietPackage} mainContent={mainContent} />
				</Route>
				<Route path='/admin'>
					{
						admin ?
							<Switch>
								<Route path={['/admin/add-class', '/admin/add-class/:id']} exact>
									<AuthAdminWrapper Component={AddClass} mainContent={mainContent} />
								</Route>
								<Route path={'/admin/classes'} exact>
									<AuthAdminWrapper Component={AllClasses} mainContent={mainContent} />
								</Route>
								<Route path={'/admin/6-week-enrollments'} exact>
									<AuthAdminWrapper Component={SixWeekNSPEnrollments} mainContent={mainContent} />
								</Route>
								<Route path={'/admin/60-min-enrollments'} exact>
									<AuthAdminWrapper Component={SixtyMinNSPCall} mainContent={mainContent} />
								</Route>
							</Switch>
							:
							<AuthWrapper Component={UserDashboard} />
					}
				</Route>
				<Route path='*'>
					{
						_id ?
							<AuthWrapper Component={UserDashboard} />
							:
							<Home />
					}
				</Route>
			</Switch>

			<Tour
				steps={steps}
				isOpen={cookie?.tour === 'started' && (step === 0 ? history.location.pathname === '/dashboard' : true)}
				onRequestClose={end}
				nextStep={next}
				goToStep={step - 1}
				startAt={0}
				disableDotsNavigation={true}
				lastStepNextButton={<Button color='primary' size='sm' onClick={end}>End Tour</Button>}
				showNavigation={false}
			/>

			<Modal isOpen={history.location.pathname === '/dashboard' && !cookie?.tour} toggle={() => setCookie('tour', 'completed')}>
				<ModalBody>
					<p className='font-weight-500'>
						Thankyou for signing up {first_name} {last_name}! Welcome to your personal dashboard Fitness Stars. Do you wish to take a website tour?
					</p>
				</ModalBody>
				<ModalFooter>
					<Button color='danger' onClick={() => setCookie('tour', 'completed')}>No</Button>
					<Button
						color='primary'
						onClick={() => {
							history.push('/dashboard');
							setCookie('tour', 'started');
						}}
					>
						Yes
					</Button>
				</ModalFooter>
			</Modal>

		</>
	);
};

export default UserLayout;
