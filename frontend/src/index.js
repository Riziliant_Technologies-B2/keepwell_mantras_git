import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";

import App from './app';
import store from "./store";

ReactDOM.render(
	<Provider store={store}>
		<App />
		<ToastContainer
			position="top-right"
			autoClose={5000}
			hideProgressBar
			newestOnTop
			closeOnClick={true}
			rtl={false}
			pauseOnFocusLoss={false}
			draggable
			pauseOnHover
			closeButton={false}
		/>
	</Provider>,
	document.getElementById("root")
);
