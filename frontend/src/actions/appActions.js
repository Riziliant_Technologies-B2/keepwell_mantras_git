export const InitializeAction = (web_content) => ({
    type: 'INITIALIZE',
    payload: web_content
});