import React, { useState } from 'react';
import classnames from 'classnames';
// import './style.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import { Link } from "react-router-dom";
import { TabContent, TabPane, Row, Col, Container, NavLink } from 'reactstrap';


export default function Testimonial({ slides, className }) {

    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    return (
        <>
            <Header2 />

            <div class="services2-bg page-head parallax overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <h3>SERVICES</h3>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li><Link to="/">HOME</Link></li>
                                <li>।</li>
                                <li>SERVICES</li>
                            </ol>
                        </div>

                    </div>

                </div>

            </div>

            <div class="single-tab-area pad90">
                <Container>
                    <Row>
                        <Col lg="4" md="12">

                            <div class="single-tab">
                                <ul id="single-tab" class="nav nav-tabs single-tab-link">
                                    <li class="nav-item full-width">
                                        <NavLink
                                            className={classnames({ active: activeTab === '1' })}
                                            onClick={() => { toggle('1'); }}
                                        >
                                            My Bespoke Service
                                            </NavLink>
                                    </li>
                                    <li class="nav-item full-width">
                                        <NavLink
                                            className={classnames({ active: activeTab === '2' })}
                                            onClick={() => { toggle('2'); }}
                                        >
                                            Live Workouts
                                            </NavLink>
                                    </li>
                                    <li class="nav-item full-width">
                                        <NavLink
                                            className={classnames({ active: activeTab === '3' })}
                                            onClick={() => { toggle('3'); }}
                                        >
                                            Personalized Training
                                            </NavLink>
                                    </li>
                                    <li class="nav-item full-width">
                                        <NavLink
                                            className={classnames({ active: activeTab === '4' })}
                                            onClick={() => { toggle('4'); }}
                                        >
                                            Nutrition Plans
                                            </NavLink>
                                    </li>
                                    <li class="nav-item full-width">
                                        <NavLink
                                            className={classnames({ active: activeTab === '5' })}
                                            onClick={() => { toggle('5'); }}
                                        >
                                            Confidential Consultations
                                            </NavLink>
                                    </li>
                                </ul>
                            </div>


                            <div class="subscribe-box">
                                <div class="subs-title">
                                    <i class="lni lni-whatsapp" aria-hidden="true"></i>
                                    <p>Click the button below to connect with me</p>
                                    <div class="bttn">
                                        <a rel="noreferrer" target="_blank" class="btn active btn-primary" href='https://web.whatsapp.com/send?phone=447584236848&text=Hi%2C%20I%20am%20interested%20in%20knowing%20more%20and%20would%20like%20some%20information%2C%20I%20have%20been%20redirected%20from%20your%20website.'>
                                            Click Here
                                    </a>
                                    </div>
                                </div>
                            </div>

                        </Col>

                        <div class="col-lg-8 col-md-12">

                            <TabContent activeTab={activeTab}>
                                <TabPane tabId="1">
                                    <div class="services-content">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12">
                                                <div class="services-img">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Bespoke-service.png`} alt=" services img" />
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-12">
                                                <h5 class="mb300">My Bespoke Service</h5>
                                                <p class="quote mt21">Here is no one size fits all approach in my services. I offer a variety of options for us to pick and choose what is best suited to your goals and lifestyle</p>

                                                <p class="mt21">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Personal Training
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Nutrition &amp; Diet
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Sports Training
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Couple Training
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Corporate Sessions
                                                </p>

                                                <p class="mt21">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Weigth Loss Plans
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Strength Building
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Stamina Building
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Post Pregnancy Weigth Loss
                                                    <br /><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Online Workouts
                                                </p>
                                            </div>
                                        </div>


                                    </div>
                                </TabPane>
                                <TabPane tabId="2">
                                    <div class="services-content">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12">
                                                <div class="services-img">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/service-1.png`} alt=" services img" />
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-12">
                                                <div class="service-title">
                                                    <h5 class="mb300">Live Workouts</h5>
                                                    <p class="quote mt20">Be part of my fitness family and do live workouts with me ! If you struggle to stay motivated and lack discipline to your workout routine - this is just right for you!</p>

                                                    <p class="mt20"> <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Follow along my bodyweight workouts of high, medium and low intensity 6 days a week suited to your fitness levels. Duration varies from 30min to 45min. All you need to do it to join via zoom and just follow along.</p>

                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> If you cannot make it to the scheduled time - recordings of all my sessions are available for you to do at your own pace.</p>
                                                </div>
                                            </div>
                                            <div class="service-title">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">

                                                        <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Every day has a different focus and no two sessions are ever the same. Lots of new exercises added every day bringing in variations you can never imagine!</p>
                                                        <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> No age restriction as there is a less intense variation to each exercise. For any medical condition please do make me aware beforehand</p>
                                                        <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                            Online community of my motivated fitness tribe keeping the team spirits high! Plenty of bonus challenges , extra workouts , meal ideas and recipe sharing</p>
                                                        <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                            Flexibility to choose from monthly rolling packages or per session booking</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </TabPane>

                                <TabPane tabId="3">
                                    <div class="services-content">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12">
                                                <div class="services-img">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/service-2.png`} alt=" services img" />
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-12">
                                                <div class="service-title">
                                                    <h5 class="mb300">Personalized Training</h5>
                                                    <p class="quote mt20">Online or face to face, I will design bespoke plans as per your fitness goals.</p>

                                                    <p class="mt20"> <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" /> Personalized workout plans just perfect to suit on your fitness level and goals</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        Weekly workout schedules with regular check ins to progress your training</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        Nutrition guidance and diet plans to support your fitness transformation</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        My coaching throughout the process to empower you with the knowledge and understanding of staying in your healthiest self throughout your lifetime</p>
                                                    <p class="mt20">
                                                        <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        6 weeks and 12 weeks rolling plans available</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tabId="4">
                                    <div class="services-content">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12">
                                                <div class="services-img">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/service-3.png`} alt=" services img" />
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-12">
                                                <div class="service-title">
                                                    <h5 class="mb300">Nutrition Plans</h5>
                                                    <p class="quote mt20">Abs are indeed made in the kitchen! Your diet contributes a lot towards your current fitness and your future health.</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        Personalized nutrition plans as per your lifestyle and food choices</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        My diet plans will cater to any underlying medical condition you may have which will be discussed in detail beforehand</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        Close monitoring of food groups and macros required for your transformation</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        I do not recommend or advice on any diet supplements or meal replacement products. I encourage healthy and balanced eating habits</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        6 weeks or 12 weeks collaboration plans to choose from</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tabId="5">
                                    <div class="services-content">
                                        <div class="row">
                                            <div class="col-md-5 col-sm-12">
                                                <div class="services-img">
                                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/service-4.png`} alt=" services img" />
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-12">
                                                <div class="service-title">
                                                    <h5 class="mb300">Confidential Consultations</h5>
                                                    <p class="quote mt20">If you are struggling to strike a balance with your health and work / home responsibilities, a confidential discussion can be a game changer</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        A face to face / telephonic / zoom consultation can be booked to discuss about your struggles</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        A 60 min consultation can be booked where we will discuss your present and past medical information, fitness routine, nutrition, lifestyle, fitness goals and any health matter you wish to discuss.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="service-title">
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        I will guide you how with those little changes in your lifestyle, you can greatly improve your health. Being a mom I understand the struggles and can help you design an easy yet effective fitness plan which will be adaptable and sustainable</p>
                                                    <p class="mt20"><img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/tick-icon.png`} width="20" height="20" alt=" services img" />
                                                        The discussion will stay confidential with an aim to help you enhance your understanding on healthy living, managing stress, increasing energy levels, mindful eating, sleep, hydration, hormonal changes, weight fluctuations, post pregnancy body changes and any wellness topic you wish to have my support in</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPane>
                            </TabContent>
                        </div>
                    </Row>
                </Container>
            </div>


            <Footer />
        </>
    )

}