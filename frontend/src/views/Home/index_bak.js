import React from 'react';
import './styles.css';
import Testimonial from '../Testimonial';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import { Link } from 'react-router-dom';

export default function Home() {

	return (
		<>

			<Header2 />



			<main className="main">

				<div className="hero-img">
					<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/banner.jpg`} alt="" className='img img-fluid' />
				</div>

				<div id="about" className="about">
					<header className="home-about-header w-100">
						<div className='text-center'><i className="lni lni-user"></i></div>
						<h2 className="about-title text-center">About Your Coach</h2>
					</header>

					<div className="about-desc">Prapti Dutt, the owner of KeepWell Mantras is a certified level three Personal Trainer, a group Fitness instructor and an advanced clinical weight loss practitioner with a decade full of experience in fitness and wellness.</div>

					<Link to='/about' className="about-cta">Click for more about me</Link>
				</div>

				<div id="services" className="services">
					<h2 className="service-title">How can I help you?</h2>

					<div className="service-gallery">
						<div className="service-row">
							<div className="service-row-left">
								<div className="service-inner-left">
									<div className="service-inner-content">
										<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/image1.jpg`} alt="" />
									</div>
								</div>

								<div className="service-inner-right">
									<div className="service-inner-content">
										<div className="service-inner-content-wrapper">
											<h3 className="service-inner-content-title">Personal training</h3>
											<p className="service-inner-content-desc">Your fitness goals are unique just like you and I would love to train you as per your fitness needs.
												<span>
													Drop me an enquiry for a training session&nbsp;
													<a
														rel='noreferrer noopener' target='_blank'
														href='https://web.whatsapp.com/send?phone=447584236848&text=Hi%2C%20I%20am%20interested%20in%20knowing%20more%20and%20would%20like%20some%20information%2C%20I%20have%20been%20redirected%20from%20your%20website.'
													>
														here
													</a>
												</span>
											</p>
										</div>
									</div>
								</div>
							</div>

							<div className="service-row-right">
								<div className="service-inner-left">
									<div className="service-inner-content">
										<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/prapti2.jpg`} alt='..' />
									</div>
								</div>

								<div className="service-inner-right">
									<div className="service-inner-content">
										<div className="service-inner-content-wrapper">
											<h3 className="service-inner-content-title">Online Fitness Sessions</h3>
											<p className="service-inner-content-desc">
												Wish to do live sessions with me? Enquire and book your first class.&nbsp;
												<Link to='/fitness-challenges'>Click here</Link> to join my current Fitness Challenge
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="service-row">

							<div className="service-row-left">
								<div className="service-inner-left">
									<div className="service-inner-content">
										<div className="service-inner-content-wrapper">
											<h3 className="service-inner-content-title">Nutrition</h3>
											<p className="service-inner-content-desc">Nutrition helps you reach your goals and keeps you there! Wish to be more confident with your macros and micro nutrients ? Get in touch for a customized 1 to 1 Nutrition plan</p>
										</div>
									</div>
								</div>

								<div className="service-inner-right">
									<div className="service-inner-content">
										<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/image2.jpg`} alt="" />
									</div>
								</div>

							</div>

							<div className="service-row-left">
								<div className="service-inner-left">
									<div className="service-inner-content">
										<div className="service-inner-content-wrapper">
											<h3 className="service-inner-content-title">Confidential consultation</h3>
											<p className="service-inner-content-desc">Don’t know where to start ? I am just a call away. Book a consultation session straight away. Feel free to contact me in confidence via this&nbsp;
												<a
													rel='noreferrer noopener' target='_blank'
													href='https://web.whatsapp.com/send?phone=447584236848&text=Hi%2C%20I%20am%20interested%20in%20knowing%20more%20and%20would%20like%20some%20information%2C%20I%20have%20been%20redirected%20from%20your%20website.'
												>
													form
												</a>
											</p>
										</div>
									</div>
								</div>

								<div className="service-inner-right">
									<div className="service-inner-content">
										<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/image4.jpeg`} alt="" />
									</div>
								</div>

							</div>

						</div>

					</div>
				</div>

				<div className="core-values">
					<div className="core-values-inner">
						<div className="core-values-card">
							<div className="core-values-icon">
								<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/teaching.png`} alt='' className='img img-fluid' />
							</div>
							<h2 className="core-values-title text-indigo">1020</h2>
							<div className="core-values-desc">Live Classes</div>
						</div>
						<div className="core-values-card">
							<div className="core-values-icon">
								<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/customers.png`} alt='' className='img img-fluid' />
							</div>
							<h2 className="core-values-title text-teal">100%</h2>
							<div className="core-values-desc">Customers Satisfaction</div>
						</div>
						<div className="core-values-card">
							<div className="core-values-icon">
								<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/rating.png`} alt='' className='img img-fluid' />
							</div>
							<h2 className="core-values-title text-green">5</h2>
							<div className="core-values-desc">Five Star Rating</div>
						</div>
						<div className="core-values-card">
							<div className="core-values-icon">
								<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/weight-scale.png`} alt='' className='img img-fluid' />
							</div>
							<h2 className="core-values-title text-yellow">504</h2>
							<div className="core-values-desc">Successful Transformation</div>
						</div>
					</div>
				</div>

				<div className="testimony">
					<div className='testimony-inner mx-auto d-none d-md-block'>
						<Testimonial slides={4} />
					</div>
					<div className='testimony-inner mx-auto d-block d-md-none'>
						<Testimonial slides={1} />
					</div>
				</div>


			</main>

			<Footer />

		</>
	);
}