import React from 'react';
import Slider from "react-slick";
// import './styles.css';
import { slider_img } from '../ClientTransfomation/slider_img';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import HomeSlider from '../HomeSlider';
import TestimonialSlider from '../TestimonialSlider';
import { Container, Row, Col } from 'reactstrap';
import { Link } from "react-router-dom";

export default function Home() {

	var settings = {
		
		infinite: true,
		speed: 500,
		focusOnSelect: true,
		slidesToShow: 6,
		slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
		responsive: [
		  {
			breakpoint: 1024,
			settings: {
			  slidesToShow: 3,
			  slidesToScroll: 3,
			  infinite: true,
			  
			}
		  },
		  {
			breakpoint: 600,
			settings: {
				infinite: true,
			  slidesToShow: 2,
			  slidesToScroll: 2,
			  initialSlide: 2
			}
		  },
		  {
			breakpoint: 480,
			settings: {
				infinite: true,
			  slidesToShow: 1,
			  slidesToScroll: 1
			}
		  }
		]
	  };
	  

	return (
		<>
			<Header2 />

			<HomeSlider slides={1} />

			<div class="features-area pb30">
				<div class="call-to-action1">
					<Container>
						<Row>
							<Col lg="6">
								<div class="cta-img">
									<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/cta.jpg`} alt="girl-img"></img>
								</div>
							</Col>
							<Col lg="6">
								<div class="cta-box">
									<h4 class="sub-title mb30">ABOUT YOUR COACH</h4>
									<p class="mb30">Prapti Dutt, the owner of KeepWell Mantras is a certified level three Personal Trainer, a group Fitness instructor and an advanced clinical weight loss practitioner with a decade full of experience in fitness and wellness.</p>
									<div class="bttn">
										<Link to="/about" class="btn active btn-primary">Read More</Link>
									</div>
								</div>
							</Col>
						</Row>
					</Container>
				</div>

				<div class="portfolio-area title-white bg1 parallax overlay pad90">
					<Container>
						<Row>
							<Col lg="12">
								<div class="section-title text-center">
									<div class="title-bar full-width mb20">
										<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img"></img>
									</div>
									<h3>My Fitness Stars</h3>
									<p></p>
								</div>
							</Col>
						</Row>
					</Container>

					<Slider {...settings}>
					{
							slider_img.map(e => (
								<div>
									<img src={e.img} alt="" className='img img-fluid' />
								</div>
							))
						}
        </Slider>
				</div>

				<div class="pricing-area text-center pad90">
					<Container>
						<Row>
							<Col md="12">
								<div class="section-title text-center">
									<div class="title-bar full-width mb20">
										<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img"></img>
									</div>
									<h3>Packages</h3>
								</div>
							</Col>
						</Row>

						<Row>
							<Col md="6">
								<div class="price-box with-first surahi bg_color1">
									<div class="price-empty">
										<p>Live Classes packages - Follow the live class schedule</p>
									</div>

									<div class="price-quantity">
										<div class="price-dtl">
											<ul>
												<li class="first-child">6 weeks package</li>
												<li>£5 per session</li>
												<li>15% discount if you book min 15 sessions<br />
                                                20% discount if you book min 20 sessions
                                            	</li>
												<li>Access to Live classes or recordings of live classes of live classes</li>
												<li>Flexibility to choose from <br />
													- HIITs (High intensity - for intermediate to advance) <br />
													- MIITs ( Medium intensity - for beginners to intermediate)<br />
													- Strength workouts ( with dumbbells)
												</li>
												<li>Access to private members community</li>
											</ul>
											<div class="price-btn bttn">
												<Link to="/login" class="btn active btn-primary">
													Buy now
											</Link>
											</div>
										</div>
									</div>
								</div>
							</Col>

							<Col md="6">
								<div class="price-box with-first surahi bg_color2">
									<div class="price-empty">
										<p>Workout Videos - Create your own package!</p>
									</div>
									<div class="price-quantity">
										<div class="price-dtl">
											<ul>
												<li class="first-child">6 weeks package</li>
												<li>£5 per session</li>
												<li>20% discount if you book min 10 sessions</li>
												<li>Access to only recorded videos</li>
												<li>Flexibility to choose from <br />
													- HIITs (High intensity - for intermediate to advance) <br />
													- MIITs ( Medium intensity - for beginners to intermediate)<br />
													- Strength workouts ( with dumbbells)</li>
												<li>Access to private members community</li>
											</ul>
											<div class="price-btn bttn">
												<Link to="/login" class="btn active btn-primary">
													Buy now
											</Link>
											</div>
										</div>
									</div>
								</div>
							</Col>
						</Row>

						<div class="bttn again">
							<Link to="/packages" class="btn active btn-primary">
								More Packages
						</Link>
						</div>
					</Container>
				</div>

				<TestimonialSlider slides={1} />

				<div class="call-to-action pad90">
					<Container>
						<Row>
							<div class="col-md-9 col-sm-12">
								<h4>Choose Your Exercise and Start Your Training With Us.</h4>
							</div>
							<div class="col-md-3 col-sm-12">
								<div class=" bttn">
									<Link to="/classes" class="btn active btn-primary">
										Book now
									</Link>
								</div>
							</div>
						</Row>
					</Container>
				</div>

			</div>

			<Footer />

		</>
	);
}