export const testimonials = [
    {
        "name": "Smriti Vig",
        "review": "It’s only been less than 2 weeks since I first started with Prapti’s fitness sessions but it has completely changed my perspective about fitness. I might not be very regular but whatever sessions I have done so far have been excellent and have got me into that fitness and exercise routine. Since then I have…"
    },
    {
        "name": "Apurva Sharma",
        "review": "Prapti is the force you need to wake you up from your slumber. She has a great way about her which helps to motivate"
    },
    {
        "name": "Aanchal Gumbeer",
        "review": "Prapti is a fantastic fitness instructor. Her sessions are so varied and fun. She is motivating and she always helps to push me to do my best. Prapti’s online fitness sessions were a god sent to me in the lockdown period because of which I have kept myself fit and strong."
    },
    {
        "name": "Pooja Srivastava",
        "review": "Prapti’s fitness regimes are just the perfect start to my mornings. Not an early riser but her beautifully balanced workouts have been something I have started looking forward to everyday… after initial sweet soreness"
    },
    {
        "name": "Prajakta Badhan",
        "review": "Prapti’s passion for fitness is infectious and her own transformation is inspiring. She offers a variety of options to train with her: zoom sessions"
    },
    {
        "name": "Sneha Lamba",
        "review": "Hi All Mums, Just wanted to share my experience with @Prapti of KeepWell Mantras. After my second pregnancy I struggled to lose my pregnancy weight. I met Prapti as part of the West Silvertown Buggy Fitness Group and enjoyed her workouts. I signed up for her personal sessions and diet plan. After 8 weeks of…"
    },
    {
        "name": "Hima Bindu",
        "review": "Dear Prapti, or Can I say My angel , you have been incredible when we first spoke before 3 months my health was terrible super back issues struggling from almost an year. Would pant even to climb 10 stairs, Your exercise suggestion for diet, supplements everything was new to me, those initial personal sessions were…"
    },
    {
        "name": "Kritee Varma",
        "review": "I did not reach out to prapti alone i reached out in a package of Irregular cycles Hormonal Imbalances  Hopelessness Three digit weight stats While she made all her efforts to comfort me"
    },
    {
        "name": "Darshana Patil",
        "review": "I still remember the day I called up Prapti guess it was a month and a half back. I was literally crying over the phone. Why was I crying because I wanted to get back to my size not zero. And I wanted someone to believe that I wanted that shift in my life. Well…"
    },
    {
        "name": "Mamta Mishra",
        "review": "I highly recommend Prapti’s Workout sessions. They are so intense, lots of variety. No session is the same. I have been doing her sessions since May2020 and i love it. I have improved on my stamina , strength and muscle gain. Thankyou Prapti for all your motivation, you are a wonderful fitness trainer."
    },
    {
        "name": "Sweta Deb",
        "review": "Prapti’s one to one sessions are very good and tailor made to suit one’s requirement. She is dedicated and has been very helpful in building my strength and weight loss. I would definitely recommend her 121 sessions."
    },
    {
        "name": "Mayura Atre",
        "review": "This recommendation is for Prapti for the super awesome workout sessions that she conducts on daily basis. I must say these are just not any random session that we see online"
    },
    {
        "name": "Chethana Suhas",
        "review": "Prapti is absolutely fantastic !!! She is one of the good things that happened to me during Lockdown. I have been doing her sessions since July and I just love it. Her sessions are not the same"
    },
    {
        "name": "Noopur Srivastava",
        "review": "I highly recommend Prapti. It’s been couple of months that I started workout with her online and I would like to mention"
    },
    {
        "name": "Ashavaree Das",
        "review": "So I have been meaning to write this since eons but I am a lazybum. I have a hectic work schedule – so grateful for that but also came with that my lack of work-life balance which merits a totally different post . Anyhow so while battling many issues"
    },
    {
        "name": "Dhruvi Jain",
        "review": "I’ve never been into fitness before I started Prapti’s sessions. I used to go to the gym/ try YouTube videos, see no results, get bored and give up. I’ve been wanting to shed off some pregnancy weight since a few months and was so glad a friend recommended Prapti to me. After attending her first…"
    },
    {
        "name": "Rebecca Peacock",
        "review": "I’ve now had 4 sessions with Prapti- she is great! She knows exactly how hard to push me and I am so pleased I signed up to it. I look forward to starting the weekend classes too, now that all my weekend plans have cancelled!"
    },
    {
        "name": "Divya Garg",
        "review": "I have recently joined Prapti’s fitness classes and really I am not one of those self motivated people who do regular excercises. But whenever I attend the class which is normally 2-3 times a week it’s worth as a new muscle of my body gets soar which I was till date unaware even existed .…"
    },
    {
        "name": "Shahanara Rohman",
        "review": "Coming out of Prapti fitness classes I feel energised, happy and ready to start my day. She really pushes you and focuses on the core body areas. I would highly recommend Prapti."
    },
    {
        "name": "Shilpi Agarwal",
        "review": "I started doing Zoom workouts 4 days a week in mid November as part of Finish Strong 2020 challenge. I reached my goal 2 weeks early! Prapti’s sessions are the perfect mix of Cardio and weight training. She gives you tips on how to make them less or more intense. She is a friend and…"
    },
    {
        "name": "Usha Sharma",
        "review": "Prapti is my lovely daughter in law. I was living with her in 2019 for 6 months and she helped me lose 10 kgs in 6 months with the help of diet and light exercises. I am 67 years old and still i was able to lose so much weight. It reduced my knee problems…"
    },
    {
        "name": "Pooja Singh",
        "review": "So far i have attended 10 sessions of hers online . I have improved alot in my flexibility and body posture. What I like about her she workouts along with us and as per us . She is really professional when it comes to fitness and works on one to one basis with her clients.…"
    },
    {
        "name": "Karishma Mehe",
        "review": "I am fairly new to the sessions &amp; I keep coming for more. I love the stretches &amp; can feel the burn . Prapti is an amazing"
    },
    {
        "name": "Smitha Bs",
        "review": "Thank you Prapti for consistently designing and training us on our fitness missions. In just 6 weeks, I believe I can achieve a better body despite my medical conditions with your guided diet plans and motivating fitness programs. You’ve truly made us realise that getting healthy is not a goal; but a way of living. Keep…"
    },
    {
        "name": "Archudha Durairaj",
        "review": "I have joined her sessions she is amazing very structured workout sessions where all types of people can join. In love with the morning sessions where you can burn 300-400 cal easily. Highly recommend her"
    },
    {
        "name": "Mahak Agarwal",
        "review": "Prapti is really professional and organises structured fitness sessions.. I like the personal touch in the sessions even when doing them virtually.. once you begin working out with her you would feel guilty on missing out a session.. Highly recommended!!!"
    },
    {
        "name": "Tara Avdesh Verma",
        "review": "Amazing full body online sessions. Could feel the pain(good one!) after each session. You are great Prapti so as your style of training. All the best.. xx"
    },
    {
        "name": "Neha Gadia",
        "review": "It’s been two months since I have started training with Prapti and I came to her as a beginner. First trial session itself made me feel this is what I was looking for( after trying many – many YouTube videos and gym sessions). Once you start getting comfortable she will change the circuits and take…"
    },
    {
        "name": "Girija Kottalgi",
        "review": "Sharing the super joy of seeing 56 kg on weighing scale as it had become a distant dream for last 6 years and you made it possible in 7 weeks !! Losing those 3 stubborn kgs felt next to impossible ……. Kudos to your work outs 🤩🤩and consistency…..and here I am feeling more in shape…"
    },
    {
        "name": "Juhi Jani",
        "review": "My fitness journey started in 2019 and when I met you in May 2021 … I have hit the plateau in terms of my fat loss. I have always been into healthy eating, but things really started to shift after doing your workouts, and I genuinely saw some great benefits… Starting from cutting out carbs…"
    },
    {
        "name": "Shubhangi Mitra",
        "review": "I have known Prapti for six months now and every time this girl surprises me with her determination. I joined Prapti’s diet regime when I suffered from some challenges of exercise and serious weight gain issue due to medical problems and steroids I was put on. Prapti with her methodological way blended everything beautifully be…"
    },
    {
        "name": "Ashavaree Das",
        "review": "So I have been meaning to write this since eons but I am a lazybum. I have a hectic work schedule – so grateful for that but also came with that my lack of work-life balance which merits a totally different post . Anyhow so while battling many issues, I finally took the plunge with…"
    }
];