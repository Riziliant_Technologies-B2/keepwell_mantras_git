import React, { useEffect, useState } from "react";
import { Button, Card, CardHeader, CardBody, Input, Row, Col, InputGroup, InputGroupAddon, FormGroup, Label } from "reactstrap";
import { useCookies } from "react-cookie";
import update from 'immutability-helper';
import NumericInput from 'react-numeric-input';

import _fetch from '../../_fetch';
import Loader from '../../components/Loader/loader';
import { toast } from "react-toastify";

const init_state = {
    discount: 0,
    min_class_count: 0,
    max_class_count: 0,
    class_type: ''
};

export default function OrderForm({ setFinalOrder }) {

    const [applied_promo_code, setAppliedPromoCode] = useState(init_state);
    const [promo_code, setPromoCode] = useState('');
    const [live_classes, setLiveClasses] = useState(0);
    const [recorded_classes, setRecordedClasses] = useState([]);
    const [total, setTotal] = useState(0);
    const [loading, setLoading] = useState(false);
    const [applying, setApplying] = useState(false);
    const [redirecting, setRedirecting] = useState(false);
    const [consent, setConsent] = useState(false);
    const [diet_plan, setDietPlan] = useState('');

    const [cookie, setCookie, removeCookie] = useCookies(['cart', 'live_class_count', 'diet_package']);

    useEffect(() => {

        if (cookie.live_class_count && !isNaN(parseInt(cookie.live_class_count, 10))) {
            setLiveClasses(parseInt(cookie.live_class_count, 10));
        }

        if (cookie.diet_package) {
            console.log(cookie.diet_package);
            setDietPlan(cookie.diet_package.diet_plan);
        }

        if (!cookie.cart) return;
        setLoading(true);
        let obj = {};
        let arr = [];

        cookie.cart.split(',').forEach(e => {
            arr.push(e.split(':')[0]);
            let qty = parseInt(e.split(':')[1]);
            if (isNaN(qty) || qty < 0) qty = 0;
            obj[e.split(':')[0]] = qty;
        });

        _fetch(`${process.env.REACT_APP_API}/category?ids=${JSON.stringify(arr)}`)
            .then(res => {
                if (res.success && Array.isArray(res.response)) {
                    let categories = res.response.map(e => ({ quantity: obj[e._id], ...e }));
                    setRecordedClasses(categories);
                }
            })
            .catch(err => {
                console.log(err);
            })
            .finally(() => setLoading(false));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {

        const recorded_classes_count = recorded_classes.reduce((init, e) => init + e.quantity, 0);
        const total_classes = recorded_classes_count + live_classes;
        let live_class_discount = 0;
        let recorded_class_discount = 0;
        let diet_discount = 0;
        let diet_package_price = 0;
        let promo = applied_promo_code;

        if (diet_plan === '6_WEEKS') {
            diet_package_price = 100;
        } else if (diet_plan === '60_MINS') {
            diet_package_price = 40;
        }

        switch (promo.class_type) {
            case 'LIVE':
                if (promo.min_class_count <= live_classes && promo.max_class_count >= live_classes) {
                    live_class_discount = promo.discount;
                }
                break;
            case 'RECORDED':
                if (promo.min_class_count <= recorded_classes_count && promo.max_class_count >= recorded_classes_count) {
                    recorded_class_discount = promo.discount;
                }
                break;
            case '6_WEEKS':
                if (promo.min_class_count <= total_classes && promo.max_class_count >= total_classes) {
                    if (diet_plan === '6_WEEKS')
                        diet_discount = promo.discount;
                }
                break;
            case '60_MINS':
                if (promo.min_class_count <= total_classes && promo.max_class_count >= total_classes) {
                    if (diet_plan === '60_MINS')
                        diet_discount = promo.discount;
                }
                break;
            default:
                if (promo.min_class_count <= total_classes && promo.max_class_count >= total_classes) {
                    live_class_discount = promo.discount;
                    recorded_class_discount = promo.discount;
                    diet_discount = promo.discount;
                }
        }

        // remove promo code if class count lies outside promo code limits
        if (promo.discount && !(live_class_discount || recorded_class_discount || diet_discount)) {
            toast.error(`This promo code can be applied ${['LIVE', 'RECORDED'].includes(promo.class_type) ? `on ${promo.class_type.toLowerCase()} classes ` : ''}only if total ${['LIVE', 'RECORDED'].includes(promo.class_type) ? `${promo.class_type.toLowerCase()} ` : ''} classes are between ${promo.min_class_count} and ${promo.max_class_count}`);
            setPromoCode('');
            return setAppliedPromoCode(init_state);
        }


        let recorded_class_total = recorded_classes.reduce((initial, e) => initial + e.quantity * e.price_per_class, 0);
        let total = (recorded_class_total * (100 - recorded_class_discount) + live_classes * 5 * (100 - live_class_discount) + diet_package_price * (100 - diet_discount)) / 100;
        setTotal(total);
    }, [live_classes, applied_promo_code, recorded_classes, diet_plan]);

    function ApplyPromoCode() {
        setApplying(true);
        _fetch(`${process.env.REACT_APP_API}/promo_code?promo_code=${promo_code}`)
            .then(res => {
                if (res.success) {
                    return setAppliedPromoCode(res.response);
                }
                toast.error('Promo code doesn\'t exist or has expired');
                setAppliedPromoCode(init_state);

            })
            .catch(err => {
                console.log(err);
                setAppliedPromoCode(init_state);
                toast.error('Promo code doesn\'t exist or has expired');
            })
            .finally(() => setApplying(false));
    }

    async function redirectToPaymentPage() {

        try {

            let body = {};

            if (promo_code) body.promo_code = promo_code;
            if (live_classes) body.live_classes_count = live_classes;
            if (diet_plan) {
                body.diet_plan = diet_plan;
                body.details = cookie.diet_package.details;
                delete body.details.diet_plan;
            }
            body.recorded_classes = recorded_classes.filter(e => e.quantity > 0).map(e => ({ category_id: e._id, quantity: e.quantity }));

            setRedirecting(true);
            let order_data = await _fetch(`${process.env.REACT_APP_API}/order`, {
                method: 'POST',
                body
            });

            if (order_data.success) {
                setRedirecting(false);
                setFinalOrder(order_data.response);
            } else {
                setRedirecting(false);
                return toast.error(order_data.response);
            }

        } catch (err) {
            console.log(err);
            setRedirecting(false);
        }
    }

    if (loading) return (
        <Card className='bg-secondary shadow w-100 h-100'>
            <Loader />
        </Card>
    )

    return (
        <Card className="bg-secondary shadow h-100">
            <CardHeader className="bg-white border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                        <h3 className="mb-0">Order Summary</h3>
                    </Col>
                </Row>
            </CardHeader>
            <CardBody>

                <Row>
                    <Col xs={7} md={8} className='font-weight-600'>Product</Col>
                    <Col xs={5} md={4} className='font-weight-600'>Subtotal</Col>
                </Row>
                <hr className="my-4" />

                <Row>
                    <Col xs={9} md={8}>
                        Live Classes&emsp;
                        <span className='font-weight-600'>
                            <i className='ni ni-fat-remove text-lg align-text-bottom'></i>&nbsp;
                            <NumericInput className='card-total-class'
                                min={0} step={1}
                                onChange={(val) => {
                                    setCookie('live_class_count', val);
                                    setLiveClasses(val)
                                }}
                                value={live_classes}
                            />
                        </span>
                    </Col>
                    <Col xs={3} md={4} className='font-weight-600'>£{live_classes * 5}</Col>
                </Row>
                <hr className="my-4" />

                {
                    diet_plan ?
                        <React.Fragment>
                            <Row>
                                <Col xs={9} md={8}>
                                    {cookie.diet_package?.details?.selected_plan}&emsp;
                                </Col>
                                <Col xs={3} md={4} className='font-weight-600'>
                                    £{diet_plan === '6_WEEKS' ? '100' : '40'}
                                </Col>
                            </Row>
                            <hr className="my-4" />
                        </React.Fragment>
                        :
                        null
                }

                {
                    recorded_classes?.length
                        ?
                        recorded_classes.map((e, i) => (
                            <React.Fragment>
                                <Row>
                                    <Col xs={9} md={8}>
                                        {e.name}&emsp;
                                        <span className='font-weight-600'>
                                            <i className='ni ni-fat-remove text-lg align-text-bottom'></i>&nbsp;

                                            <NumericInput className='card-total-class'
                                                min={0} step={1} max={e.classes.length}
                                                onChange={(val) => {
                                                    setRecordedClasses(update(recorded_classes, { [i]: { quantity: { $set: val } } }));
                                                    let categories = recorded_classes.map((e, ind) => e.quantity ? `${e._id}:${i === ind ? val : e.quantity}` : '').join(',');
                                                    setCookie('cart', categories, { maxAge: 2592000, sameSite: true, path: '/' });
                                                }}
                                                value={e.quantity}
                                            />
                                        </span>
                                    </Col>
                                    <Col xs={3} md={4} className='font-weight-600'>£{e.quantity * e.price_per_class}</Col>
                                </Row>
                                <hr className="my-4" />
                            </React.Fragment>
                        ))
                        :
                        null
                }

                <Row>
                    <Col xs={9} md={8} className='pr-4'>
                        <InputGroup>
                            <Input
                                placeholder="promo code" value={promo_code}
                                onChange={(ev) => setPromoCode(ev.target.value)}
                            />
                            <InputGroupAddon addonType="append">
                                <Button color="primary" onClick={ApplyPromoCode} disabled={applying}>
                                    {applying ? <Loader size={25} /> : null} Apply
                                </Button>
                            </InputGroupAddon>
                        </InputGroup>
                        {
                            applied_promo_code.class_type === 'LIVE'
                                ?
                                <div className='text-xs text-info'>
                                    This promo code is applicable only on Live Classes
                                </div>
                                :
                                null
                        }
                        {
                            applied_promo_code.class_type === 'RECORDED'
                                ?
                                <div className='text-xs text-info'>
                                    This promo code is applicable only on Workout Videos
                                </div>
                                :
                                null
                        }
                        {
                            applied_promo_code.class_type === '6_WEEKS'
                                ?
                                <div className='text-xs text-info'>
                                    This promo code is applicable only on 6 week nutrition support plan
                                </div>
                                :
                                null
                        }
                        {
                            applied_promo_code.class_type === '60_MINS'
                                ?
                                <div className='text-xs text-info'>
                                    This promo code is applicable only on 60 minute nutrition support consultation call
                                </div>
                                :
                                null
                        }
                    </Col>
                    <Col xs={3} md={4} className='font-weight-600'>
                        {applied_promo_code.discount}% Discount
                    </Col>
                </Row>
                <hr className="my-4" />

                <Row>
                    <Col xs={9} md={8} className='font-weight-600'>Total</Col>
                    <Col xs={3} md={4} className='font-weight-600'>£{total}</Col>
                </Row>

                <hr className="my-4" />

                <p className='text-xs'>
                    You assume the risk of your participation in any activity during this program. You agree that you are voluntarily participating in the aforementioned activities and assume all risk of injury, illness, damage, or loss to you or your property that might result, including, without limitation, any loss or theft of any personal property, whether arising out of the negligence of Trainer or otherwise. By execution of this agreement, you hereby agree to indemnify and hold harmless Trainer from any loss, liability, damage, or cost Trainer may incur due to the provision of fitness training by Trainer to you.
                </p>
                <p className='text-xs'>
                    You acknowledge that Trainer offers a service to his/her clients encompassing the entire recreational and/or fitness spectrum / nutrition programme. You agree on behalf of yourself to release and discharge Trainer from any and all claims or causes of action (known or unknown) arising out of the negligence of Trainer, whether active or passive.
                </p>
                <p className='text-xs'>
                    All bookings have a 6 week expiry and no refunds will be issued once booked.
                </p>

                <FormGroup check>
                    <Label check className='mb-3'>
                        <Input type="checkbox" id="checkbox2" onClick={() => setConsent(!consent)} />{' '}
                        I consent
                    </Label>
                </FormGroup>

                <Row>
                    <Col xs={6} className='text-center'>
                        <Button
                            type='button' color='danger' className='w-100'
                            onClick={() => {
                                removeCookie('cart');
                                removeCookie('live_class_count');
                                removeCookie('diet_package');
                                setLiveClasses(0);
                                setDietPlan('');
                                setRecordedClasses([]);
                                setPromoCode('');
                                setAppliedPromoCode(init_state);
                                setTotal(0);
                            }} disabled={redirecting}
                        >
                            Clear Cart
                        </Button>
                    </Col>
                    <Col xs={6} className='text-center'>
                        <Button
                            type='button' color='primary' className='w-100'
                            onClick={redirectToPaymentPage} disabled={redirecting || !consent}
                        >
                            {
                                redirecting ?
                                    <React.Fragment><Loader size={20} />Finalizing order ...</React.Fragment>
                                    :
                                    'Confirm Order'
                            }
                        </Button>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
};
