import React from "react";
import { Button, Card, CardHeader, CardBody, Row, Col } from "reactstrap";
import { useSelector } from "react-redux";
import { useCookies } from "react-cookie";
import moment from "moment";

const mid = process.env.REACT_APP_TAKEPAYMENTS_MID;
const currencycode = "826";
const transactiontype = "SALE";
const orderdesc = 'KeepWell Mantras';
const address1 = '';
const address2 = '';
const address3 = '';
const address4 = '';
const city = '';
const state = '';
const postcode = '';
const countrycode = '';
const hashmethod = 'SHA1';
const callbackurl = `${process.env.REACT_APP_HOME}/dashboard`;
const resultdeliverymethod = 'SERVER';
const serverresulturl = `${process.env.REACT_APP_API}/order/response`;
const PaymentFormDisplaysResult = 'false';

export default function Payment({ order }) {

    const { first_name, last_name, email } = useSelector(st => st.user);
    const [, , removeCookie] = useCookies(['cart', 'live_class_count']);

    return (
        <Card className="bg-secondary shadow h-100">
            <CardHeader className="bg-white border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                        <h3 className="mb-0">Order Summary</h3>
                    </Col>
                </Row>
            </CardHeader>

            <CardBody>

                <Row>
                    <Col xs={7} md={8} className='font-weight-600'>Product</Col>
                    <Col xs={5} md={4} className='font-weight-600'>Subtotal</Col>
                </Row>
                <hr className="my-4" />

                {
                    order.live_classes.count ?
                        <React.Fragment>
                            <Row>
                                <Col xs={7} md={8}>
                                    Live Classes&emsp;
                                    <span className='font-weight-600'>
                                        <i className='ni ni-fat-remove text-lg align-text-bottom'></i>&nbsp;
                                        {order.live_classes.count}
                                    </span>
                                </Col>
                                <Col xs={5} md={4} className='font-weight-600'>£{order.live_classes.count * order.live_classes.price_per_class}</Col>
                            </Row>
                            <hr className="my-4" />
                        </React.Fragment>
                        :
                        null
                }

                {
                    order.recorded_classes?.length
                        ?
                        order.recorded_classes.map((e, i) => (
                            <React.Fragment>
                                <Row>
                                    <Col xs={7} md={8}>
                                        {e.category_id.name}&emsp;
                                        <span className='font-weight-600'>
                                            <i className='ni ni-fat-remove text-lg align-text-bottom'></i>&nbsp;
                                            {e.count}
                                        </span>
                                    </Col>
                                    <Col xs={5} md={4} className='font-weight-600'>£{e.count * e.price_per_class}</Col>
                                </Row>
                                <hr className="my-4" />
                            </React.Fragment>
                        ))
                        :
                        null
                }

                {
                    order.diet_package ?
                        <React.Fragment>
                            <Row>
                                <Col xs={7} md={8}>
                                    {order.diet_package.plan === '6_WEEKS' ? '6 Weeks Nutrition Support Plan' : '60 Minutes Nutrition Consultation Call'}
                                </Col>
                                <Col xs={5} md={4} className='font-weight-600'>£{order.diet_package.price}</Col>
                            </Row>
                            <hr className="my-4" />
                        </React.Fragment>
                        :
                        null
                }

                {
                    order.promo_code
                        ?
                        <React.Fragment>
                            <Row>
                                <Col xs={7} md={8} className='pr-4'>
                                    <div>Promo Code: <span className='font-weight-600'>{order.promo_code.name}</span></div>
                                    {
                                        order.promo_code.class_type === 'LIVE'
                                            ?
                                            <div className='text-xs text-info'>
                                                This promo code is applicable only on Live Classes
                                            </div>
                                            :
                                            null
                                    }
                                    {
                                        order.promo_code.class_type === 'RECORDED'
                                            ?
                                            <div className='text-xs text-info'>
                                                This promo code is applicable only on Workout Videos
                                            </div>
                                            :
                                            null
                                    }
                                </Col>
                                <Col xs={5} md={4} className='font-weight-600'>
                                    {order.promo_code.discount}%
                                </Col>
                            </Row>
                            <hr className="my-4" />
                        </React.Fragment>
                        :
                        null
                }

                <Row>
                    <Col xs={7} md={8} className='font-weight-600'>Total</Col>
                    <Col xs={5} md={4} className='font-weight-600'>£{order.invoice_total / 100}</Col>
                </Row>
                <hr className="my-4" />

            </CardBody>

            <CardBody>

                <Row>
                    <Col xs={{ size: 8, offset: 2 }}>

                        <form method="POST" onSubmit={() => { removeCookie('cart'); removeCookie('live_class_count') }}
                            action={process.env.REACT_APP_TAKEPAYMENT_URL}>

                            <input type="hidden" name="MerchantID" value={mid} />
                            <input type="hidden" name="Amount" value={order.invoice_total} />
                            <input type="hidden" name="CurrencyCode" value={currencycode} />
                            <input type="hidden" name="OrderID" value={order.invoice_id} />
                            <input type="hidden" name="TransactionType" value={transactiontype} />
                            <input type="hidden" name="TransactionDateTime" value={moment(order.createdAt).utc().format('YYYY-MM-DD HH:mm:ss Z')} />
                            <input type="hidden" name="OrderDescription" value={orderdesc} />
                            <input type="hidden" name="CustomerName" value={`${first_name}${last_name ? ` ${last_name}` : ''}`} />
                            <input type="hidden" name="Address1" value={address1} />
                            <input type="hidden" name="Address2" value={address2} />
                            <input type="hidden" name="Address3" value={address3} />
                            <input type="hidden" name="Address4" value={address4} />
                            <input type="hidden" name="City" value={city} />
                            <input type="hidden" name="State" value={state} />
                            <input type="hidden" name="PostCode" value={postcode} />
                            <input type="hidden" name="CountryCode" value={countrycode} />
                            <input type="hidden" name="EmailAddress" value={email} />
                            <input type="hidden" name="HashMethod" value={hashmethod} />
                            <input type="hidden" name="CallbackURL" value={callbackurl} />
                            <input type="hidden" name="ResultDeliveryMethod" value={resultdeliverymethod} />
                            <input type="hidden" name="ServerResultURL" value={serverresulturl} />
                            <input type="hidden" name="PaymentFormDisplaysResult" value={PaymentFormDisplaysResult} />
                            <input type="hidden" name="HashDigest" value={order.pi} />

                            <Button type='submit' color='primary' className='w-100' >Complete Payment</Button>
                        </form>
                    </Col>
                </Row>
            </CardBody>

        </Card>
    );
};
