import React from "react";
import { useSelector } from "react-redux";

import { Card, CardHeader, CardBody, FormGroup, Form, Input, Row, Col } from "reactstrap";

export default function UserInfo() {

    const { first_name, last_name, email, mobile } = useSelector(st => st.user);

    return (
        <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                        <h3 className="mb-0">Billing Details</h3>
                    </Col>
                </Row>
            </CardHeader>
            <CardBody>
                <Form>
                    <h6 className="heading-small text-muted mb-4">
                        User information
                    </h6>
                    <div className="pl-lg-4">
                        <Row>
                            <Col lg="6">
                                <FormGroup>
                                    <label
                                        className="form-control-label"
                                        htmlFor="input-first-name"
                                    >
                                        First name
                                    </label>
                                    <Input
                                        className="form-control-alternative"
                                        defaultValue={first_name}
                                        id="input-first-name"
                                        placeholder="First name"
                                        type="text" disabled={true}
                                    />
                                </FormGroup>
                            </Col>
                            <Col lg="6">
                                <FormGroup>
                                    <label
                                        className="form-control-label"
                                        htmlFor="input-last-name"
                                    >
                                        Last name
                                    </label>
                                    <Input
                                        className="form-control-alternative"
                                        defaultValue={last_name}
                                        id="input-last-name"
                                        placeholder="Last name"
                                        type="text" disabled={true}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="6">
                                <FormGroup>
                                    <label
                                        className="form-control-label"
                                        htmlFor="input-email"
                                    >
                                        Email Address
                                    </label>
                                    <Input
                                        className="form-control-alternative"
                                        id="input-email"
                                        placeholder="Email Address"
                                        type="email" disabled={true}
                                        defaultValue={email}
                                    />
                                </FormGroup>
                            </Col>
                            <Col lg="6">
                                <FormGroup>
                                    <label
                                        className="form-control-label"
                                        htmlFor="input-mobile"
                                    >
                                        Mobile No.
                                    </label>
                                    <Input
                                        className="form-control-alternative"
                                        defaultValue={mobile}
                                        id="input-mobile"
                                        placeholder="Mobile No."
                                        type="text" disabled={true}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                    </div>

                </Form>
            </CardBody>
        </Card>
    );
};
