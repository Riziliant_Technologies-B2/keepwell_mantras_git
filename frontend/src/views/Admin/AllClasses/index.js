import React, { useCallback, useEffect, useState } from "react";
import {
	Card, CardHeader, CardFooter, Pagination, PaginationItem, PaginationLink,
	Table, Container, Button, Input,
} from "reactstrap";
import update from 'immutability-helper';
import moment from "moment";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

import AdminHeader from "../../../components/Headers/AdminHeader.js";
import _fetch from '../../../_fetch';
import Loader from "../../../components/Loader/loader.js";

const pageLength = 10;

export default function AllClasses() {

	const [loading, setLoading] = useState(false);
	const [page, setPage] = useState(0);
	const [totalPages, setTotalPages] = useState(1);
	const [filter, setFilter] = useState({
		type: '',
		date: ''
	});
	const [classes, setClasses] = useState([]);
	let history = useHistory();

	const getClasses = useCallback(async () => {
		try {
			setLoading(true);

			let url = `${process.env.REACT_APP_API}/classes?include_enrollment=true&`;
			url += `${filter.category ? `category=${filter.category}&` : ''}`;
			url += `${filter.type ? `type=${filter.type}&` : ''}`;
			url += `${filter.date ? `date=${filter.date}&` : ''}`;

			let res = await _fetch(url);
			if (res.success) {
				console.log(res.response);
				setClasses(res.response);
				setTotalPages(Math.ceil(res.response.length / pageLength));
			}

		} catch (err) {
			console.log(err);
		} finally {
			setLoading(false);
		}
	}, [filter]);

	const deleteClass = async (ind) => {
		try {
			setLoading(true);
			let res = await _fetch(`${process.env.REACT_APP_API}/classes?id=${classes[ind]._id}`, { method: 'DELETE' });
			if (res.success) {
				console.log(res.response);
				setClasses(update(classes, { $splice: [[ind, 1]] }));
				toast.success('Class deleted successfully');
			} else {
				toast.error(res.response);
			}
		} catch (err) {
			console.log(err);
		} finally {
			setLoading(false);
		}
	}

	useEffect(() => {
		getClasses();
	}, [getClasses]);

	if (loading) return (
		<>
			<AdminHeader />

			<Container className="mt--7" fluid>
				<Card className="bg-secondary shadow" style={{ height: 400 }}>
					<Loader />
				</Card>
			</Container>
		</>
	);

	return (
		<>
			<AdminHeader />
			{/* Page content */}
			<Container className="mt--9" fluid>
				<Card className="shadow">

					<CardHeader className="border-0">
						<div className='d-flex justify-content-between'>
							<h3 className="mb-0">All Classes</h3>
							<div className='d-flex flex-column'>
								<Input
									className="form-control-alternative mb-2"
									id="input-gender"
									name='category'
									type="select"
									onChange={(ev) => setFilter({ ...filter, type: ev.target.value })}
									value={filter.type}
									defaultValue=''
								>
									<option value='' disabled>Select Type</option>
									<option value='LIVE'>Live</option>
									<option value='RECORDED'>Recorded</option>
								</Input>
								<Input
									type='date'
									onChange={(ev) => setFilter({ ...filter, date: ev.target.value })}
									value={filter.date} className='mb-2'
								/>
								<Button onClick={() => setFilter({ date: '', type: '' })}>
									Clear
								</Button>
							</div>
						</div>
					</CardHeader>

					<Table className="align-items-center table-flush" responsive>
						<thead className="thead-light">
							<tr>
								<th scope="col">Class</th>
								<th scope="col">Class Type</th>
								<th scope="col">Date</th>
								<th scope="col">Total Enrolled User</th>
								<th scope="col">Category</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							{
								classes?.slice(page * pageLength, (page + 1) * pageLength).map((e, i) => (
									<tr>
										<th scope="row">
											{e.title}
										</th>
										<td>{e.type}</td>
										<td>
											{e.date ? moment(e.date).format('llll') : ''}
										</td>
										<td>{e.userCount}</td>
										<td>{e.category.name}</td>
										<td className='w-25'>
											<Button
												size='sm' color='info'
												onClick={() => history.push(`/admin/add-class/${e._id}`)}
											>
												Edit
											</Button>
											<Button
												size='sm' color='danger' className='mr-auto'
												disabled={!!(e.userCount && e.userCount > 0)}
												onClick={() => deleteClass(page * pageLength + i)}
											>
												Delete
											</Button>
										</td>
									</tr>
								))
							}
						</tbody>
					</Table>

					<CardFooter className="py-4">
						<nav aria-label="...">
							<Pagination
								className="pagination justify-content-end mb-0"
								listClassName="justify-content-end mb-0"
							>
								<PaginationItem className="pointer">
									<PaginationLink
										href="#"
										onClick={(e) => page === 0 ? '' : setPage(page - 1)}
									>
										<i className="lni lni-chevron-left"></i>
										<span className="sr-only">Previous</span>
									</PaginationLink>
								</PaginationItem>

								<PaginationItem className={page === 0 ? 'active' : ''}>
									<PaginationLink
										href="#"
										onClick={(e) => page === 0 ? '' : setPage(page - 1)}
									>
										{page === 0 ? 1 : page}
									</PaginationLink>
								</PaginationItem>

								{
									(page === 0 ? totalPages > 1 : true) ?
										<PaginationItem className={page === 0 ? '' : 'active'}>
											<PaginationLink
												href="#"
												onClick={(e) => page === 0 ? setPage(1) : ''}
											>
												{page === 0 ? 2 : page + 1}
											</PaginationLink>
										</PaginationItem>
										:
										null
								}

								{
									(page === 0 ? 2 < totalPages : page + 1 < totalPages) ?
										<PaginationItem>
											<PaginationLink
												href="#"
												onClick={(e) => page === 0 ? setPage(2) : setPage(1)}
											>
												{page === 0 ? 3 : page + 2}
											</PaginationLink>
										</PaginationItem>
										:
										null
								}

								<PaginationItem>
									<PaginationLink
										href="#"
										onClick={(e) => page + 1 < totalPages ? setPage(page + 1) : ''}
									>
										<i className="lni lni-chevron-right"></i>
										<span className="sr-only">Next</span>
									</PaginationLink>
								</PaginationItem>
							</Pagination>
						</nav>
					</CardFooter>

				</Card>

			</Container>
		</>
	);
};
