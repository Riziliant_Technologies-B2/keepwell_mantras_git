import React, { useCallback, useEffect, useState } from "react";
import {
    Card, CardHeader, CardFooter, Pagination, PaginationItem, PaginationLink,
    Table, Container, Button, Modal, ModalHeader, ModalBody, ModalFooter
} from "reactstrap";
import moment from "moment";

import AdminHeader from "../../../components/Headers/AdminHeader.js";
import _fetch from '../../../_fetch';
import Loader from "../../../components/Loader/loader.js";

const pageLength = 10;

export default function SixtyMinNSPCall() {

    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(0);
    const [totalPages, setTotalPages] = useState(1);
    const [enrollments, setEnrollments] = useState([]);
    const [details, setDetails] = useState(null);

    const getClasses = useCallback(async () => {
        try {
            setLoading(true);

            let url = `${process.env.REACT_APP_API}/enrollment/admin/60_mins`;

            let res = await _fetch(url);
            if (res.success) {
                console.log(res.response);
                setEnrollments(res.response);
                setTotalPages(Math.ceil(res.response.length / pageLength));
            }

        } catch (err) {
            console.log(err);
        } finally {
            setLoading(false);
        }
    }, []);

    useEffect(() => {
        getClasses();
    }, [getClasses]);

    if (loading) return (
        <>
            <AdminHeader />

            <Container className="mt--7" fluid>
                <Card className="bg-secondary shadow" style={{ height: 400 }}>
                    <Loader />
                </Card>
            </Container>
        </>
    );

    return (
        <>
            <AdminHeader />
            {/* Page content */}
            <Container className="mt--9" fluid>
                <Card className="shadow">

                    <CardHeader className="border-0">
                        <h3 className="mb-0">6 Week Nutrition Support Plan Enrollments</h3>
                    </CardHeader>

                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Purchase Date</th>
                                <th scope="col">View Full Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                enrollments?.slice(page * pageLength, (page + 1) * pageLength).map((e, i) => (
                                    <React.Fragment key={i}>
                                        <tr className='br-secondary'>
                                            <th scope="row">
                                                {e?.user?.first_name} {e?.user?.last_name}
                                            </th>
                                            <td>
                                                {e?.user?.email}
                                            </td>
                                            <td>
                                                {e?.user?.mobile}
                                            </td>
                                            <td>
                                                {moment(e?.createdAt).format('lll')}
                                            </td>
                                            <td>
                                                <Button
                                                    type='button' color='success' className='mr-auto btn-sm'
                                                    onClick={() => setDetails(e.details)}
                                                >
                                                    View
                                                </Button>
                                            </td>
                                        </tr>
                                    </React.Fragment>
                                ))
                            }
                        </tbody>
                    </Table>

                    <CardFooter className="py-4">
                        <nav aria-label="...">
                            <Pagination
                                className="pagination justify-content-end mb-0"
                                listClassName="justify-content-end mb-0"
                            >
                                <PaginationItem className="pointer">
                                    <PaginationLink
                                        href="#"
                                        onClick={(e) => page === 0 ? '' : setPage(page - 1)}
                                    >
                                        <i className="lni lni-chevron-left"></i>
                                        <span className="sr-only">Previous</span>
                                    </PaginationLink>
                                </PaginationItem>

                                <PaginationItem className={page === 0 ? 'active' : ''}>
                                    <PaginationLink
                                        href="#"
                                        onClick={(e) => page === 0 ? '' : setPage(page - 1)}
                                    >
                                        {page === 0 ? 1 : page}
                                    </PaginationLink>
                                </PaginationItem>

                                {
                                    (page === 0 ? totalPages > 1 : true) ?
                                        <PaginationItem className={page === 0 ? '' : 'active'}>
                                            <PaginationLink
                                                href="#"
                                                onClick={(e) => page === 0 ? setPage(1) : ''}
                                            >
                                                {page === 0 ? 2 : page + 1}
                                            </PaginationLink>
                                        </PaginationItem>
                                        :
                                        null
                                }

                                {
                                    (page === 0 ? 2 < totalPages : page + 1 < totalPages) ?
                                        <PaginationItem>
                                            <PaginationLink
                                                href="#"
                                                onClick={(e) => page === 0 ? setPage(2) : setPage(1)}
                                            >
                                                {page === 0 ? 3 : page + 2}
                                            </PaginationLink>
                                        </PaginationItem>
                                        :
                                        null
                                }

                                <PaginationItem>
                                    <PaginationLink
                                        href="#"
                                        onClick={(e) => page + 1 < totalPages ? setPage(page + 1) : ''}
                                    >
                                        <i className="lni lni-chevron-right"></i>
                                        <span className="sr-only">Next</span>
                                    </PaginationLink>
                                </PaginationItem>
                            </Pagination>
                        </nav>
                    </CardFooter>

                </Card>

            </Container>

            <Modal isOpen={!!(details)} toggle={() => setDetails(null)}>
                <ModalHeader toggle={() => setDetails(null)}>Upload File</ModalHeader>
                <ModalBody>
                    E-Mail: {details?.email}<br />
                    Name: {details?.first_name} {details?.last_name}<br />
                    phone: {details?.mobile}<br />
                    City: {details?.city}<br />
                    country: {details?.country}<br />
                    age: {details?.age}<br />
                    gender: {details?.gender}<br />
                    weight: {details?.weight}<br />
                    activity level: {details?.activity_level}<br />
                    height: {details?.height}<br />
                    preferred day: {details?.preferred_day}<br />
                    preferred time: {details?.preferred_time}<br />
                    selected plan: {details?.selected_plan}<br />
                    message: {details?.message}<br />
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={() => setDetails(null)}>Close</Button>
                </ModalFooter>
            </Modal>

        </>
    );
};
