import React, { useEffect, useState } from "react";

import {
	Button,
	Card,
	CardHeader,
	CardBody,
	FormGroup,
	Form,
	Input,
	Container,
	Row,
	Col,
	Label
} from "reactstrap";
import { toast } from "react-toastify";

import _fetch from "../../../_fetch.js";
import Loader from "../../../components/Loader/loader.js";
import moment from "moment";
import { useParams } from "react-router-dom";
import AdminHeader from "../../../components/Headers/AdminHeader.js";

export default function AddClass() {

	const [categories, setCategories] = useState([]);
	const [loading, setLoading] = useState(null);
	const [data, setData] = useState({
		type: '',
		title: '',
		description: '',
		date: '',
		time: '',
		duration_in_minutes: 0,
		meeting_link: '',
		recording_link: '',
		category: '',
		order: 0
	});
	const { id } = useParams();

	useEffect(() => {

		async function init() {
			try {
				setLoading(true);
				let res = await _fetch(`${process.env.REACT_APP_API}/category`)
				if (res.success && Array.isArray(res.response)) {
					setCategories(res.response);
				}
			} catch (err) {
				console.log(err);
			} finally {
				setLoading(false);
			}
		}

		init();

		if (id) {
			_fetch(`${process.env.REACT_APP_API}/classes?ids=["${id}"]`)
				.then(res => {
					if (res.success && Array.isArray(res.response)) {
						let date = res.response[0].date;
						setData({
							...res.response[0],
							date: date ? moment(date).format('YYYY-MM-DD') : '',
							time: date ? moment(date).format('HH:mm') : '',
							category: res.response[0].category._id
						})
					}
				})
				.catch(err => {
					console.log(err);
				});
		}

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	async function addClass(ev) {

		ev.preventDefault();

		if (!data.title) return toast.error('title is required');
		if (!data.type) return toast.error('type is required');
		if (!data.category) return toast.error('category is required');
		if (data.type === 'LIVE' && (!data.date || !data.time)) return toast.error('date and time are required');

		setLoading(true);

		let body = {
			...data,
			date: moment(`${data.date} ${data.time}`).toDate()
		}

		if (id) body._id = id;

		try {
			console.log(data);
			const res = await _fetch(`${process.env.REACT_APP_API}/classes`, { method: id ? "PATCH" : "POST", body: body });
			if (res.success) {
				toast.success(`Class ${id ? 'updated' : 'added'} successfully`);
			} else toast.error(res.response);
		} catch (err) {
			console.log(err);
			toast.error('Some error occurred while adding the class');
		}
		setLoading(false);
	}

	const onChange = (ev) => {

		if (ev.target.name === 'date' || ev.target.name === 'time') {
			console.log(ev.target.value)
		}

		setData(prev => ({ ...prev, [ev.target.name]: ev.target.value }));
	}

	return (
		<>
			<AdminHeader />

			<Container className="mt--9" fluid>

				<Card className="bg-secondary shadow">

					<CardHeader className="bg-white border-0">
						<Row className="align-items-center">
							<Col xs="8">
								<h3 className="mb-0">{!id ? 'Add' : 'Update'} Class</h3>
							</Col>
							<Col className="text-right" xs="4">
								<Button
									color="primary"
									onClick={addClass}
									disabled={!!loading}
								>
									{loading ? <Loader size={20} /> : `${!id ? 'Add' : 'Update'} Class`}
								</Button>
							</Col>
						</Row>
					</CardHeader>

					<CardBody>
						<Form onSubmit={addClass}>

							<div className="pl-lg-4">

								<Row>
									<Col lg="4">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Class Title
											</label>
											<Input
												className="form-control-alternative"
												name='title'
												id="input-first-name"
												type="text" required={false}
												onChange={onChange}
												value={data.title}
											/>
										</FormGroup>
									</Col>
									<Col lg="4">
										<label
											className="form-control-label"
											htmlFor="input-first-name"
										>
											Class Type
										</label>
										<div>
											<FormGroup check inline>
												<Label check>
													<Input type='radio' name='type' value='LIVE' checked={data.type === 'LIVE'} onChange={onChange} /> Live Class
												</Label>
											</FormGroup>
											<FormGroup check inline>
												<Label check>
													<Input type='radio' name='type' value='RECORDED' checked={data.type === 'RECORDED'} onChange={onChange} /> Workout Video
												</Label>
											</FormGroup>
										</div>
									</Col>
									<Col lg="4">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Class Category
											</label>
											<Input
												className="form-control-alternative"
												id="input-gender"
												name='category'
												type="select"
												onChange={onChange}
												value={data.category}
												defaultValue=''
											>
												<option value='' disabled>Select Category</option>
												{
													categories.map(e => (
														<option value={e._id} key={e._id}>{e.name}</option>
													))
												}
											</Input>
										</FormGroup>
									</Col>
								</Row>

								<Row>

									{
										data.type === 'LIVE' &&
										<Col lg="4">
											<FormGroup>
												<label
													className="form-control-label"
													htmlFor="input-first-name"
												>
													Meeting Link
												</label>
												<Input
													className="form-control-alternative"
													name='meeting_link'
													id="input-first-name"
													type="text" required={false}
													onChange={onChange}
													value={data.meeting_link}
												/>
											</FormGroup>
										</Col>
									}
									<Col lg="4">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Recording Link
											</label>
											<Input
												className="form-control-alternative"
												name='recording_link'
												id="input-first-name"
												type="text" required={false}
												onChange={onChange}
												value={data.recording_link}
											/>
										</FormGroup>
									</Col>
									<Col lg="4">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Class Duration
											</label>
											<Input
												className="form-control-alternative"
												name='duration_in_minutes'
												id="input-first-name"
												type="text" required={false}
												onChange={onChange}
												value={data.duration_in_minutes}
											/>
										</FormGroup>
									</Col>
								</Row>

								<Row>
									{
										data.type === 'LIVE' &&
										<Col lg="4">
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Class Date and Time
											</label>
											<div>
												<FormGroup check inline>
													<Label check>
														<Input type='date' name='date' onChange={onChange} value={data.date} />
													</Label>
												</FormGroup>
												<FormGroup check inline>
													<Label check>
														<Input type='time' name='time' onChange={onChange} value={data.time} />
													</Label>
												</FormGroup>
											</div>
										</Col>
									}
									{
										data.type === 'RECORDED' &&
										<Col lg="4">
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Order
											</label>
											<Input
												className="form-control-alternative"
												name='order'
												id="input-first-name"
												type="text" required={false}
												onChange={onChange} value={data.order}
											/>
										</Col>
									}
									<Col lg="8">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												Class Summary
											</label>
											<Input
												className="form-control-alternative"
												rows="3"
												type="textarea"
												name="description"
												onChange={onChange}
												defaultValue={data.description}
											/>
										</FormGroup>
									</Col>
								</Row>
							</div>

						</Form>
					</CardBody>

				</Card>

			</Container>
		</>
	);
};
