import React, { useCallback, useEffect, useState } from "react";
import {
    Card, CardHeader, CardFooter, Pagination, PaginationItem, PaginationLink,
    Table, Container, Button, Modal, ModalHeader, ModalBody, ModalFooter
} from "reactstrap";
import update from 'immutability-helper';
import moment from "moment";
import { toast } from "react-toastify";

import AdminHeader from "../../../components/Headers/AdminHeader.js";
import _fetch from '../../../_fetch';
import Loader from "../../../components/Loader/loader.js";

const pageLength = 10;

export default function SixWeekNSPEnrollments() {

    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(0);
    const [totalPages, setTotalPages] = useState(1);
    const [file, setFile] = useState(null);
    const [_id, setId] = useState(null);
    const [uploading, setUploading] = useState(null);
    const [details, setDetails] = useState(null);

    const [enrollments, setEnrollments] = useState([]);

    const getClasses = useCallback(async () => {
        try {
            setLoading(true);

            let url = `${process.env.REACT_APP_API}/enrollment/admin/6_week_diet`;

            let res = await _fetch(url);
            if (res.success) {
                console.log(res.response);
                setEnrollments(res.response);
                setTotalPages(Math.ceil(res.response.length / pageLength));
            }

        } catch (err) {
            console.log(err);
        } finally {
            setLoading(false);
        }
    }, []);

    async function upload() {
        try {
            setUploading(true);
            let res = await _fetch(`${process.env.REACT_APP_API}/media/admin/file_signed_url?_id=${enrollments[_id].user._id}`);

            const formData = new FormData();

            let name = file.name;

            let url = `${enrollments[_id].user._id}/diet/${new Date().getMilliseconds()}${name}`

            formData.append('key', url);
            formData.append('acl', 'public-read');

            for (let a in res.response) {
                formData.append(a, res.response[a]);
            }

            formData.append('file', file);
            res = await fetch(`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}`, {
                method: 'POST',
                body: formData
            });

            if (res.status === 204) {
                console.log('res', res);
                try {
                    const res = await _fetch(
                        `${process.env.REACT_APP_API}/enrollment/admin/6_week_diet`,
                        {
                            method: "PATCH",
                            body: {
                                _id: enrollments[_id]._id,
                                url: `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/${url}`,
                                name
                            }
                        }
                    );
                    if (res.success) {
                        toast.success('File uploaded successfully');
                        setEnrollments(update(enrollments, { [_id]: { files: { $push: [{ url, name, date: new Date() }] } } }));
                    }
                } catch (err) {
                    console.log(err);
                    toast.error('Some error occurred while uploading file');
                }
                setFile(null);
            }
            setUploading(false);

        } catch (err) {
            console.log(err);
            toast.error('Some error occurred while uploading file');
            setUploading(false);
        }
    }

    async function deleteFile(eid, fid) {
        try {
            const res = await _fetch(
                `${process.env.REACT_APP_API}/enrollment/files`,
                {
                    method: "DELETE",
                    body: { eid: enrollments[eid]._id, fid: enrollments[eid].files[fid]._id }
                }
            );
            if (res.success) {
                toast.success('File removed successfully');
                setEnrollments(update(enrollments, { [eid]: { files: { $splice: [[fid, 1]] } } }));
            } else toast.error(res.response);
        } catch (err) {
            console.log(err);
            toast.error('Some error occurred while deleting file');
        }
    }

    useEffect(() => {
        getClasses();
    }, [getClasses]);

    if (loading) return (
        <>
            <AdminHeader />

            <Container className="mt--7" fluid>
                <Card className="bg-secondary shadow" style={{ height: 400 }}>
                    <Loader />
                </Card>
            </Container>
        </>
    );

    return (
        <>
            <AdminHeader />
            {/* Page content */}
            <Container className="mt--9" fluid>
                <Card className="shadow">

                    <CardHeader className="border-0">
                        <h3 className="mb-0">6 Week Nutrition Support Plan Enrollments</h3>
                    </CardHeader>

                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                            <tr>
                                <th scope="col">Diet Plan File</th>
                                <th scope="col">Upload Date</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                enrollments?.slice(page * pageLength, (page + 1) * pageLength).map((e, i) => (
                                    <React.Fragment key={i}>
                                        <tr className='br-secondary'>
                                            <th
                                                colSpan={3}
                                                scope='colgroup'
                                                className='pointer'
                                                onClick={() => setEnrollments(update(enrollments, { [page * pageLength + i]: { expanded: { $set: !e.expanded } } }))}
                                            >
                                                <div className='w-100 d-flex justify-content-between align-items-center'>
                                                    <span>{e?.user?.first_name} {e?.user?.last_name} ({e?.user?.email}, {e?.user?.mobile})</span>
                                                    <div className='ml-auto'>
                                                        <label
                                                            className="btn btn-primary btn-sm my-0"
                                                            htmlFor="input-profile-picture"
                                                        >
                                                            Upload Files
                                                        </label>
                                                        <input
                                                            id="input-profile-picture"
                                                            type="file" hidden accept='application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
                                                            multiple={false}
                                                            onChange={(ev) => {
                                                                setId(page * pageLength + i);
                                                                const temp = ev.target.files;
                                                                // push raw photo/video file into arr1
                                                                for (let i = 0; i < temp.length; i++) {
                                                                    const file = temp[i];
                                                                    return setFile(file);
                                                                }
                                                            }}
                                                        />
                                                    </div>
                                                    <Button
                                                        type='button' color='success' className='ml-2 btn-sm'
                                                        onClick={(ev) => {
                                                            ev.stopPropagation();
                                                            setDetails(e.details);
                                                        }}
                                                    >
                                                        View Details
                                                    </Button>
                                                </div>
                                            </th>
                                        </tr>
                                        {
                                            e.expanded ?
                                                e.files.map((f, ind) => (
                                                    <tr key={ind}>
                                                        <th scope="row">
                                                            {f.name}
                                                        </th>
                                                        <td>
                                                            {moment(f.date).format('lll')}
                                                        </td>
                                                        <td>
                                                            <Button
                                                                type='button' color='success' className='mr-2 btn-sm'
                                                                onClick={() => window.open(f.url)}
                                                            >
                                                                View
                                                            </Button>
                                                            <Button
                                                                type='button' color='success' className='mr-auto btn-sm'
                                                                onClick={() => deleteFile(page * pageLength + i, ind)}
                                                            >
                                                                Delete
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                                :
                                                null
                                        }
                                    </React.Fragment>
                                ))
                            }
                        </tbody>
                    </Table>

                    <CardFooter className="py-4">
                        <nav aria-label="...">
                            <Pagination
                                className="pagination justify-content-end mb-0"
                                listClassName="justify-content-end mb-0"
                            >
                                <PaginationItem className="pointer">
                                    <PaginationLink
                                        href="#"
                                        onClick={(e) => page === 0 ? '' : setPage(page - 1)}
                                    >
                                        <i className="lni lni-chevron-left"></i>
                                        <span className="sr-only">Previous</span>
                                    </PaginationLink>
                                </PaginationItem>

                                <PaginationItem className={page === 0 ? 'active' : ''}>
                                    <PaginationLink
                                        href="#"
                                        onClick={(e) => page === 0 ? '' : setPage(page - 1)}
                                    >
                                        {page === 0 ? 1 : page}
                                    </PaginationLink>
                                </PaginationItem>

                                {
                                    (page === 0 ? totalPages > 1 : true) ?
                                        <PaginationItem className={page === 0 ? '' : 'active'}>
                                            <PaginationLink
                                                href="#"
                                                onClick={(e) => page === 0 ? setPage(1) : ''}
                                            >
                                                {page === 0 ? 2 : page + 1}
                                            </PaginationLink>
                                        </PaginationItem>
                                        :
                                        null
                                }

                                {
                                    (page === 0 ? 2 < totalPages : page + 1 < totalPages) ?
                                        <PaginationItem>
                                            <PaginationLink
                                                href="#"
                                                onClick={(e) => page === 0 ? setPage(2) : setPage(1)}
                                            >
                                                {page === 0 ? 3 : page + 2}
                                            </PaginationLink>
                                        </PaginationItem>
                                        :
                                        null
                                }

                                <PaginationItem>
                                    <PaginationLink
                                        href="#"
                                        onClick={(e) => page + 1 < totalPages ? setPage(page + 1) : ''}
                                    >
                                        <i className="lni lni-chevron-right"></i>
                                        <span className="sr-only">Next</span>
                                    </PaginationLink>
                                </PaginationItem>
                            </Pagination>
                        </nav>
                    </CardFooter>

                </Card>

            </Container>



            <Modal isOpen={!!(file)} toggle={() => setFile(null)}>
                <ModalHeader toggle={() => setFile(null)}>Upload File</ModalHeader>
                <ModalBody>
                    {file?.name}
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => upload()} className='mr-2' disabled={uploading}>
                        {uploading ? <Loader size={20} /> : 'Upload'}
                    </Button>
                    <Button color="secondary" onClick={() => setFile(null)} disabled={uploading}>Cancel</Button>
                </ModalFooter>
            </Modal>


            <Modal isOpen={!!(details)} toggle={() => setDetails(null)}>
                <ModalHeader toggle={() => setDetails(null)}>Upload File</ModalHeader>
                <ModalBody>
                    E-Mail: {details?.email}<br />
                    Name: {details?.first_name} {details?.last_name}<br />
                    phone: {details?.mobile}<br />
                    City: {details?.city}<br />
                    country: {details?.country}<br />
                    age: {details?.age}<br />
                    gender: {details?.gender}<br />
                    weight: {details?.weight}<br />
                    activity level: {details?.activity_level}<br />
                    height: {details?.height}<br />
                    preferred day: {details?.preferred_day}<br />
                    preferred time: {details?.preferred_time}<br />
                    selected plan: {details?.selected_plan}<br />
                    message: {details?.message}<br />
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={() => setDetails(null)}>Close</Button>
                </ModalFooter>
            </Modal>

        </>
    );
};
