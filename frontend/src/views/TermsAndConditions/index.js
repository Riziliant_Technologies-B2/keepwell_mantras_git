import React from 'react';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import './about.css';

export default function Privacy() {

    return (
        <>
            <Header2 />

            <header className="privacy-header">
                <h1 className="privacy-title">Website Terms &amp; Conditions</h1>
            </header>

            <main className="main">

                <div className="privacy-content-wrapper container p-3">

                    <p>
                        Welcome to our website (https://www.keepwellmantras.com/). If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Prapti Dutt Limited’s relationship with you in relation to this website.
                    </p>

                    <p>
                        The term ‘Prapti Dutt Limited’ or ‘Keep Well Mantras’ or ‘us’ or ‘we’ refers to the owner of the website whose registered office is in London. The term ‘you’ refers to the user or viewer of our website.
                    </p>

                    <strong>The use of this website is subject to the following terms of use:</strong>

                    <ul>
                        <li>
                            the content of the pages of this website is for your general information and use only. It is subject to change without notice
                        </li>
                        <li>
                            neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law
                        </li>
                        <li>
                            this website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions
                        </li>
                        <li>
                            all trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website
                        </li>
                        <li>
                            unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence
                        </li>
                        <li>
                            from time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s)
                        </li>
                        <li>
                            you may not create a link to this website from another website or document without Prapti Dutt Limited’s prior written consent
                        </li>
                        <li>
                            your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Scotland and Wales.
                        </li>
                    </ul>
                    <strong>Disclaimer</strong>

                    <p>
                        The information contained in this website (https://www.keepwellmantras.com/) is for general information purposes only. The information is provided by Prapti Dutt Limited and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.
                    </p>
                    <p>
                        In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.
                    </p>
                    <p>Through this website you are able to link to other websites which are not under the control of Prapti Dutt Limited. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.
                    </p>
                    <p>Every effort is made to keep the website up and running smoothly. However, Prapti Dutt Limited takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.
                    </p>
                    <strong>Copyright</strong>
                    <p>
                        This website https://www.keepwellmantras.com/) and its content is copyright of Prapti Dutt Limited – © Prapti Dutt Limited. All rights reserved.
                    </p>
                    <p>
                        Any redistribution or reproduction of part or all of the contents in any form is prohibited other than the following:
                    </p>
                    <ul>
                        <li>you may print or download to a local hard disk extracts for your personal and non-commercial use only</li>
                        <li>you may copy the content to individual third parties for their personal use, but only if you acknowledge the website as the source of the material</li>
                        <li>you may not, except with our express written permission, distribute or commercially exploit the content. Nor may you transmit it or store it in any other website or other form of electronic retrieval system.</li>
                    </ul>

                </div>
            </main>

            <Footer />

        </>
    );
}