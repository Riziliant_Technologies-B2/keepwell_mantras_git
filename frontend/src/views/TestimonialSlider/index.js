import React from 'react';
import Slider from "react-slick";
// import './style.css';
import { testimonials } from './testimonial';

export default function Testimonial({ slides, className }) {
    return (
        <>
        <div class="testimonial-area bg5 paralax overlay pad90">
            <div class="container">
        <div class="section-title text-center">
                    <div class="title-bar full-width mb20">
                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img"/>
                    </div>
                    <h3>testimonial</h3>
                    </div>
                    </div>
                    <div class="container custom_slid">
        <Slider
            dots={false} infinite={true} slidesToScroll={1} speed={500} autoplay={true} autoplaySpeed={3000}
            slidesToShow={slides} pauseOnHover={true} className={className}
            // prevArrow={<div><i className="lni lni-chevron-left-circle text-dark"></i></div>}
            // nextArrow={<div><i className="lni lni-chevron-right-circle text-dark"></i></div>}
        >
            {
                testimonials.map(e => (
                 
                    <div class="row">  

                    <div class="col-md-12">
                        <div className='testimonial-item' key={e.name}>
                            <div class="test-img-2">
                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`} alt="user-img"/>
                    </div>
                    <h4>{e.name}</h4>
                    <p>
                        {e.review}
                    </p>
                </div>
                </div>
               </div>
                  
                ))
            }
        </Slider>
        </div>
        </div>
        </>
    )

}