import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import Header from '../../components/Headers/Header';
import DietPackageForm from './form';
import Payment from './payment';

export default function DietPackage() {

    const { email, first_name, last_name, mobile, gender } = useSelector(st => st.user);
    const [order, setOrder] = useState(null);
    const [data, setData] = useState({
        first_name: first_name,
        last_name: last_name,
        email: email,
        mobile: mobile,
        city: '',
        country: '',
        age: '-1',
        gender: gender || '-1',
        weight: '',
        height: '',
        waist: '',
        activity_level: '-1',
        preferred_day: '',
        preferred_time: '',
        selected_plan: '',
        message: ''
    });


    return (
        <>
            <Header />
            {
                order ?
                <Payment order={order} />
                :
                <DietPackageForm setOrder={setOrder} data={data} setData={setData}/>
            }
        </>
    );
}