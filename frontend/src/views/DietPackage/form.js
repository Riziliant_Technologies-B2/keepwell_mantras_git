import React, { useState } from 'react';
import { Container, Card, CardBody, CardHeader, Form, Button, Col, Row, Input, FormGroup, Label } from 'reactstrap';
import { Spinner } from 'reactstrap';
import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';


export default function DietPackageForm({ setOrder, data, setData }) {


    const [selected, setSelected] = useState(1);
    const [err, setErr] = useState('');
    const [sending, setSending] = useState(false);
    const [, setCookies] = useCookies(['diet_package']);
    let history = useHistory();

    async function redirectToPaymentPage(ev) {

        try {

            ev.preventDefault();
            setErr('');

            let message = '';

            for (let p in data) {
                if (!data[p] || data[p] === '-1') {
                    message = `${p.replace('_', ' ')} is required`;
                    break;
                }
            }

            if (message) {
                setErr(message);
                return;
            }

            let body = {
                diet_plan: data.selected_plan,
                details: JSON.parse(JSON.stringify(data))
            };

            body.details.selected_plan = data.selected_plan === '60_MINS' ? '60 Minutes Nutrition Consultation Call' : '6 Weeks Nutrition Support Plan';
            setCookies('diet_package', body)
            history.push('/buy-classes');

        } catch (err) {
            console.log(err);
            setSending(false);
        }
    }

    const onChange = (ev) => setData(prev => ({ ...prev, [ev.target.name]: ev.target.value }));

    return (
        <Container className="mt--7" fluid>
            <Card className="bg-secondary shadow">
                <CardHeader className="bg-transparent">
                    <h2 className="mb-0">Nutrition Support Plan</h2>
                </CardHeader>
                <CardBody>

                    <Row className='mb-4'>
                        <Col xs={12} md={6} className='my-3'>
                            <Card
                                onClick={() => setSelected(1)}
                                className={`p-4 text-center text-xl font-weight-600 pointer shadow card-lift--hover ${selected === 1 ? 'bg-primary text-white' : ''}`}
                            >
                                60 Minutes Nutrition Consultation Call
                            </Card>
                        </Col>
                        <Col xs={12} md={6} className='my-3'>
                            <Card
                                onClick={() => setSelected(2)}
                                className={`p-4 text-center text-xl font-weight-600 pointer shadow card-lift--hover ${selected === 2 ? 'bg-primary text-white' : ''}`}
                            >
                                6 Weeks Nutrition Support Plan
                            </Card>
                        </Col>
                    </Row>

                    <Card className='mb-4'>
                        <CardBody>
                            <div className='d-flex align-items-start pb-4 mb-4 border-bottom'>
                                <i className='lni lni-flag mx-auto mt-1' style={{ fontSize: '2rem' }} />
                                <div className='flex-fill ml-3'>
                                    <h4 className='text-lg'>Goal</h4>
                                    <p className='mb-0'>
                                        {
                                            selected === 1 ?
                                                'The goal is to fill up the nutrition gaps. We will go over your existing diet and lifestyle and discuss the right eating strategies and lifestyle modifications required to achieve your fitness goals. During our consultation call, I will do a thorough study of your existing food diary to provide you with key action points to take away from the call and implement in your diet. The service includes consultation and guidance with easy to adopt lifestyle changes. If you have been eating healthy but stuck with a weight loss plateau or just need guidance from time to time, this is the perfect plan.'
                                                :
                                                <React.Fragment>
                                                    To implement a 6 week duration plan of action with the right nutrition and lifestyle enhancements customized to your fitness goals. The initial consultation call will be followed by a diet sheet with sample meal ideas customised to your existing food habits and lifestyle. Option to choose from a set meal plan ideas or calorie counting diet approach will be discussed during the call.
                                                    <br />
                                                    This service enables you to reach out to me for support throughout the 6 weeks process for any queries or questions you may have. A bi-weekly catch up will be conducted to go through the results and changes on diet as needed. We will be monitoring the results through changes in scale, body stats and progress pics. Clients should be ideally providing these to me before our bi-weekly catch up calls.
                                                </React.Fragment>
                                        }

                                    </p>
                                </div>
                            </div>
                            <Row>
                                <Col xs={12} md={6} lg={selected === 1 ? 4 : 3}>
                                    <div className='d-flex align-items-start'>
                                        <i className='lni lni-timer mx-auto mt-1' style={{ fontSize: '2rem' }} />
                                        <div className='flex-fill ml-3'>
                                            <h4 className='text-lg'>Duration</h4>
                                            <p className='mb-0'>
                                                {
                                                    selected === 1 ?
                                                        '1 time - 60 minute call'
                                                        :
                                                        '6 Weeks'
                                                }
                                            </p>
                                        </div>
                                    </div>

                                </Col>
                                {
                                    selected === 2 &&
                                    <Col xs={12} md={6} lg={selected === 1 ? 4 : 3}>
                                        <div className='d-flex align-items-start'>
                                            <i className='lni lni-phone-set mx-auto mt-1' style={{ fontSize: '2rem' }} />
                                            <div className='flex-fill ml-3'>
                                                <h4 className='text-lg'>Progress check ins </h4>
                                                <p className='mb-0'>
                                                    Bi-weekly check ins
                                                </p>
                                            </div>
                                        </div>
                                    </Col>
                                }
                                <Col xs={12} md={6} lg={selected === 1 ? 4 : 3}>
                                    <div className='d-flex align-items-start'>
                                        <i className='lni lni-pound mx-auto mt-1' style={{ fontSize: '2rem' }} />
                                        <div className='flex-fill ml-3'>
                                            <h4 className='text-lg'>Fee</h4>
                                            <p className='mb-0'>
                                                {
                                                    selected === 1 ?
                                                        '£40 per session'
                                                        :
                                                        '£100'
                                                }
                                            </p>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} md={6} lg={selected === 1 ? 4 : 3}>
                                    <div className='d-flex align-items-start'>
                                        <i className='lni lni-calendar mx-auto mt-1' style={{ fontSize: '2rem' }} />
                                        <div className='flex-fill ml-3'>
                                            <h4 className='text-lg'>Roll over plan</h4>
                                            <p className='mb-0'>
                                                {
                                                    selected === 1 ?
                                                        '20% off on consecutive call bookings'
                                                        :
                                                        '25% off on consecutive plan booking'
                                                }
                                            </p>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <div className='d-flex align-items-start mb-4 pt-4 mt-4 border-top'>
                                <i className='lni lni-book mx-auto mt-1' style={{ fontSize: '2rem' }} />
                                <div className='flex-fill ml-3'>
                                    <h4 className='text-lg'>Terms And Conditions</h4>
                                    <div className='d-flex align-items-start mb-2'>
                                        <i className='lni lni-checkmark mt-1 mx-auto' />
                                        <div className='flex-fill ml-3'>
                                            The client is required to fill in a client questionnaire prior to starting the process requiring all important information to conduct an in depth analysis of the current lifestyle and diet. This will enable me to provide you a customized plan rightly suiting your existing food habits and lifestyle.
                                        </div>
                                    </div>
                                    <div className='d-flex align-items-start mb-2'>
                                        <i className='lni lni-checkmark mt-1 mx-auto' />
                                        <div className='flex-fill ml-3'>
                                            Our success lies in team work which is highly dependent on being jointly accountable towards your transformation journey. Goal is to attain a healthy weight loss whilst improving overall health through simple lifestyle changes. As a coach I will be trying my best to make the plan sustainable and achievable through easy to adopt lifestyle changes wherever needed.
                                        </div>
                                    </div>
                                    <div className='d-flex align-items-start mb-2'>
                                        <i className='lni lni-checkmark mt-1 mx-auto' />
                                        <div className='flex-fill ml-3'>
                                            Regular updates through scale movements, body stats measurements and overall general well being are the only way we will know the success or opportunity to modify anything. Clients are required to keep the coach updated of these from time to time in this process.
                                        </div>
                                    </div>
                                    <div className='d-flex align-items-start mb-2'>
                                        <i className='lni lni-checkmark mt-1 mx-auto' />
                                        <div className='flex-fill ml-3'>
                                            Once a plan is booked, refunds are not issued due to your unavailability or vacation or any other personal reasons. However, a delayed start date is possible.
                                        </div>
                                    </div>
                                    <div className='d-flex align-items-start mb-2'>
                                        <i className='lni lni-checkmark mt-1 mx-auto' />
                                        <div className='flex-fill ml-3'>
                                            To get the best results from the nutrition plan, it is highly recommended to incorporate physical activity as an important element. I offer discounts when my clients opt in for both diet and workouts combo packages.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </CardBody>
                    </Card>

                    <Card className='mb-4'>
                        <CardHeader>
                            <h2 className="mb-0">Book An Appointment</h2>
                        </CardHeader>
                        <CardBody>
                            <Form onSubmit={redirectToPaymentPage}>

                                {/* basic info */}
                                <h6 className="heading-small text-muted mb-4">
                                    Contact Detail
                                </h6>
                                <div className="pl-lg-4">

                                    <Row>

                                        <Col lg="6">
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-first-name"
                                                >
                                                    First name
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.first_name}
                                                    name='first_name'
                                                    id="input-first-name"
                                                    placeholder="First name"
                                                    type="text" required={false}
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col lg="6">
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-last-name"
                                                >
                                                    Last name
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.last_name}
                                                    name='last_name'
                                                    id="input-last-name"
                                                    placeholder="Last name"
                                                    type="text"
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>

                                    <Row>

                                        <Col lg="6">
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-mobile"
                                                >
                                                    Mobile Number
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.mobile}
                                                    id="input-mobile"
                                                    name='mobile'
                                                    placeholder='Mobile No.'
                                                    type="text"
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col lg="6">
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-email"
                                                >
                                                    Email
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.email}
                                                    id="input-email"
                                                    name='email'
                                                    placeholder='Email'
                                                    type="text"
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>

                                    <Row>

                                        <Col lg="6">
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-city"
                                                >
                                                    City
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.area}
                                                    id="input-city"
                                                    name='city'
                                                    placeholder='City'
                                                    type="text"
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col lg="6">
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-username"
                                                >
                                                    Country
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.country}
                                                    id="input-country"
                                                    name='country'
                                                    placeholder='Country'
                                                    type="text"
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>
                                </div>
                                <hr className='my-4' />

                                {/* medical info */}
                                <h6 className="heading-small text-muted mb-4">
                                    Personal Detail
                                </h6>
                                <div className="pl-lg-4">
                                    <Row>

                                        <Col lg={4}>
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-age"
                                                >
                                                    Age Group
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    id="input-age"
                                                    name='age'
                                                    type="select"
                                                    value={data.age}
                                                    onChange={onChange}
                                                >
                                                    <option value='-1' disabled>-- Choose Age Group --</option>
                                                    <option value='20-30'>20-30</option>
                                                    <option value='30-40'>30-40</option>
                                                    <option value='40-50'>40-50</option>
                                                    <option value='50-60'>50-60</option>
                                                    <option value='60 above'>60 above</option>
                                                </Input>
                                            </FormGroup>
                                        </Col>

                                        <Col lg={4}>
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-gender"
                                                >
                                                    Gender
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    id="input-gender"
                                                    name='gender'
                                                    type="select"
                                                    value={data.gender}
                                                    onChange={onChange}
                                                >
                                                    <option value='-1' disabled>-- Choose Gender --</option>
                                                    <option value='FEMALE'>Female</option>
                                                    <option value='MALE'>Male</option>
                                                    <option value='OTHER'>Others</option>
                                                    <option value='RATHER_NOT_SAY'>Rather Not Say</option>
                                                </Input>
                                            </FormGroup>
                                        </Col>

                                        <Col lg={4}>
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-activity_level"
                                                >
                                                    Activity Level
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    id="input-activity_level"
                                                    name='activity_level'
                                                    type="select"
                                                    value={data.activity_level}
                                                    onChange={onChange}
                                                >
                                                    <option value='-1' disabled>-- Choose Activity Level --</option>
                                                    <option value='Sedentary'>Sedentary: little or no exercise</option>
                                                    <option value='Light'>Light: exercise 1-3 times/week</option>
                                                    <option value='Moderate'>Moderate: 4-5 times/week</option>
                                                    <option value='Very Active'>Very Active: intense exercise 6-7 times/week</option>
                                                    <option value='Extra Active'>Extra Active: very intense exercise daily, or physical job</option>
                                                </Input>
                                            </FormGroup>
                                        </Col>

                                        <Col lg={4}>
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-weight"
                                                >
                                                    Weight (in kg)
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.weight}
                                                    name='weight'
                                                    id="input-weight"
                                                    placeholder="Weight"
                                                    type='number' required={false}
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col lg={4}>
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-waist"
                                                >
                                                    Waist (in inch)
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.waist}
                                                    name='waist'
                                                    id="input-waist"
                                                    placeholder="Waist"
                                                    type='number'
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                        <Col lg={4}>
                                            <FormGroup>
                                                <label
                                                    className="form-control-label"
                                                    htmlFor="input-profession"
                                                >
                                                    Height (in cm)
                                                </label>
                                                <Input
                                                    className="form-control-alternative"
                                                    value={data.height}
                                                    name='height'
                                                    id="input-height"
                                                    placeholder="Height"
                                                    type='number'
                                                    onChange={onChange}
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>
                                </div>
                                <hr className="my-4" />


                                {/* Program Detail */}
                                <h6 className="heading-small text-muted mb-4">Preferred day and time for the call</h6>
                                <div className="pl-lg-4">
                                    <Row>

                                        <Col lg={3}>
                                            <FormGroup>
                                                <legend className="form-control-label">Preferred Day</legend>
                                                <div className='d-flex'>
                                                    <FormGroup check>
                                                        <Label check>
                                                            <Input
                                                                type="radio" name="preferred_day"
                                                                value='Week Day' onChange={onChange}
                                                                checked={data.preferred_day === 'Week Day'}
                                                            />{' '}
                                                            Week Day
                                                        </Label>
                                                    </FormGroup>
                                                    <FormGroup check className='ml-3'>
                                                        <Label check>
                                                            <Input
                                                                type="radio" name="preferred_day"
                                                                value='Weekend' onChange={onChange}
                                                                checked={data.preferred_day === 'Weekend'}
                                                            />{' '}
                                                            Weekend
                                                        </Label>
                                                    </FormGroup>
                                                </div>
                                            </FormGroup>
                                        </Col>

                                        <Col lg={3}>
                                            <FormGroup>
                                                <legend className="form-control-label">Preferred Time</legend>
                                                <div className='d-flex'>
                                                    <FormGroup check>
                                                        <Label check>
                                                            <Input
                                                                type="radio" name="preferred_time"
                                                                value='9:00 am to 12:00 pm' onChange={onChange}
                                                                checked={data.preferred_time === '9:00 am to 12:00 pm'}
                                                            />{' '}
                                                            9:00 am to 12:00 pm
                                                        </Label>
                                                    </FormGroup>
                                                    <FormGroup check className='ml-3'>
                                                        <Label check>
                                                            <Input
                                                                type="radio" name="preferred_time"
                                                                value='1:00 pm to 6:00 pm' onChange={onChange}
                                                                checked={data.preferred_time === '1:00 pm to 6:00 pm'}
                                                            />{' '}
                                                            1:00 pm to 6:00 pm
                                                        </Label>
                                                    </FormGroup>
                                                </div>
                                            </FormGroup>
                                        </Col>

                                        <Col lg={6}>
                                            <FormGroup>
                                                <legend className="form-control-label">Selected Plan</legend>
                                                <div className='d-flex'>
                                                    <FormGroup check>
                                                        <Label check>
                                                            <Input
                                                                type="radio" name="selected_plan"
                                                                value='60_MINS'
                                                                onChange={onChange}
                                                                checked={data.selected_plan === '60_MINS'}
                                                            />{' '}
                                                            60 Minutes Nutrition Consultation Call
                                                        </Label>
                                                    </FormGroup>
                                                    <FormGroup check className='ml-3'>
                                                        <Label check>
                                                            <Input
                                                                type="radio" name="selected_plan"
                                                                value='6_WEEKS'
                                                                onChange={onChange}
                                                                checked={data.selected_plan === '6_WEEKS'}
                                                            />{' '}
                                                            6 Weeks Nutrition Support Plan
                                                        </Label>
                                                    </FormGroup>
                                                </div>
                                            </FormGroup>
                                        </Col>

                                    </Row>

                                    <FormGroup>
                                        <label
                                            className="form-control-label"
                                            htmlFor="input-gender"
                                        >
                                            Message
                                        </label>
                                        <Input
                                            className="form-control-alternative"
                                            placeholder="Message"
                                            rows="3"
                                            type="textarea"
                                            name="message"
                                            onChange={onChange}
                                            value={data.message}
                                        />
                                    </FormGroup>

                                </div>
                                <hr className="my-4" />

                                {err ? <p className='text-danger'>{err}</p> : ''}

                                <Button color='primary' type='submit'>
                                    {sending && <Spinner style={{ width: 20, height: 20 }} />} Submit
                                </Button>

                            </Form>
                        </CardBody>
                    </Card>

                </CardBody>
            </Card>
        </Container>
    );
}