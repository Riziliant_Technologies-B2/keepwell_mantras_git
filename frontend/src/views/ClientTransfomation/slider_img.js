export const slider_img = [
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-29.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-30.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-31.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-32.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-33.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-34.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-35.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-36.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-37.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-38.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-39.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-40.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-41.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-42.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-43.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-44.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-45.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-46.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-47.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-48.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-49.jpg`
    },
    {
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/trans-50.jpg`
    },    
];