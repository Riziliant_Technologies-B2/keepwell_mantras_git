import React, { Component } from "react";
import { Pagination, PaginationItem, PaginationLink, Collapse } from "reactstrap";

class ClientPagination extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: props.currentPage,
      toggleQuestion: 0,
    };
  }

  setToggequestion = (e, i) =>{
    e.preventDefault();
    this.setState({
      toggleQuestion: i
    });
  }

  handleClick = (e, index) => {
    e.preventDefault();
    this.setState({
      currentPage: index
    });
    document.body.scrollTop = 100;
    document.documentElement.scrollTop = 100;
  };

  render() {
    // const [toggleQuestion, setToggequestion] = useState(0);

    const { pageSize, pageCount, data } = this.props;
    const { currentPage, toggleQuestion } = this.state;

    let pageNumbers = [];
    for (let i = 0; i < pageCount; i++) {
      pageNumbers.push(
        <PaginationItem key={i} active={currentPage === i ? true : false}>
          <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
            {i + 1}
          </PaginationLink>
        </PaginationItem>
      );
    }
    const paginatedData = data.slice(
      currentPage * pageSize,
      (currentPage + 1) * pageSize
    );

    return (
      <React.Fragment>

        {
          paginatedData.map((datum, i) => (

            <div class="col-md-6" key={i}>
              <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                <div class="col p-4 d-flex flex-column position-static">
                  <h3 class="mb-0">{datum.name}</h3>
                  <p>{datum.content_1}</p>
                  <div class="title-bar full-width mb20">
                    <img src={datum.img_1} alt="title-img" />
                  </div>
                  <p>{datum.content_2} <span id="dots">...</span>
                    <Collapse isOpen={toggleQuestion === (i+1) ? true : false}>
                      {datum.read_more}
                    </Collapse>
                  </p>
                  <span onClick={e => this.setToggequestion(e, i+1)}>Read more</span>
                </div>
                <div class="col-auto d-none d-lg-block">
                  <img src={datum.img_2} alt="transformation img" width="200" height="250" />
                </div>
              </div>
            </div>

          ))
        }

        <React.Fragment>
<div class="container">
  <div class="row">
          <Pagination className="justify-content-center-2 Page navigation example">
            <PaginationItem>
              <PaginationLink
                onClick={e => this.handleClick(e, currentPage - 1)}
                previous
                href="#"
              />
            </PaginationItem>
            {pageNumbers}

            <PaginationItem disabled={currentPage >= pageCount - 1}>
              <PaginationLink
                onClick={e => this.handleClick(e, currentPage + 1)}
                next
                href="#"
              />
            </PaginationItem>
          </Pagination>
</div>
</div>
        </React.Fragment>
      </React.Fragment>
    );
  }
}

export default ClientPagination;
