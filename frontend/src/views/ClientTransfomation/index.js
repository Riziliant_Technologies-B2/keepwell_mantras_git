import React from 'react';
// import './style.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import { Link } from "react-router-dom";
import Slider from "react-slick";
import { youtube_link } from './youtube_link';
import { slider_img } from './slider_img';
import ClientPagination  from './ClientPagination';
import { client_data } from './client_data';

export default function Testimonial({ slides, className }) {

    var settings = {
        
        infinite: true,
        speed: 500,
        slidesToShow: 6,
        focusOnSelect: true,
        arrows: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    
                }
            },
            {
                breakpoint: 600,
                settings: {
                    
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    var settings_1 = {
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <>
            <Header2 />

            <div class="trans-bg page-head parallax overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <h3>Client Transformation</h3>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li><Link to="/home">home</Link></li>
                                <li>।</li>
                                <li>Client Transformation</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>


            <div class="contact-area pad90">
                <div class="container-fluid-2">
                    <div class="row mb-2">
                        <ClientPagination pageSize={16} pageCount={3} currentPage={0} data={client_data} />
                    </div>
                </div>
            </div>

            <div class="portfolio-area title-white bg1 parallax  pad90 video-review">
                <div class="video-set">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                    <h3 class="rockstarts">Hear directly from my rockstars</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <Slider {...settings_1}>
                {
                    youtube_link.map((e, i) => (
                        <div class='testimonial-item' key={i}>
                            <iframe src={e.url} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                        </div>
                    ))
                }
            </Slider>

            <div class="testimonial-down">
            <Slider {...settings}>
                {
                    slider_img.map((e, i) => (
                        <div key={i}>
                            <img src={e.img} alt="" class='img img-fluid' />
                        </div>
                    ))
                }
            </Slider>
            </div>

            {/* <Slider
                dots={false} infinite={true} slidesToScroll={1} speed={500} autoplay={true} autoplaySpeed={3000}
                slidesToShow={4} pauseOnHover={true}
                prevArrow={<div><i className="lni lni-chevron-left-circle text-dark"></i></div>}
                nextArrow={<div><i className="lni lni-chevron-right-circle text-dark"></i></div>}
            >
                {
                    slider_img.map(e => (
                        <div className='testimonial-item' >
                            <img src={e.img} alt="Transformation img" height="400" />
                        </div>
                    ))
                }
            </Slider> */}


            <Footer />
        </>
    )

}