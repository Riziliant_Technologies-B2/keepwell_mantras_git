import React from 'react';
// import './style.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import { Link } from "react-router-dom";


export default function Testimonial({ slides, className }) {
    return (
        <>
            <Header2 />

            <div class="services3-bg page-head parallax overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <h3>PACKAGES</h3>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li><Link to="/">HOME</Link></li>
                                <li>।</li>
                                <li>PACKAGES</li>
                            </ol>
                        </div>

                    </div>

                </div>

            </div>


            <div class="pricing-area text-center pad90">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                </div>
                                <h3>Fitness class packages</h3>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="price-box with-first surahi bg_color1">
                                <div class="price-empty"><p>Live Classes packages - Follow the live class schedule</p>
                                </div>
                                <div class="price-quantity">

                                    <div class="price-dtl">
                                        <ul>
                                            <li class="first-child">6 weeks package</li>
                                            <li>£5 per session</li>
                                            <li>15% discount if you book min 15 sessions<br />
                                                20% discount if you book min 20 sessions
                                            </li>
                                            <li>Access to Live classes or recordings of live classes</li>
                                            <li>Flexibility to choose from <br />
                                                - HIITs (High intensity - for intermediate to advance) <br />
                                                - MIITs ( Medium intensity - for beginners to intermediate)<br />
                                                - Strength workouts ( with dumbbells)</li>
                                            <li>Access to private members community</li>
                                        </ul>
                                        <div class="price-btn bttn">
                                            <Link to="/login" class="btn active btn-primary">
                                                Buy now
											</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="price-box with-first surahi bg_color2">
                                <div class="price-empty"><p>Workout Videos - Create your own package!</p>
                                </div>
                                <div class="price-quantity">

                                    <div class="price-dtl">
                                        <ul>
                                            <li class="first-child">6 weeks package</li>
                                            <li>£5 per session</li>
                                            <li>20% discount if you book min 10 sessions</li>
                                            <li>Access to only recorded videos</li>
                                            <li>Flexibility to choose from <br />
                                                - HIITs (High intensity - for intermediate to advance) <br />
                                                - MIITs ( Medium intensity - for beginners to intermediate)<br />
                                                - Strength workouts ( with dumbbells)</li>
                                            <li>Access to private members community</li>
                                        </ul>
                                        <div class="price-btn bttn">
                                            <Link to="/login" class="btn active btn-primary">
                                                Buy now
											</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="price-box with-first surahi bg_colo3">
                                <div class="price-empty"><p>Combo Live and Workout Videos</p>
                                </div>
                                <div class="price-quantity">

                                    <div class="price-dtl">
                                        <ul>
                                            <li class="first-child">6 weeks package</li>
                                            <li>£5 per session</li>
                                            <li>20% discount if you book min 20 sessions</li>
                                            <li>Access to both live classes &amp; recorded videos</li>
                                            <li>Flexibility to choose from <br />
                                                - HIITs (High intensity - for intermediate to advance)<br />
                                                - MIITs ( Medium intensity - for beginners to intermediate)<br />
                                                - Strength workouts ( with dumbbells)</li>
                                            <li>Access to private members community</li>
                                        </ul>
                                        <div class="price-btn bttn">
                                            <Link to="/login" class="btn active btn-primary">
                                                Buy now
											</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                </div>
                                <h3>Nutrition &amp; Fitness class packages</h3>
                            </div>
                        </div>

                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="price-box with-first surahi bg_color4">
                                    <div class="price-empty"><p>Nutrition only support package</p>
                                    </div>
                                    <div class="price-quantity">

                                        <div class="price-dtl">
                                            <ul>
                                                <li class="first-child">£100</li>
                                                <li>6 weeks package</li>
                                                <li>Bi - weekly check ins</li>
                                                <li>Nutrition and lifestyle enhancements customized to your fitness goals. Option to choose from a set meal plan ideas or calorie counting diet approach will be discussed during the initial consulation call.</li>
                                                <li>Can be upgraded to include workouts</li>
                                            </ul>
                                            <div class="price-btn bttn">
                                                <Link to="/login" class="btn active btn-primary">
                                                    Buy now
											</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="price-box with-first surahi bg_color5">
                                    <div class="price-empty"><p>Fitness &amp; Nutrition Combo package</p>
                                    </div>
                                    <div class="price-quantity">

                                        <div class="price-dtl">
                                            <ul>
                                                <li class="first-child"><span>£200</span> £160 only! (40% off)</li>
                                                <li>6 weeks package</li>
                                                <li>Customised Fitness plan with access to any 20 sessions</li>
                                                <li>Customised nutrition support plan for 6 weeks</li>
                                                <li>Flexibility to choose from<br />
                                                    - HIITs (High intensity - for intermediate to advance)<br />
                                                    - MIITs ( Medium intensity - for beginners to intermediate)<br />
                                                    - Strength workouts ( with dumbbells)</li>
                                                <li>Access to private members community</li>
                                            </ul>
                                            <div class="price-btn bttn">
                                                <Link to="/login" class="btn active btn-primary">
                                                    Buy now
											</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="price-box with-first surahi bg_color6">
                                    <div class="price-empty"><p>Personal Training &amp; Nutrition support</p>
                                    </div>
                                    <div class="price-quantity">

                                        <div class="price-dtl">
                                            <ul>
                                                <li class="first-child">Bespoke pricing - please contact to discuss</li>
                                                <li>6 weeks package</li>
                                                <li>Customised Fitness plan with 1:1 sessions </li>
                                                <li>Customised nutrition support plan for 6 weeks (optional)</li>
                                                <li>Flexibility to choose from and include group sessions (optional) <br />
                                                    - HIITs (High intensity - for intermediate to advance) <br />
                                                    - MIITs ( Medium intensity - for beginners to intermediate)<br />
                                                    - Strength workouts ( with dumbbells)</li>
                                                <li>Access to private members community</li>
                                            </ul>
                                            <div class="price-btn bttn">
                                                <Link to="/login" class="btn active btn-primary">
                                                    Buy now
											</Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

            </div>


            <Footer />
        </>
    )

}