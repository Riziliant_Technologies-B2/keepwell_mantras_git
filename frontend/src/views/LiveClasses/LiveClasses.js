import React, { useEffect, useState } from "react";
import {
	Container, Badge, Button, Card, CardTitle, CardText, CardSubtitle, CardBody,
	Popover, CardHeader, Modal, ModalBody, ModalFooter, ListGroup, ListGroupItem, ButtonGroup
} from "reactstrap";
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';

import _fetch from '../../_fetch';
import Header from "../../components/Headers/Header.js";
import './styles.scss';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import Loader from "../../components/Loader/loader";
import { getEnrollments } from "../../actions/userActions";
import Reviews from "../Reviews";

const localizer = momentLocalizer(moment);

export default function LiveClasses() {

	const [sessions, setSessions] = useState([]);
	const [selectedEvent, setSelectedEvent] = useState(null);
	const [selectedClass, setSelectedClass] = useState(false);
	const [live, setLive] = useState(false);
	const [loading, setLoading] = useState(false);
	const [live_classes_count, setLiveClassesCount] = useState(0);
	const [available__recorded_live_classes, setAvailableRecordedLiveClasses] = useState([]);
	const [view, setView] = useState('CALENDAR');

	const { total_live_classes, watched_live_classes, points, live_enrollments } = useSelector(st => st.user);
	let history = useHistory();
	let dispatch = useDispatch();

	useEffect(() => {

		if (!isNaN(total_live_classes) && !isNaN(watched_live_classes))
			setLiveClassesCount(total_live_classes - watched_live_classes)
	}, [total_live_classes, watched_live_classes])

	async function init() {
		try {
			setLoading(true);
			let res = await _fetch(`${process.env.REACT_APP_API}/classes?type=LIVE`);
			if (res.success) {
				console.log(res.response);
				let classes = [], top = []
				res.response.forEach(e => {
					let bool = moment(e.date).isBetween(moment().subtract(1, 'weeks'), moment().add(1, 'weeks'));
					let _top = moment().format('l') === moment(e.date).format('l');
					if (_top)
						top.push(e);
					else if (bool && !_top)
						classes.unshift(e);
					else if (!bool && !_top)
						classes.push(e);
				})

				setSessions([...top, ...classes]);
			}
			await getEnrollments(dispatch);
		} catch (err) {
			console.log(err);
		} finally {
			setLoading(false);
		}
	}

	useEffect(() => {
		const l = live_enrollments;
		if (Array.isArray(l)) {
			let arr = [];
			l.forEach(e => {
				e.classes.forEach(c => {
					if (c.type === 'RECORDED_LIVE' && moment().isBefore(moment(c.watch_start).add(1, 'd'))) {
						arr.push(c._id)
					}
				})
			});
			setAvailableRecordedLiveClasses(arr);
		}
	}, [live_enrollments])

	useEffect(() => {
		init();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	function recordedClass(id) {
		let w = window.open();

		_fetch(`${process.env.REACT_APP_API}/classes/recording_url?class_id=${selectedClass || id}`)
			.then(res => {
				if (res.success) {
					if(res.response.class_recording == null){
						return;
					}
					if (res.response.type === 'LIVE') {
						setSelectedClass('');
						setSelectedEvent(null);
						w.location = res.response.class_recording;
						init();
						return;
					}
				} else {
					toast.error(res.response);
				}
			})
			.catch(err => {
				console.log(err);
				toast.error(err.message);
			})

		w.addEventListener('error', (ev) => {
			console.log(ev.message);
			toast.error(ev.message);
		});

	}

	function liveClass() {
		let w = window.open();
		_fetch(`${process.env.REACT_APP_API}/classes/meeting_link?class_id=${selectedClass}`)
			.then(res => {
				if (res.success) {
					if (res.response) {
						setSelectedClass('');
						setSelectedEvent(null);
						w.location = res.response;
						init();
					} else {
						toast.error('some error occurred please try again');
						init();
					}
				} else {
					toast.error(res.response);
				}
			})
			.catch(err => {
				console.log(err);
				toast.error(err.message);
			})
	}

	if (loading) return (
		<>
			<Header />

			<Container className="mt--7" fluid>
				<Card className="bg-secondary shadow" style={{ height: 400 }}>
					<Loader />
				</Card>
			</Container>
		</>
	);

	return (
		<>
			<Header />

			{/* Page content */}
			<Container className="mt--7" fluid>
				<Card className="bg-secondary shadow">

					<CardHeader className="bg-white border-0">
						<div className='d-flex flex-column flex-md-row justify-content-between'>
							<div>
								<div className='text-lg font-weight-600'>Available Live Classes: {live_classes_count ? live_classes_count : 0}</div>
								<div> Buy your workouts for 6 weeks </div>
							</div>
							<Button color='primary' className='ml-0 ml-md-3 mr-auto mr-md-0 mt-2 mt-md-0 mb-auto live-class-checkout' onClick={() => history.push('/buy-classes')}>
								Buy Live Classes
							</Button>
							<div className='d-block d-md-none'>
								<ButtonGroup className='mt-3'>
									<Button onClick={()=>setView('LIST')}><i class="lni lni-list text-lg"></i></Button>
									<Button onClick={()=>setView('CALENDAR')}><i class="lni lni-calendar text-lg"></i></Button>
								</ButtonGroup>
							</div>
						</div>
					</CardHeader>

					<CardBody className={view === 'CALENDAR' ? 'd-block' : 'd-none'}>
						<div className='live-classes-container live-calendar'>
							<Calendar
								localizer={localizer}
								events={
									sessions.map(e => ({
										title: e.title,
										start: moment(e.date).toDate(),
										end: moment(e.date).add(e.duration_in_minutes, 'minutes').toDate(),
										allDay: false,
										description: e.description,
										_id: e._id,
										id: `a${e._id}`,
										category: e.category,
										recording_link: e.recording_link
									}))
								}
								startAccessor="start"
								endAccessor="end"
								titleAccessor={(event) => (
									<div className='d-flex flex-row align-items-center' id={event.id}>
										<span className='ml-2'>{event.title}</span>
									</div>
								)}
								views={['month', 'week', 'day']}
								tooltipAccessor={null}
								onSelectEvent={(e) => setSelectedEvent(e)}
							/>
							{
								selectedEvent &&
								<Popover isOpen={true} target={selectedEvent?.id} trigger="legacy" toggle={() => setSelectedEvent(null)}>
									<Card style={{ width: 300 }}>
										<CardBody>
											<CardTitle tag="h5" style={{ fontSize: 18 }}>
												{selectedEvent?.title}
												<div>
													<Badge color="info" style={{ textTransform: 'capitalize' }}>{selectedEvent?.category.name}</Badge>
												</div>
											</CardTitle>
											<CardSubtitle tag="h6" className="mb-2 text-muted" style={{ fontSize: 14 }}>
												{moment(selectedEvent?.start).format('LT')} - {moment(selectedEvent?.end).format('LT')}
											</CardSubtitle>
											<CardText className='text-xs'>{selectedEvent?.description}</CardText>
											<div className='my-3'>
												<h5>Review this class on:</h5>
												<Reviews size='xs' />
											</div>
											<div className='d-flex justify-content-center'>
												<Button
													type='button' color='primary'
													disabled={moment().isBefore(selectedEvent.end) || !selectedEvent.recording_link} size='small'
													className='mr-2'
													onClick={() => {
														if (live_classes_count || points) {
															setLive(false);
															if (!available__recorded_live_classes.includes(selectedEvent?._id)) {
																setSelectedClass(selectedEvent?._id);
																return setSelectedEvent(null);
															}
															recordedClass(selectedEvent?._id);
														}
														else {
															toast.info('Buy more slots to watch this video');
															history.push('/cart');
														}
													}}
												>
													Watch Now
												</Button>
												<Button
													type='button' color='primary'
													onClick={() => {
														if (live_classes_count) {
															setLive(true);
															setSelectedClass(selectedEvent?._id);
															setSelectedEvent(null);
														}
														else {
															toast.info('Buy more slots to watch this video');
															history.push('/buy-classes');
														}
													}}
													disabled={!moment().isBetween(moment(selectedEvent?.start).subtract(30, 'minutes'), selectedEvent.end)} size='small'
													className='ml-2'
												>
													Join Live
												</Button>
											</div>
										</CardBody>
									</Card>
								</Popover>
							}
						</div>
					</CardBody>

					<ListGroup className={view === 'CALENDAR' ? 'd-none' : 'd-block'}>

						{
							sessions.map(e => (
								<ListGroupItem className="d-flex flex-column justify-content-between" key={e._id}>
									<h5 className='text-lg'>{e.title}</h5>
									<h6 className='text-muted text-sm my-2'>
										{moment(e?.date).format('ll')} {moment(e?.date).format('LT')} - {moment(e.date).add(e.duration_in_minutes, 'minutes').format('LT')}
									</h6>
									<div>
										<Button
											type='button' color='primary'
											disabled={moment().isBefore(moment(e.date).add(e.duration_in_minutes, 'minutes')) || !e.recording_link} size='sm'
											className='mr-2 my-1'
											onClick={() => {
												if (live_classes_count) {
													setLive(false);
													if (!available__recorded_live_classes.includes(e?._id)) {
														setSelectedClass(e?._id);
														return setSelectedEvent(null);
													}
													recordedClass(e?._id);
												}
												else {
													toast.info('Buy more slots to watch this video');
													history.push('/cart');
												}
											}}
										>
											Watch Now
										</Button>
										<Button
											type='button' color='primary'
											onClick={() => {
												if (live_classes_count) {
													setLive(true);
													setSelectedClass(e?._id);
													setSelectedEvent(null);
												}
												else {
													toast.info('Buy more slots to watch this video');
													history.push('/buy-classes');
												}
											}}
											disabled={!moment().isBetween(moment(e?.date).subtract(30, 'minutes'), moment(e.date).add(e.duration_in_minutes, 'minutes'))}
											size='sm'
											className='mr-auto my-1'
										>
											Join Live
										</Button>
									</div>
								</ListGroupItem>
							))
						}
					</ListGroup>

				</Card>
				<Modal isOpen={!!selectedClass} toggle={() => setSelectedClass('')}>
					<ModalBody>
						<p className='font-weight-500'>
							{
								live_classes_count ?
									<React.Fragment>
										Watching this recording will reduce 1 live class from your purchased live classes.<br />
										Do you want to continue ?
									</React.Fragment>
									:
									points ?
										<React.Fragment>
											You do not have any live classes left, Watching this recording will reduce 50 points from points you have earned by referrals.<br />
											Do you want to continue ?
										</React.Fragment>
										:
										'Please purchase more live classes'
							}
						</p>
					</ModalBody>
					<ModalFooter>
						<Button color='danger' onClick={() => setSelectedClass('')}>No</Button>
						<Button color='primary' onClick={() => live ? liveClass() : recordedClass()}>Yes</Button>
					</ModalFooter>
				</Modal>
			</Container>
		</>
	);
};
