import React from 'react';
import './styles.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import { Link } from 'react-router-dom';

export default function FitnessMakeover() {

    return (
        <>
            <Header2 />

            <header className="fitness-makeover-header">
                <h1 className="fitness-makeover-title">Fitness Challenges</h1>
            </header>

            <main className="main">
                <div className="content-wrapper">

                    <div className="fitness-makeover-intro container">
                        <h2 className="fitness-makeover-intro-title">FITNESS SESSION SCHEDULE</h2>

                        <div className='w-100' style={{ height: 1, borderTop: '1px solid #8B5728' }}></div>

                        <h3 style={{ color: '#8b5728', fontSize: 35 }} className='my-4'>Core Sculpt - 6 Weeks Fitness Challenge </h3>

                        <div className='w-100 mb-5' style={{ height: 1, borderTop: '1px solid #8B5728' }}></div>

                        <div className='row no-gutters w-100 mt-4'>
                            <div className='col-12 col-md-6 p-3'>

                                <h4 style={{ fontSize: 25 }} className='text-muted'>WHAT TO EXPECT</h4>

                                <ul className="makeover-checklist-menu mb-4">
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>
                                            ACCESS TO UNIQUE WORKOUTS LIVE AND RECORDING. ( MEDIUM &amp; HIGH INTENSITY)
                                        </span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>EXTRA FITNESS TASKS AND NUTRITION TIPS WEEKLY</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>ACCESS TO MY PRIVATE CHALLENGE GROUPS</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>PROGRESS TRACKING AND ACCOUNTABILITY</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>MEAL IDEAS / RECIPE SHARING</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>GUIDANCE AND MENTORING</span>
                                    </li>
                                </ul>

                                <Accordion allowMultipleExpanded={true} allowZeroExpanded={true}>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                When do we start?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                You can join in anytime and commit for your 6 weeks of fitness challenge!
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                What's the process?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                2 step process:<br />
                                                – Take a trial session from this page best suited to your fitness abilities. You can choose to try a High intensity session (suited to intermediate to advance fitness), a Strength training session with dumbbells (suited to all abilities) and a Medium intensity session (suited to beginners to intermediate fitness). Scroll below to try the videos<br />
                                                – Register on my website an book your package on your personal dashboard Fitness Stars<br />
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Is it suitable for beginners?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                Yes!<br />
                                                I do 10 live sessions in a week. 4 high intensity, 4 medium intensity and 2 weight training sessions. Medium intensity and weight trainings are suitable for beginners
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                What's more for the challenge members?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                Access to my members only WhatsApp &amp; Facebook group<br />
                                                Access to my live &amp; recorded sessions<br />
                                                Personal goal tracking and monitoring<br />
                                                Nutrition guidance<br />
                                                Extra fitness &amp; nutrition tasks weekly<br />
                                                My fitness community support<br />
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Do you do customised diet support?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                Yes, I offer customised diet support consultation package for 6 weeks. I am offering 20% discount on my diet support if you sign up for minimum 25 sessions for 6 weeks.
                                            </p>
                                            <p>
                                                Diet support includes<br />
                                                – Forms required to be filled up to include your your food diary, any intolerance, food choices and your existing lifestyle<br />
                                                – I customise the diet for yourself and will do a consultation call to go over it<br />
                                                – I will offer sample meal plans best suited<br />
                                                – I will support you throughout the 6 week process<br />
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Do I need any equipment?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                My sessions are no equipment body weight circuits. You just need a soft floor or exercise mat. Only sessions on Wed and Sun mornings require dumbbells if you wish to do weight training.
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                How to attend these sessions?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                For all Live Classes &amp; Recordings Access go to your Live Class calendar on your Fitness Stars personal dashboard. You can either join Live classes by clicking on 'Join LIVE' at the scheduled time or you can access the recording of the live class by clicking on 'Watch Now' button. The classes are catered to all fitness levels from beginners to advance.
                                            </p>
                                            <p>
                                                You can even create your own package by choosing Workout Videos if you are looking for more targeted focus area like Lower body or Abs or looking to do more Strength training sessions throughout the week. My live classes offer a generic schedule for all levels but if you are looking at a body part more than the other, you can create a package by blending in workout videos with live classes. If you need more support in this, please get in touch with me.
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Which sessions should I target?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                Depending on your fitness level:
                                            </p>
                                            <p>
                                                1. High intensity sessions in mornings : These are best suited to people who have been into fitness for a while. It is high paced and few exercises can be intense for a beginner. However, I always give less intense variations of all exercises in the session.
                                            </p>
                                            <p>
                                                2. Medium Intensity sessions in evenings : This are perfect for beginners to intermediate fitness level. The focus will be full body exercises
                                            </p>
                                            <p>
                                                3. Weight training sessions on Wed &amp; Sun mornings : This are suitable for all levels (beginners can use lighter weights)
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                How much does the 6 weeks challenge cost?
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <p>
                                                There is no charge to participate in the 6 week challenge, all you have to pay is per session charge £5. The sessions can be accessed either live or through recorded links. You have freedom to choose any of the High intensity, Medium intensity and Weight training sessions within your booked block. If you finish your block early, you can buy more at the same price for £5 per additional session.
                                            </p>
                                            <p>
                                                If you wish to also add diet support plan, I am offering discount with min 25 sessions purchased. Get in touch to find more information.
                                            </p>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                </Accordion>

                                <h6 style={{ fontSize: 18 }} className="mt-4 mb-3">Anything else - Please feel free to ask!</h6>
                                <Link className='about-cta' to='/register'>Buy Now</Link>

                            </div>
                            <div className='col-12 col-md-6'>
                                <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/fitness.jpg`} alt='' className='img img-fluid' />
                            </div>
                        </div>


                        <div className='w-100 mb-4' style={{ height: 1, borderTop: '1px solid #8B5728', marginTop: 60 }}></div>

                        <h3 style={{ color: '#8b5728', fontSize: 35 }} className='mb-4'>HIITs - High intensity interval training</h3>
                        <h4 style={{ fontSize: 20 }} className='text-muted mb-4'>Suitable for intermediate and advance level</h4>

                        <div className='w-100 mb-5' style={{ height: 1, borderTop: '1px solid #8B5728' }}></div>

                        <div className='row w-100 mt-4'>
                            <div className='col-12 col-md-6 order-1 order-md-0 py-3'>
                                <ul className="makeover-checklist-menu mb-4">
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>
                                            45 - 50 min live workouts
                                        </span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Recordings available throughout the week</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Six days a week</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Every day has a different focus ( Stamina / Strength training / Lower body / Full body / Abs / Upper body)</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Plenty of new exercises and techniques to train each and every muscle</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Variations - my USP - no 2 session are ever the same</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Modification of all exercises to suit your fitness level</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>No equipment needed (except strength training day)</span>
                                    </li>
                                </ul>

                                <Link className='about-cta' to='/register'>Buy Now</Link>

                            </div>

                            <div className='col-12 col-md-6 order-0 order-md-1 py-3 mt-4 mt-md-0'>
                                <h4 style={{ fontSize: 25 }} className='text-muted'>HIGH INTENSITY SCHEDULE (45 - 50 MIN)</h4>
                                <p className='mb-1'>TUESDAY – 6:30 AM – RESISTANCE TRAINING</p>
                                <p className='mb-1'>WEDNESDAY – 6:30 AM – LEGS / LOWER BODY</p>
                                <p className='mb-1'>THURSDAY – 6:30 AM – ABS</p>
                                <p className='mb-1'>FRIDAY – 8:AM – FULL BODY</p>
                                <p className='mb-1'>SATURDAY – 8:AM – UPPER BODY</p>
                                <p className='mb-1'>SUNDAY – 8:AM – UPPER BODY</p>
                            </div>
                        </div>

                        <div className='row mt-5'>
                            <div className='col-12 col-md-5 offset-0 offset-md-1 text-center mt-4'>
                                <h4 className='text-center' style={{ fontSize: 20, color: '#8b5728' }}>FULL BODY HIGH INTENSITY WORKOUT</h4>
                                <iframe width="400" height="267" src="https://www.youtube.com/embed/_S6lzO_aZ-o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div className='col-12 col-md-6 text-center mt-4'>
                                <h4 className='text-center' style={{ fontSize: 20, color: '#8b5728' }}>STRENGTH TRAINING WITH DUMBBELLS WORKOUT</h4>
                                <iframe width="400" height="267" src="https://www.youtube.com/embed/FHtGIgVV-3E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>

                        <div className='w-100 mt-5 mb-4' style={{ height: 1, borderTop: '1px solid #8B5728' }}></div>

                        <h3 style={{ color: '#8b5728', fontSize: 35 }} className='mb-4'>MIITs - Medium intensity interval training</h3>
                        <h4 style={{ fontSize: 20 }} className='text-muted mb-4'>Suitable for intermediate and advance level</h4>

                        <div className='w-100 mb-5' style={{ height: 1, borderTop: '1px solid #8B5728' }}></div>

                        <div className='row w-100 mt-4'>
                            <div className='col-12 col-md-6 py-3'>
                                <ul className="makeover-checklist-menu mb-4">
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>
                                            35 - 40 min live workouts
                                        </span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Recordings available throughout the week</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>4 days a week</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Full body circuits</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Plenty of new exercises and techniques to train each and every muscle</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Variations - my USP - no 2 session are ever the same</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Modification of all exercises to suit your fitness level</span>
                                    </li>
                                    <li className="challenge-checklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>No equipment needed (except strength training day)</span>
                                    </li>
                                </ul>

                                <Link className='about-cta' to='/register'>Buy Now</Link>

                            </div>
                            <div className='col-12 col-md-6'>
                                <h4 style={{ fontSize: 25 }} className='text-muted'>MEDIUM INTENSITY FULL BODY SCHEDULE (40 - 45 MIN)</h4>
                                <p className='mb-1'>TUESDAY – 6:15 PM – MEDIUM INTENSITY FULL BODY</p>
                                <p className='mb-1'>WEDNESDAY – 6:15 PM – MEDIUM INTENSITY FULL BODY</p>
                                <p className='mb-1'>THURSDAY – 6:15 PM – MEDIUM INTENSITY FULL BODY</p>
                                <p className='mb-1'>FRIDAY – 6:15 PM – MEDIUM INTENSITY FULL BODY</p>
                            </div>
                        </div>

                        <div className='mt-5 w-100'>
                            <h4 style={{ fontSize: 20, color: '#8b5728' }}>THIS MEDIUM INTENSITY WORKOUT (MIIT)</h4>
                            <iframe width="393" height="262" src="https://www.youtube.com/embed/ZYbJIaKapL4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>

                </div>
            </main>

            <Footer />
        </>
    );
}