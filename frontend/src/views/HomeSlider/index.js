import React from 'react';
import Slider from "react-slick";
// import './style.css';
import { slide_data } from './slide_data';
import { Container, Row, Col, Card, CardBody } from 'reactstrap';
import { Link } from "react-router-dom";

export default function Testimonial({ slides, className }) {

    return (
        <>
            <div class="custom_slid">
                <Slider
                    dots={false} infinite={true} slidesToScroll={1} speed={800} autoplay={true} autoplaySpeed={6000}
                    slidesToShow={slides} pauseOnHover={false} className={className}
                    prevArrow={<div><i className="lni lni-chevron-left"></i></div>}
                    nextArrow={<div><i className="lni lni-chevron-right"></i></div>}
                >
                    {
                        slide_data.map((e, i) => (

                            <div key={e.name}>
                                <div className="text-box p-5">
                                    <h1 style={{ fontSize: "20px" }}>{e.content_1}</h1>
                                    <h3 style={{ fontSize: "20px" }} className="b-700">
                                        {e.content_2}
                                    </h3>
                                </div>
                                {(i == 0) ? (
                                    <Link to="/login">
                                        <img src={e.banner} alt="" className='img img-fluid' />
                                    </Link>) : (
                                        <img src={e.banner} alt="" className='img img-fluid' />
                                    )}
                            </div>
                        ))
                    }
                </Slider>
            </div>

            <Container className="top-sec">
                <Row>
                    <Col lg="6" sm="6">
                        <Card className="mb-3 top-sec-card">
                            <Row className="no-gutters">
                                <Col md="4">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/1.jpg`} class="card-img" alt="..." />
                                </Col>
                                <Col md="8">
                                    <CardBody>
                                        <h5 class="card-title">Personal Training</h5>
                                        <p class="card-text">Your fitness goals are unique just like you and I would love to train you as per your fitness needs.</p>
                                        <p class="card-text">Drop me an enquiry for a training session <span><Link to="/contact">here</Link></span></p>
                                    </CardBody>
                                </Col>
                            </Row>
                        </Card>
                    </Col>

                    <Col lg="6" sm="6">
                        <Card className="mb-3 top-sec-card">
                            <Row className="no-gutters">
                                <Col md="4">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/2.jpg`} class="card-img" alt="..." />
                                </Col>
                                <Col md="8">
                                    <CardBody>
                                        <h5 class="card-title">Online Fitness Sessions</h5>
                                        <p class="card-text">Wish to do live sessions with me? Enquire and book your first class.
                                    <span><Link to="/classes">  Click here  </Link></span>
                                            to join my current Fitness Challenge
                                    </p>

                                    </CardBody>
                                </Col>
                            </Row>
                        </Card>
                    </Col>

                    <Col lg="6" sm="6">
                        <Card className="mb-3 top-sec-card">
                            <Row className="no-gutters">
                                <Col md="4">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/3.jpg`} class="card-img" alt="..." />
                                </Col>
                                <Col md="8">
                                    <CardBody>
                                        <h5 class="card-title">Nutrition</h5>
                                        <p class="card-text">Nutrition helps you reach your goals and keeps you there! Wish to be more confident with your macros and micro nutrients ? <Link to="/contact">Get in touch</Link> for a customized 1 to 1 Nutrition plan.
                                    </p>
                                    </CardBody>
                                </Col>
                            </Row>
                        </Card>
                    </Col>

                    <Col lg="6" sm="6">
                        <Card className="mb-3 top-sec-card">
                            <Row className="no-gutters">
                                <Col md="4">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/4.jpg`} class="card-img" alt="..." />
                                </Col>
                                <Col md="8">
                                    <CardBody>
                                        <h5 class="card-title">Confidential consultation</h5>
                                        <p class="card-text">Don’t know where to start ? I am just a call away. Book a consultation session straight away. Feel free to contact me in confidence via this
                                    <span><Link to="/contact"> form </Link></span>
                                        </p>
                                    </CardBody>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Container>

        </>
    )

}