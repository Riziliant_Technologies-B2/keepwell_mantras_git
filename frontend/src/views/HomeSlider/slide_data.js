export const slide_data = [
    {
        "name": "img_1",
        "content_1": "",
        "content_2": "",
        "banner": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/banner_diwali.png`
    },
    {
        "name": "img_4",
        "content_1": "Prapti Dutt",
        "content_2": "Madness is a must!",
        "banner": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/s-1.jpg`
    },
    {
        "name": "img_2",
        "content_1": "goal digger",
        "content_2": "fitness is not a destination it is a way of life",
        "banner": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/s-2.jpg`
    },
    {
        "name": "img_3",
        "content_1": "befit Strong",
        "content_2": "it never gets easier. You just get stronger",
        "banner": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/s-3.jpg`
    }
];