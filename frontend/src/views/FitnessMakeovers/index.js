import React from 'react';
import './styles.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';

export default function FitnessMakeover() {

    return (
        <>
            <Header2 />

            <header className="fitness-makeover-header">
                <h1 className="fitness-makeover-title">Fitness Makeover</h1>
            </header>

            <main className="main">
                <div className="content-wrapper">

                    <div className="fitness-makeover-intro">
                        <h2 className="fitness-makeover-intro-title">My Bespoke Service</h2>

                        <em className="fitness-makeover-desc">There is no one size fits all approach in my services. I offer a variety of options for us to pick and choose what is best suited to your goals and lifestyle</em>

                        <div className="makeover-checklist">
                            <div className="makeover-checklist-left">
                                <ul className="makeover-checklist-menu">
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Personal Training</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Nutrition &amp; Diet</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Sports Training</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Couple Training</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Corporate Sessions</span>
                                    </li>
                                </ul>
                            </div>
                            <div className="makeover-checklist-right">
                                <ul className="makeover-checklist-menu">
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Weight Loss Plans</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Strength Building</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Stamina Building</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Post Pregnancy Weight Loss</span>
                                    </li>
                                    <li className="makeoverchecklist-item">
                                        <i className="lni lni-checkmark"></i>
                                        <span>Online Workouts</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="makeover-blocks">
                        <div className="block">
                            <h2 className="block-title">Live Workouts</h2>
                            <div className="row align-items-center">
                                <div className="col-12 col-md-6 col-xl-4 offset-xl-2 my-2">
                                    <p className='h1 font-weight-300'>
                                        Be part of my fitness family and do live workouts with me ! If you struggle to stay motivated and lack discipline to your workout routine – this is just right for you!
                                    </p>
                                </div>

                                <div className="col-12 col-md-6 col-xl-4 my-2">
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Follow along my bodyweight workouts of high, medium and low intensity 6 days a week suited to your fitness levels. Duration varies from 30min to 45min. All you need to do it to join via zoom and just follow along.</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>If you cannot make it to the scheduled time – recordings of all my sessions are available for you to do at your own pace.</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Every day has a different focus and no two sessions are ever the same. Lots of new exercises added every day bringing in variations you can never imagine!</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>No age restriction as there is a less intense variation to each exercise. For any medical condition please do make me aware beforehand</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Online community of my motivated fitness tribe keeping the team spirits high! Plenty of bonus challenges , extra workouts , meal ideas and recipe sharing</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Flexibility to choose from monthly rolling packages or per session booking</p>
                                    </div>
                                </div>
                                <div className='col-0 col-xl-2'></div>
                            </div>

                            <div className="block-cta">
                                <i className="lni lni-whatsapp"></i>
                                <p>Click the button below to connect with me</p>
                                <a href='https://api.whatsapp.com/send/?phone=%2B447584236848&text&app_absent=0' >Click Here</a>
                            </div>
                        </div>

                        <div className="block">
                            <h2 className="block-title">Personalized Training</h2>

                            <div className="row align-items-center">
                                <div className="col-12 col-md-6 col-xl-4 offset-xl-2 my-2">
                                    <p className='h1 font-weight-300'>
                                        Online or face to face, I will design bespoke plans as per your fitness goals.
                                    </p>
                                </div>

                                <div className="col-12 col-md-6 col-xl-4 my-2">
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Personalized workout plans just perfect to suit on your fitness level and goals</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Weekly workout schedules with regular check ins to progress your training</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Nutrition guidance and diet plans to support your fitness transformation</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>My coaching throughout the process to empower you with the knowledge and understanding of staying in your healthiest self throughout your lifetime</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>6 weeks and 12 weeks rolling plans available</p>
                                    </div>
                                </div>
                                <div className='col-0 col-xl-2'></div>
                            </div>

                            <div className="block-cta">
                                <i className="lni lni-whatsapp"></i>
                                <p>Click the button below to connect with me</p>
                                <a href='https://api.whatsapp.com/send/?phone=%2B447584236848&text&app_absent=0' >Click Here</a>
                            </div>
                        </div>

                        <div className="block">
                            <h2 className="block-title">Nutrition Plans</h2>


                            <div className="row align-items-center">
                                <div className="col-12 col-md-6 col-xl-4 offset-xl-2 my-2">
                                    <p className='h1 font-weight-300'>
                                        Abs are indeed made in the kitchen! Your diet contributes a lot towards your current fitness and your future health.
                                    </p>
                                </div>

                                <div className="col-12 col-md-6 col-xl-4 my-2">
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Personalized nutrition plans as per your lifestyle and food choices</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>My diet plans will cater to any underlying medical condition you may have which will be discussed in detail beforehand</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>Close monitoring of food groups and macros required for your transformation</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>I do not recommend or advice on any diet supplements or meal replacement products. I encourage healthy and balanced eating habits</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>6 weeks or 12 weeks collaboration plans to choose from</p>
                                    </div>
                                </div>
                                <div className='col-0 col-xl-2'></div>
                            </div>

                            <div className="block-cta">
                                <i className="lni lni-whatsapp"></i>
                                <p>Click the button below to connect with me</p>
                                <a href='https://api.whatsapp.com/send/?phone=%2B447584236848&text&app_absent=0' >Click Here</a>
                            </div>
                        </div>

                        <div className="block">
                            <h2 className="block-title">Confidential Consultations</h2>

                            <div className="row align-items-center">
                                <div className="col-12 col-md-6 col-xl-4 offset-xl-2 my-2">
                                    <p className='h1 font-weight-300'>
                                        If you are struggling to strike a balance with your health and work / home responsibilities, a confidential discussion can be a game changer
                                    </p>
                                </div>

                                <div className="col-12 col-md-6 col-xl-4 my-2">
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>A face to face / telephonic / zoom consultation can be booked to discuss about your struggles</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>A 60 min consultation can be booked where we will discuss your present and past medical information, fitness routine, nutrition, lifestyle, fitness goals and any health matter you wish to discuss.</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>I will guide you how with those little changes in your lifestyle, you can greatly improve your health. Being a mom I understand the struggles and can help you design an easy yet effective fitness plan which will be adaptable and sustainable</p>
                                    </div>
                                    <div className='d-flex mb-2'>
                                        <div className='pr-1 h1 font-weight-700 align-self-start' style={{ lineHeight: '27px' }}>·</div>
                                        <p>The discussion will stay confidential with an aim to help you enhance your understanding on healthy living, managing stress, increasing energy levels, mindful eating, sleep, hydration, hormonal changes, weight fluctuations, post pregnancy body changes and any wellness topic you wish to have my support in</p>
                                    </div>
                                </div>
                                <div className='col-0 col-xl-2'></div>
                            </div>

                            <div className="block-cta">
                                <i className="lni lni-whatsapp"></i>
                                <p>Click the button below to connect with me</p>
                                <a href='https://api.whatsapp.com/send/?phone=%2B447584236848&text&app_absent=0' >Click Here</a>
                            </div>

                        </div>
                    </div>
                </div>
            </main>

            <Footer />
        </>
    );
}