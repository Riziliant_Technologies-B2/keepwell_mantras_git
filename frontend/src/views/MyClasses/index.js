import React, { useEffect, useState } from "react";
import { Card, CardHeader, Table, Container, Button, Badge, Modal, ModalBody, ModalFooter } from "reactstrap";
import moment from "moment";
import { useHistory } from "react-router-dom";
import update from 'immutability-helper';

import Header from "../../components/Headers/Header.js";
import _fetch from '../../_fetch';
import Loader from "../../components/Loader/loader.js";
import { toast } from "react-toastify";

export default function MyClasses() {

    const [loading, setLoading] = useState(false);
    const [classes, setClasses] = useState([]);
    const [selectedClass, setSelectedClass] = useState(false);

    let history = useHistory();

    useEffect(() => {

        async function init() {
            try {
                setLoading(true);

                let res = await _fetch(`${process.env.REACT_APP_API}/classes/my_classes`);
                if (res.success) {
                    setClasses(res.response);
                }

            } catch (err) {
                console.log(err);
            } finally {
                setLoading(false);
            }
        }

        init();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loading) return (
        <>
            <Header />

            <Container className="mt--7" fluid>
                <Card className="bg-secondary shadow" style={{ height: 400 }}>
                    <Loader />
                </Card>
            </Container>
        </>
    );

    return (
        <>
            <Header />
            {/* Page content */}
            <Container className="mt--7" fluid>
                <Card className="shadow">

                    <CardHeader className="border-0">
                        <Button type='button' color='secondary' onClick={() => history.goBack()}>Back</Button>
                    </CardHeader>

                    <Table className="align-items-center table-flush" responsive>
                        <thead className="thead-light">
                            <tr>
                                <th scope='col'>Title</th>
                                <th scope='col'>Completion</th>
                                <th scope='col'>Expires</th>
                                <th scope='col' />
                            </tr>
                        </thead>
                        <tbody>
                            {
                                classes.map((c, i) => (
                                    <React.Fragment key={i}>
                                        <tr className='br-secondary'>
                                            <th
                                                colSpan={4}
                                                scope='colgroup'
                                                className='text-lg'
                                                onClick={() => setClasses(update(classes, { [i]: { expanded: { $set: !c.expanded } } }))}
                                            >
                                                <div className='w-100 d-flex justify-content-between'>
                                                    <span>{c.category.name}</span>
                                                    {
                                                        c.expanded ?
                                                            <i className="lni lni-chevron-up-circle ml-auto"></i>
                                                            :
                                                            <i className="lni lni-chevron-down-circle ml-auto"></i>
                                                    }
                                                </div>
                                            </th>
                                        </tr>
                                        {
                                            c.expanded ?
                                                c.classes.map(e => (
                                                    <tr>
                                                        <th scope="row" style={{ width: '50%' }}>
                                                            {e.title}
                                                        </th>
                                                        <td style={{ width: '15%' }}>
                                                            {
                                                                e.start_time ?
                                                                    moment().isAfter(moment(e.expires))
                                                                        ?
                                                                        <Badge color="" className="badge-dot">
                                                                            <i className="bg-success" />
                                                                            Completed
                                                                        </Badge>
                                                                        :
                                                                        <Badge color="" className="badge-dot mr-4">
                                                                            <i className="bg-info" />
                                                                            In progress
                                                                        </Badge>
                                                                    :
                                                                    <Badge color="" className="badge-dot mr-4">
                                                                        <i className="bg-danger" />
                                                                        Not Started
                                                                    </Badge>
                                                            }
                                                        </td>
                                                        <td style={{ width: '20%' }}>
                                                            {
                                                                moment(e.expires).format('llll')
                                                            }
                                                        </td>
                                                        <td style={{ width: '15%' }}>
                                                            <Button
                                                                type='button' color='success' className='mr-auto btn-sm'
                                                                onClick={() => {
                                                                    !e.start_time ?
                                                                        setSelectedClass(e._id)
                                                                        :
                                                                        moment().isBefore(moment(e.expires))
                                                                            ?
                                                                            history.push(`/watch/${e._id}`)
                                                                            :
                                                                            toast.error('You have already watched this class')
                                                                }}
                                                                disabled={moment().isAfter(moment(e.expires))}
                                                            >
                                                                Watch Now
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))
                                                :
                                                null
                                        }
                                    </React.Fragment>
                                ))
                            }
                        </tbody>
                    </Table>
                </Card>
                <Modal isOpen={!!selectedClass} toggle={() => setSelectedClass('')}>
                    <ModalBody>
                        <p className='font-weight-500'>
                            This class will be accessible only for next 24 hours once you start watching.<br />
                            <br />
                            Do you want to continue?
                        </p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color='danger' onClick={() => setSelectedClass('')}>No</Button>
                        <Button color='primary' onClick={() => history.push(`/watch/${selectedClass}`)}>Yes</Button>
                    </ModalFooter>
                </Modal>

            </Container>
        </>
    );
};
