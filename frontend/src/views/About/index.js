import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
// import './about.css';
import { Container, Row, Col } from 'reactstrap';
import { Link } from "react-router-dom";

// const imgs = [
//     <div className={`about-gallery-card`}>
//         <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/img1.jpeg`} alt="" />
//     </div>
//     ,
//     <div className={`about-gallery-card`}>
//         <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/img2.jpeg`} alt="" />
//     </div>
//     ,
//     <div className={`about-gallery-card`}>
//         <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/img3.jpeg`} alt="" />
//     </div>
//     ,
//     <div className={`about-gallery-card`}>
//         <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/img4.jpeg`} alt="" />
//     </div>
//     ,
//     <div className={`about-gallery-card`}>
//         <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/img5.jpeg`} alt="" />
//     </div>
// ];

export default function About() {

    return (
        <>
            <Header2 />

            <div class="about-bg page-head parallax overlay">
                <Container>
                    <Row>
                        <Col md="12">
                            <div class="section-title text-header">
                                <h3>about us</h3>
                            </div>
                        </Col>
                        <Col md="12">
                            <ol class="breadcrumb">
                                <li><Link to="/">HOME</Link></li>
                                <li>।</li>
                                <li>ABOUT US</li>
                            </ol>
                        </Col>
                    </Row>
                </Container>
            </div>

            <div class="about-area pad90 start-about">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-6 col-sm-12">
                            <div class="section-title text-left">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                </div>
                                <h3>MY JOURNEY TO KEEPWELL MANTRAS</h3>
                            </div>
                        </div>

                        <div class="col-lg-7 col-md-6 col-sm-12">
                            <div class="about-desc">
                                <p class="about-col-desc">I am also a mom to two beautiful boys and understand how challenging it is to find time for ourselves and yet be the best parent ever! My wellness approach is very adaptable and sustainable, I simply focus on a consistent approach towards fitness and healthy living.</p>
                                <p class="about-col-desc">Prior to me venturing into my most loved profession as a fitness trainer, I have worked for Forbes 500 MNCs for over 12 years. I am a CFA and post graduate with diploma in Insurance.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-6 col-sm-12">
                            <div class="about-opening">
                                <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-Dutt-final-about.jpg`} width="350" alt="About Img" />
                            </div>
                        </div>

                        <div class="col-lg-7 col-md-6 col-sm-12">
                            <div class="aboutus-box">
                                <div class="about-desc">
                                    <p class="more-about">More about me</p>

                                    <p class="about-col-desc">After my second son was born, I decided to pursue my passion and took a bold yet most rewarding decision of my life to dedicate all my efforts towards health and fitness of my community.</p>
                                    <p class="about-col-desc">Ever since, I have built a tribe of my clients, whom I lovingly call as my “Fitness Family”, they are my Fitness Stars. For them, I am not just a fitness trainer but a wellness coach and a mentor. I offer bespoke service in Fitness and Nutrition and leave no stone unturned to achieve my Fitfam’s fitness goals.</p>
                                    <p class="about-col-desc">Having lead global corporate wellness initiatives for over 5 years, I believe in leading my example. I empower my clients to lead their lives by their conscious efforts and with my guidance throughout the process. I work for you and with you in your fitness journey.</p>
                                    <h3 class="able">What it takes to transform</h3><br />
                                    <p class="about-col-desc">I have transformed my fitness twice after each kid and have lost of over 3 stones of body weight within an year post delivery. My 2 boys were born with Caesarean section and due to longer duration of bed rest, I was unable to resume my fitness level until after 3 months. There is a lot that changes in a women’s body post delivery and healing takes its time. Everyone’s recovery period is different but starting slow and progressing gradually is the best way to resume strength.</p>
                                    <p class="about-col-desc">Staying positive and emotionally strong will make your fitness journey fun and memorable. Losing weight in a healthy manner takes time and perseverance. I have coached many new mums and supported them in this fitness journey. Keeping the right balance of nutrition and fitness training should be your ultimate goal.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="trainers-area ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="trainer-pro">
                                <div class="trainer-img">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-second-1.jpg`} alt="trainer-img" width="255" height="250" />

                                </div>
                                <div class="trainer-bio">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trainer-pro">
                                <div class="trainer-img">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-second-2.jpg`} alt="trainer-img" width="255" height="250" />

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trainer-pro">
                                <div class="trainer-img">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-second-3.jpg`} alt="trainer-img" width="255" height="250" />

                                </div>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trainer-pro">
                                <div class="trainer-img">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-second-4.jpg`} alt="trainer-img" width="255" height="250" />

                                </div>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="trainer-pro">
                                <div class="trainer-img">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-second-5.jpg`} alt="trainer-img" width="255" height="250" />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="trainer-pro">
                                <div class="trainer-img">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-second-6.jpg`} alt="trainer-img" width="255" height="250" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="about-area pad90 last-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-left">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                </div>
                                <h3>Size 16 to size 0 journey!</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <div class="aboutus-box">
                                <div class="about-desc">
                                    <p class="about-col-desc">I wouldn’t have done this all by myself, loads of acts come in play together when you transform.
                                    I would love to share my journey to all those new mums or mums who haven’t gone back to their pre pregnancy fitness or even
those who want to get there once and for all ! And also to everyone who can resonate with me being a small business.</p>
                                    <p class="about-col-desc">Long post and so was my journey ! Apologies in advance.</p>
                                    <p class="about-col-desc">A CFA, Post grad in finance and Dip CII – yet I left my profession to pursue my real calling to be a fitness trainer!</p>
                                    <p class="about-col-desc">Jan 2019 – Blessed with my second boy with another C-section. It took 3 months to walk normal without the crouch and even 1 floor on stairs was a marathon.
By March 2019, I had gained another 10 kgs with, struggled with post partum depression and was just losing it. Starting weight 76 kgs.</p>
                                    <p class="about-col-desc">In April 2019, My mum left for India, and this gave me an opportunity to do my own meals and get movement in my life.
                                    Started with 20 min walks and doing 15 min stairs when my son napped ..I was exhausted and my legs used to cry but I didn’t stop! It took me 3 months to get better with meal planning.
                                    Slowly increased my exercises with 45 min walk everyday + core stretches and body weight circuits to get my core strength back.
I also started running but it instead exhausted me even more with my sleepless nights! Had to stop.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12">
                            <div class="about-opening">
                                <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Prapti-Dutt-final.jpg`} alt="About Img" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="about-col-desc">Finally by Aug 2019, I got confident with intensity,
used to do youtube videos but couldn’t get a discipline as got easily bored! Kept losing weight but had another 10 kgs to shift and that belly wasn’t going anywhere!</p>
                            <p class="about-col-desc">I resumed by work after 9 months but still stuck to my discipline to workout every night before I slept but this time I stopped doing online videos.
                            I still remember that day – I told myself today I will do 45min of only those exercises that I love – no more following workouts! And that was a game changer! No equipment .. my own variations.
I did better, enjoyed it, did more and yes lost 3 kgs that month! Weight finally 65 kgs by end of Sep !</p>
                            <p class="about-col-desc">Oct – Dec 2019 – Increased my exercises to an hour everyday and my 10,000 steps walk every single day! It started to become an addiction of getting sore from home workouts!
I took only 1 day rest to focus on flexibility training with yoga moves to enhance my stretches. By end of Dec I was back to my pre pregnancy weight! It was 9 months and 15 kgs gone!</p>
                            <p class="about-col-desc">Jan 2020! Here came the real transformation – I quit my contract job to dedicate my time fully on health &amp; fitness. Sounds crazy? Yes, I was .. that’s what many said.
But, like they say in hindi ‘sapno ko paane ke liye samajhdaar nai, paagal hona padta hai’..</p><p class="about-col-desc">Finally launched KeepWell Mantras in Feb 2020 and turned my dream into a reality.
                                                                                            The set backs &amp; emotional turmoil and dilemma of career change took longer than anticipated though. What didn’t change was my inner fire to prove myself .. it was all or none for me! I had a lot at stake..
I am sure many of you might resonate with me and know what it takes to take such huge leaps of faith in yourself. It is a struggle when no one else sees what you already believe in.</p>
                            <p class="about-col-desc">Well, I can go on and on.. but hey ! after transforming fitness of over 350 lives and having done over 1000 live workouts, I today feel damn proud of my own self!
I have never been more happier, fitter and stronger physically and mentally! I now have a huge community of fitness lovers and we rock our goals everyday!</p>
                            <p class="about-col-desc">To summarize my weight loss of over 20 kgs in 1 year:</p>
                            <ul class="about-down">
                                <li>— Workouts and diet should always be done side by side</li>
                                <li>— At home workouts or gym or PT – there is no difference, its the mindset! I never went to the gym and had no PT</li>
                                <li>— I followed a full balanced diet – no keto no shakes no supplements no pills – literally simple basic home food</li>
                                <li>— I was off processed food totally – yes no indulgences until I had shifted 15 kgs</li>
                                <li>— I didn’t throw away my pre pregnancy clothes – and didn’t buy any new clothes until I was where I wanted to be</li>
                                <li>— I always kept small goals for short period of time which wasn’t just weight loss</li>
                                <li>— Don’t be so focussed on weight loss, be focussed on becoming stronger</li>
                                <li>— I breastfed my little one until he was 2 years – it doesn’t interfere with your weight loss</li>
                                <li>— Consistency is everything !</li>
                                <li>— Don’t ever give up</li>
                                <ul>
                                </ul>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <Footer />
        </>
    );
}