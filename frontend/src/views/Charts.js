import React, { useEffect, useState } from 'react';
import { useSelector } from "react-redux";
import classnames from "classnames";
import Chart from "chart.js";
import { Line } from 'react-chartjs-2';
import {
	Card, CardHeader, CardBody, Row, Col, Nav, NavItem, NavLink,
} from "reactstrap";
import moment from "moment";

import {
	chartOptions,
	parseOptions,
	chartExample1,
} from "../variables/charts.js";

const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

export default function HealthCharts() {

	const { stats } = useSelector(st => st.user);
    const [weightTrend, setWeightTrend] = useState({
		labels: [],
		data: []
	});
	const [waistTrend, setWaistTrend] = useState({
		labels: [],
		data: []
	});

	const [activeWaistNav, setWaistActiveNav] = useState(1);
	const [activeWeightNav, setWeightActiveNav] = useState(1);

	useEffect(() => {
		if (Array.isArray(stats) && stats.length) {
			let labels = [], data = [];
			if (activeWeightNav === 3) {
				stats.forEach(e => {
					labels.push(moment(e.date).format('ll'));
					data.push(e.weight_in_kg);
				});
			} else if (activeWeightNav === 1) {
				let month = moment().month(), count = stats.length - 1, curr = moment(stats[count].date);
				while (count >= 0 && curr.month() === month) {
					data.push(stats[count].weight_in_kg);
					labels.push(curr.format('Do MMM'));
					count -= 1;
					if (count > -1)
						curr = moment(stats[count].date);
				}
                labels.reverse();
                data.reverse();

			} else {
				let year = moment().year();
				let arr = monthNames.map(e => ({ sum: 0, total: 0 }));
				stats.filter(e => year === moment(e.date).year()).forEach(e => {
					let curr = moment(e.date);
					arr[curr.month()].sum += e.weight_in_kg;
					arr[curr.month()].total += 1;
				});
				console.log(arr);
				arr.forEach((e, i) => {
					data.push((e.sum && e.total) ? parseFloat(Number(e.sum / e.total).toFixed(2), 10) : null);
					labels.push(monthNames[i]);
				})
			}

			setWeightTrend({ data, labels });

		}
	}, [stats, activeWeightNav]);

	useEffect(() => {
		if (Array.isArray(stats) && stats.length) {
			let labels = [], data = [];
			if (activeWaistNav === 3) {
				stats.forEach(e => {
					labels.push(moment(e.date).format('ll'));
					data.push(e.waist_in_inch);
				});
			} else if (activeWaistNav === 1) {
				let month = moment().month(), count = stats.length - 1, curr = moment(stats[count].date);
				while (count >= 0 && curr.month() === month) {
					data.push(stats[count].waist_in_inch);
					labels.push(curr.format('Do MMM'));
					count -= 1;
					if (count > -1)
						curr = moment(stats[count].date);
				}
                labels.reverse();
                data.reverse();
			} else {
				let year = moment().year();
				let arr = monthNames.map(e => ({ sum: 0, total: 0 }));
				stats.filter(e => year === moment(e.date).year()).forEach(e => {
					let curr = moment(e.date);
					arr[curr.month()].sum += e.waist_in_inch;
					arr[curr.month()].total += 1;
				});
				console.log(arr);
				arr.forEach((e, i) => {
					data.push((e.sum && e.total) ? parseFloat(Number(e.sum / e.total).toFixed(2), 10) : null);
					labels.push(monthNames[i]);
				})
			}

			setWaistTrend({ data, labels });

		}
	}, [stats, activeWaistNav]);

	if (window.Chart) {
		parseOptions(Chart, chartOptions());
	}

	const toggleNavs = (e, index) => {
		e.preventDefault();
		setWeightActiveNav(index);
	};

	const toggleWaistNavs = (e, index) => {
		e.preventDefault();
		setWaistActiveNav(index);
	};


    return (
        <Row className='mb-6'>
            <Col xs={12} md={6}>
                <Card className="shadow">
                    <CardHeader className="bg-transparent">
                        <Row className="align-items-center">
                            <div className="col">
                                <h6 className="text-uppercase text-muted ls-1 mb-1">
                                    Performance
                                </h6>
                                <h2 className="mb-0">Weight Trends</h2>
                            </div>
                            <div className="col">
                                <Nav className="justify-content-end" pills>
                                    <NavItem>
                                        <NavLink
                                            className={classnames("py-2 px-3", {
                                                active: activeWeightNav === 1,
                                            })}
                                            href="#pablo"
                                            onClick={(e) => toggleNavs(e, 1)}
                                        >
                                            <span className="d-none d-md-block">Month</span>
                                            <span className="d-md-none">M</span>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames("py-2 px-3", {
                                                active: activeWeightNav === 2,
                                            })}
                                            href="#pablo"
                                            onClick={(e) => toggleNavs(e, 2)}
                                        >
                                            <span className="d-none d-md-block">Year</span>
                                            <span className="d-md-none">Y</span>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames("py-2 px-3", {
                                                active: activeWeightNav === 3,
                                            })}
                                            data-toggle="tab"
                                            href="#pablo"
                                            onClick={(e) => toggleNavs(e, 3)}
                                        >
                                            <span className="d-none d-md-block">All Time</span>
                                            <span className="d-md-none">A</span>
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                            </div>
                        </Row>
                    </CardHeader>
                    <CardBody>
                        {/* Chart */}
                        <div className="chart">
                            <Line
                                data={{
                                    labels: weightTrend.labels,
                                    datasets: [{
                                        label: 'Weight',
                                        data: weightTrend.data
                                    }]
                                }}
                                options={chartExample1.weightOptions}
                                getDatasetAtEvent={(e) => console.log(e)}
                            />
                        </div>
                    </CardBody>
                </Card>
            </Col>
            <Col xs={12} md={6}>
                <Card className="shadow">
                    <CardHeader className="bg-transparent">
                        <Row className="align-items-center">
                            <div className="col">
                                <h6 className="text-uppercase text-muted ls-1 mb-1">
                                    Performance
                                </h6>
                                <h2 className="mb-0">Waist Trends</h2>
                            </div>
                            <div className="col">
                                <Nav className="justify-content-end" pills>
                                    <NavItem>
                                        <NavLink
                                            className={classnames("py-2 px-3", {
                                                active: activeWaistNav === 1,
                                            })}
                                            href="#pablo"
                                            onClick={(e) => toggleWaistNavs(e, 1)}
                                        >
                                            <span className="d-none d-md-block">Month</span>
                                            <span className="d-md-none">M</span>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames("py-2 px-3", {
                                                active: activeWaistNav === 2,
                                            })}
                                            href="#pablo"
                                            onClick={(e) => toggleWaistNavs(e, 2)}
                                        >
                                            <span className="d-none d-md-block">Year</span>
                                            <span className="d-md-none">Y</span>
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames("py-2 px-3", {
                                                active: activeWaistNav === 3,
                                            })}
                                            data-toggle="tab"
                                            href="#pablo"
                                            onClick={(e) => toggleWaistNavs(e, 3)}
                                        >
                                            <span className="d-none d-md-block">All Time</span>
                                            <span className="d-md-none">A</span>
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                            </div>
                        </Row>
                    </CardHeader>
                    <CardBody>
                        {/* Chart */}
                        <div className="chart">
                            <Line
                                data={{
                                    labels: waistTrend.labels,
                                    datasets: [{
                                        label: 'Weight',
                                        data: waistTrend.data
                                    }]
                                }}
                                options={chartExample1.waistOptions}
                                getDatasetAtEvent={(e) => console.log(e)}
                            />
                        </div>
                    </CardBody>
                </Card>
            </Col>
        </Row>
    );
}