import React from 'react';
import Slider from "react-slick";

import './styles.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import Testimonial from '../Testimonial';

export default function FitnessStars() {

    const images = ['001-1.jpeg', '001.jpeg', '002.jpeg', '01-1.jpeg', '02-1.jpeg', '03-1.jpeg', '04-1.jpeg', '05-1.jpeg', '06-1.jpeg', '07-1.jpeg', '08-1.jpeg', '09-1.jpeg', '09.jpeg', '10-1.jpeg', '10.jpeg'];

    return (
        <>
            <Header2 />

            <div className='position-relative d-flex flex-column'>

                <header className="fitness-makeover-header pb-3">
                    <h1 className="fitness-makeover-title">Some Sweet Words From Clients</h1>
                </header>

                <div className='text-center w-100 position-absolute' style={{ top: '3.5%' }}>
                    <i className="lni lni-star fitness-stars-intro-icon"></i>
                </div>

                <main className="main">
                    <div className='container'>
                        <div className="content-wrapper">
                            <div className="fitness-stars-intro">
                                <h2 className="fitness-stars-intro-title">My Fitness Stars</h2>
                                <p className="fitness-stars-desc">My Fitness Family has been doing wonders this lockdown. Smashing their goals without stepping out of their homes! Ever since I moved my sessions online, I now have a family of fitness lovers just as crazy about their fitness as I am.. My tribe has been putting in great amount of efforts to be in their best shape ever!!</p>
                                <p className="fitness-stars-desc">I have a dedicated social media group in which I take huge pride and needless to say, my fitness family loves it to the core!!! I share my recent Fitness Star announcements and why not hear from them directly.. would you like to be one of my fitness stars?</p>
                            </div>
                        </div>

                        <div className="testimony">
                            <div className='testimony-inner mx-auto d-flex flex-column flex-md-row'>
                                <div className="fitness-stars-video-gallery-card">
                                    <iframe
                                        width="853" height="480" src="https://www.youtube.com/embed/MoGHNOyc9do" title="YouTube video player"
                                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                    >
                                    </iframe>
                                </div>

                                <div className="fitness-stars-video-gallery-card">
                                    <iframe
                                        width="853" height="480" src="https://www.youtube.com/embed/FfKGtZboKZg" title="YouTube video player"
                                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                    >
                                    </iframe>
                                </div>

                                <div className="fitness-stars-video-gallery-card">
                                    <iframe
                                        width="853" height="480" src="https://www.youtube.com/embed/PcjD1kvFL8Q" title="YouTube video player"
                                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                    >
                                    </iframe>
                                </div>

                            </div>
                        </div>

                        <div className="testimony">
                            <div className='testimony-inner mx-auto d-none d-md-block'>

                                <Slider
                                    dots={false} infinite={true} slidesToScroll={1} speed={500} autoplay={true} autoplaySpeed={3000}
                                    slidesToShow={3} pauseOnHover={true}
                                    prevArrow={<div><i className="lni lni-chevron-left-circle text-dark"></i></div>}
                                    nextArrow={<div><i className="lni lni-chevron-right-circle text-dark"></i></div>}
                                >
                                    {
                                        images.map(e => (
                                            <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/${e}`} alt='..' className='img img-fluid' />
                                        ))
                                    }
                                </Slider>
                            </div>
                            <div className='testimony-inner mx-auto d-block d-md-none'>

                                <Slider
                                    dots={false} infinite={true} slidesToScroll={1} speed={500} autoplay={true} autoplaySpeed={3000}
                                    slidesToShow={1} pauseOnHover={true}
                                    prevArrow={<div><i className="lni lni-chevron-left-circle text-dark"></i></div>}
                                    nextArrow={<div><i className="lni lni-chevron-right-circle text-dark"></i></div>}
                                >
                                    {
                                        images.map(e => (
                                            <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/${e}`} alt='..' className='img img-fluid' />
                                        ))
                                    }
                                </Slider>
                            </div>
                        </div>

                        <div className="testimony">
                            <div className='testimony-inner mx-auto d-none d-md-block'>
                                <Testimonial slides={3} />
                            </div>
                            <div className='testimony-inner mx-auto d-block d-md-none'>
                                <Testimonial slides={1} />
                            </div>
                        </div>
                    </div>

                </main>

            </div>

            <Footer />
        </>
    );
}