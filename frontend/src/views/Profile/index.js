import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import update from 'immutability-helper';
import {
	Button, Card, CardHeader, CardBody, FormGroup, Form, Input,
	Container, Row, Col, Modal, ModalBody, ModalHeader, ModalFooter
} from "reactstrap";
import AvatarEditor from 'react-avatar-editor';
import { toast } from "react-toastify";
import validator from "validator";

import UserHeader from "../../components/Headers/UserHeader.js";
import _fetch from "../../_fetch.js";
import Loader from "../../components/Loader/loader.js";
import { LoginAction } from "../../actions/userActions.js";

import HealthCharts from "../Charts.js";
import Enrollments from "./enrollments.js";
import Activities from "./activity.js";
import Referral from "./referral.js";

export default function Profile() {

	const {
		first_name, last_name, mobile, profile_picture, gender, _id, bio,
		weight, waist
	} = useSelector(st => st.user);


	const [file, setFile] = useState(null);
	const [activityFile, setActivityFile] = useState(null);
	const [uploading, setUploading] = useState(null);
	const [loading, setLoading] = useState(null);
	const [updatingPass, setUpdatingPass] = useState(null);
	const [activity, setActivity] = useState([]);
	const [data, setData] = useState({
		first_name: first_name,
		last_name: last_name,
		mobile: mobile,
		gender: gender,
		bio: bio,
		weight: weight,
		waist: waist
	});
	const [pass, setPass] = useState({
		newPass: '',
		oldPass: ''
	});
	const [activityInd, setActivityInd] = useState(null);
	const editorRef = useRef();
	const activityEditorRef = useRef();
	const [zoom, setZoom] = useState('');

	let dispatch = useDispatch();

	useEffect(() => {
		_fetch(`${process.env.REACT_APP_API}/activity`)
			.then(res => {
				if (res.success) {
					setActivity(res.response);
				}
			})
			.catch(err => {
				console.log(err);
			});
	}, []);

	async function updateUserData(ev) {

		ev.preventDefault();

		if (!first_name) return toast.error('First name is required');
		setLoading(true);
		try {
			console.log(data);
			const res = await _fetch(`${process.env.REACT_APP_API}/user`, { method: "PATCH", body: data });
			if (res.success) {
				toast.success('Profile updated successfully');
				dispatch(LoginAction(res.response));
			}
		} catch (err) {
			console.log(err);
			toast.error('Some error occurred while updating your profile');
		}
		setLoading(false);
	}

	const onFileChange = (evt) => {

		const temp = evt.target.files;

		console.log(temp);

		// push raw photo/video file into arr1
		for (let i = 0; i < temp.length; i++) {
			const file = temp[i];
			console.log(file);
			if (file.type.startsWith('image/'))
				return setFile(file);
		}
	}

	const onActivityFileChange = (evt) => {

		const temp = evt.target.files;

		console.log(temp);

		// push raw photo/video file into arr1
		for (let i = 0; i < temp.length; i++) {
			const file = temp[i];
			console.log(file);
			if (file.type.startsWith('image/'))
				return setActivityFile(file);
		}
	}

	async function upload() {
		try {
			setUploading(true);
			let res = await _fetch(`${process.env.REACT_APP_API}/media/image_signed_url`);

			const formData = new FormData();

			let ext = file.name.split('.');
			if (ext.length > 1) ext = '.' + ext[ext.length - 1];
			else ext = '.png';

			let url = `${_id}/profile${new Date().getMilliseconds()}${ext}`

			formData.append('key', url);
			formData.append('acl', 'public-read');
			formData.append('Content-Type', file.type);

			for (let a in res.response) {
				formData.append(a, res.response[a]);
			}

			editorRef.current.getImageScaledToCanvas().toBlob(async (Blob) => {
				formData.append('file', Blob);
				res = await fetch(`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}`, {
					method: 'POST',
					body: formData
				});

				if (res.status === 204) {
					console.log('res', res);
					try {
						console.log(data);
						const res = await _fetch(
							`${process.env.REACT_APP_API}/user`,
							{
								method: "PATCH",
								body: {
									profile_picture: `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/${url}`
								}
							}
						);
						if (res.success) {
							toast.success('Profile updated successfully');
							dispatch(LoginAction(res.response));
						}
					} catch (err) {
						console.log(err);
						toast.error('Some error occurred while updating your profile');
					}
					setFile(null);
				}
				setUploading(false);
			});

		} catch (err) {
			console.log(err);
			toast.error('Some error occurred while uploading image');
			setUploading(false);
		}


	}

	async function uploadPictureForActivity() {
		try {

			setUploading(true);
			let res = await _fetch(`${process.env.REACT_APP_API}/media/image_signed_url`);

			const formData = new FormData();

			let ext = activityFile.name.split('.');
			if (ext.length > 1) ext = '.' + ext[ext.length - 1];
			else ext = '.png';

			let url = `${_id}/activity/activity${new Date().getMilliseconds()}${ext}`

			formData.append('key', url);
			formData.append('acl', 'public-read');
			formData.append('Content-Type', activityFile.type);

			for (let a in res.response) {
				formData.append(a, res.response[a]);
			}

			activityEditorRef.current.getImageScaledToCanvas().toBlob(async (Blob) => {
				formData.append('file', Blob);
				res = await fetch(`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}`, {
					method: 'POST',
					body: formData
				});

				if (res.status === 204) {
					console.log('res', res);
					try {
						console.log(activityInd, activity);
						const res = await _fetch(
							`${process.env.REACT_APP_API}/activity`,
							{
								method: "PATCH",
								body: {
									picture: `/${url}`,
									activity_id: activity[activityInd]._id
								}
							}
						);
						if (res.success) {
							toast.success('Picture uploaded successfully');
							setActivity(update(activity, { [activityInd]: { picture: { $set: `/${url}` } } }))
						}
					} catch (err) {
						console.log(err);
						toast.error('Some error occurred while updating your profile');
					}
					setActivityFile(null);
				}
				setUploading(false);
			});

		} catch (err) {
			console.log(err);
			toast.error('Some error occurred while uploading image');
			setUploading(false);
		}

	}

	async function updatePass() {
		try {
			setUpdatingPass(true);
			if (!pass.oldPass || !pass.newPass) {
				return toast.error('Please enter new password');
			}
			if (!validator.isStrongPassword(pass.newPass)) {
				return toast.error('Password must be at least 8 character long, and must have at least 1 lower case letter, 1 upper case letter, 1 number and 1 special symbol.');
			}

			let res = await _fetch(`${process.env.REACT_APP_API}/user/password`, { method: "PATCH", body: { password: pass.newPass, current_password: pass.oldPass } })
			if (res.success) {
				toast.success('Password updated successfully');
			} else {
				toast.error(res.response);
			}

		} catch (err) {
			console.log(err);
		} finally {
			setUpdatingPass(false);
		}
	}

	const onChange = (ev) => setData(prev => ({ ...prev, [ev.target.name]: ev.target.value }));

	const onPassChange = (ev) => setPass(prev => ({ ...prev, [ev.target.name]: ev.target.value }))

	return (
		<>
			<UserHeader />

			<Container className="mt--7" fluid>

				<Card className="bg-secondary shadow">

					<CardHeader className="bg-white border-0">
						<Row className="align-items-center">
							<Col xs="8">
								<h3 className="mb-0">My account</h3>
							</Col>
							<Col className="text-right" xs="4">
								<Button
									color="primary"
									href="#pablo"
									onClick={updateUserData}
									size="sm" disabled={!!loading}
								>
									{loading ? <Loader size={20} /> : 'Save Profile'}
								</Button>
							</Col>
						</Row>
					</CardHeader>

					<CardBody>
						<Form onSubmit={updateUserData}>

							{/* basic info */}
							<h6 className="heading-small text-muted mb-4">
								User information
							</h6>
							<div className="pl-lg-4">
								<Row>
									<Col lg="6">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-first-name"
											>
												First name
											</label>
											<Input
												className="form-control-alternative"
												value={data.first_name}
												name='first_name'
												id="input-first-name"
												placeholder="First name"
												type="text" required={false}
												onChange={onChange}
											/>
										</FormGroup>
									</Col>
									<Col lg="6">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-last-name"
											>
												Last name
											</label>
											<Input
												className="form-control-alternative"
												value={data.last_name}
												name='last_name'
												id="input-last-name"
												placeholder="Last name"
												type="text"
												onChange={onChange}
											/>
										</FormGroup>
									</Col>
								</Row>
								<Row>
									<Col lg="6">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-username"
											>
												Mobile Number
											</label>
											<Input
												className="form-control-alternative"
												value={data.mobile}
												id="input-username"
												name='mobile'
												placeholder={mobile || 'Mobile No.'}
												type="text"
												onChange={onChange}
											/>
										</FormGroup>
									</Col>
									<Col lg="6">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-gender"
											>
												Gender
											</label>
											<Input
												className="form-control-alternative"
												id="input-gender"
												name='gender'
												type="select"
												value={data.gender}
												onChange={onChange}
											>
												<option value='' disabled>Gender</option>
												<option value='FEMALE'>Female</option>
												<option value='MALE'>Male</option>
												<option value='OTHER'>Others</option>
												<option value='RATHER_NOT_SAY'>Rather Not Say</option>
											</Input>
										</FormGroup>
									</Col>
								</Row>
							</div>

							<hr className='my-4' />

							{/* medical info */}
							<h6 className="heading-small text-muted mb-4">
								Medical information
							</h6>
							<div className="pl-lg-4">
								<Row>
									<Col lg="6">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-weight"
											>
												Weight (in kg)
											</label>
											<Input
												className="form-control-alternative"
												value={data.weight}
												name='weight'
												id="input-weight"
												placeholder="Weight"
												type='number' required={false}
												onChange={onChange}
											/>
										</FormGroup>
									</Col>
									<Col lg="6">
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-waist"
											>
												Waist (in inches)
											</label>
											<Input
												className="form-control-alternative"
												value={data.waist}
												name='waist'
												id="input-waist"
												placeholder="Waist"
												type='number'
												onChange={onChange}
											/>
										</FormGroup>
									</Col>
								</Row>

								<HealthCharts />

							</div>

							<hr className="my-4" />

							{/* picture */}
							<h6 className="heading-small text-muted mb-4">
								Profile Picture
							</h6>
							<div className="pl-lg-4">
								<Row>
									<Col md="12">
										<div className='mb-3' style={{ width: 100 }}>
											{
												profile_picture ?
													<img src={profile_picture} alt='' className='img img-fluid rounded-circle' />
													:
													null
											}
										</div>
										<FormGroup>
											<label
												className="btn btn-primary btn-sm"
												htmlFor="input-profile-picture"
											>
												Browse Pictures
											</label>
											<input
												id="input-profile-picture"
												type="file" hidden accept='image/*'
												multiple={false}
												onChange={(ev) => {
													setActivityInd(null);
													onFileChange(ev);
												}}
											/>
										</FormGroup>
									</Col>
								</Row>
							</div>
							<hr className="my-4" />

							{/* Goal */}
							<h6 className="heading-small text-muted mb-4">My Goals</h6>
							<div className="pl-lg-4">
								<FormGroup>
									<Input
										className="form-control-alternative"
										placeholder="A few words about your goals ..."
										rows="3"
										type="textarea"
										name="bio"
										onChange={onChange}
										defaultValue={bio}
									/>
								</FormGroup>
							</div>
							<hr className="my-4" />

							{/* Password */}
							<h6 className="heading-small text-muted mb-4">Update Password</h6>
							<div className="pl-lg-4">
								<Row className=''>
									<Col xs={12} md={4}>
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-new-pass"
											>
												Current Password
											</label>
											<Input
												className="form-control-alternative"
												name='oldPass'
												id="input-old-pass"
												placeholder="Current Password"
												type="password"
												onChange={onPassChange}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} md={4}>
										<FormGroup>
											<label
												className="form-control-label"
												htmlFor="input-confirm-pass"
											>
												New Password
											</label>
											<Input
												className="form-control-alternative"
												name='newPass'
												id="input-new-pass"
												placeholder="New Password"
												type="password"
												onChange={onPassChange}
											/>
										</FormGroup>
									</Col>
									<Col xs={12} md={4} className='my-auto'>
										<Button onClick={updatePass} color='primary' className='mr-auto'>
											{updatingPass ? <Loader size={20} /> : 'Update Password'}
										</Button>
									</Col>
								</Row>
							</div>
							<hr className="my-4" />

						</Form>

						{/* Points and Referral Link */}

						<Referral />

						{/* Enrollments */}

						<Enrollments />

						{/* Activities */}

						<Activities
							{...{
								onActivityFileChange,
								setZoom,
								setActivityInd,
								activity
							}}
						/>

					</CardBody>

				</Card>

				{/* Modal for profile pic upload */}
				<Modal isOpen={!!(file)} toggle={() => setFile(null)}>
					<ModalHeader toggle={() => setFile(null)}>Upload Picture</ModalHeader>
					<ModalBody>
						<AvatarEditor
							ref={editorRef}
							image={file}
							width={350}
							height={350}
							border={20}
							color={[255, 255, 255, 0.6]} // RGBA
							scale={1}
							rotate={0}
						/>
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={() => upload()} className='mr-2' disabled={uploading}>
							{uploading ? <Loader size={20} /> : 'Upload'}
						</Button>
						<Button color="secondary" onClick={() => setFile(null)} disabled={uploading}>Cancel</Button>
					</ModalFooter>
				</Modal>

				{/* Modal for activity pic upload */}
				<Modal isOpen={!!(activityFile)} toggle={() => setActivityFile(null)}>
					<ModalHeader toggle={() => setActivityFile(null)}>Upload Picture</ModalHeader>
					<ModalBody>
						<AvatarEditor
							ref={activityEditorRef}
							image={activityFile}
							width={350}
							height={350}
							border={20}
							color={[255, 255, 255, 0.6]} // RGBA
							scale={1}
							rotate={0}
						/>
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={() => uploadPictureForActivity()} className='mr-2' disabled={uploading}>
							{uploading ? <Loader size={20} /> : 'Upload'}
						</Button>
						<Button color="secondary" onClick={() => setActivityFile(null)} disabled={uploading}>Cancel</Button>
					</ModalFooter>
				</Modal>

				<Modal isOpen={!!(zoom)} toggle={() => setZoom(null)}>
					<ModalBody className='text-center'>
						<img src={zoom} className='img img-fluid' alt='...' />
					</ModalBody>
					<ModalFooter>
						<Button color="secondary" onClick={() => setZoom(null)}>Close</Button>
					</ModalFooter>
				</Modal>

			</Container>
		</>
	);
};
