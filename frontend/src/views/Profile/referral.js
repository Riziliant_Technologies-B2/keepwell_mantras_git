import React from "react";
import { Row, Col } from "reactstrap";
import { EmailIcon, EmailShareButton, FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton, WhatsappIcon, WhatsappShareButton } from "react-share";
import { useSelector } from "react-redux";

export default function Referral() {

    const { points, referral_code } = useSelector(st => st.user);

    return (
        <>

            {/* Activity */}

            <h6 className="heading-small text-muted mb-4">
                Refer And Earn
            </h6>
            <div className='pl-lg-4 mb-4'>
                <Row>
                    <Col>
                        <p className='font-weight-600'>Earned Points</p>
                        <div>{points}</div>
                    </Col>
                    <Col>
                        <p className='font-weight-600'>Referral Link</p>
                        <div className='d-flex align-items-center'>
                            {process.env.REACT_APP_HOME}/register?referrer={referral_code}
                            <FacebookShareButton
                                url={`${process.env.REACT_APP_HOME}/register?referrer=${referral_code}`}
                                quote='Join KeepWell Mantras today !!'
                                className='ml-2'
                            >
                                <FacebookIcon size={32} round={true} />
                            </FacebookShareButton>
                            <WhatsappShareButton
                                url={`${process.env.REACT_APP_HOME}/register?referrer=${referral_code}`}
                                title='Join KeepWell Mantras today !!'
                                className='ml-2'
                            >
                                <WhatsappIcon size={32} round={true} />
                            </WhatsappShareButton>
                            <TwitterShareButton
                                url={`${process.env.REACT_APP_HOME}/register?referrer=${referral_code}`}
                                title='Join KeepWell Mantras today !!'
                                className='ml-2'
                            >
                                <TwitterIcon size={32} round={true} />
                            </TwitterShareButton>
                            <EmailShareButton
                                url={`${process.env.REACT_APP_HOME}/register?referrer=${referral_code}`}
                                className='ml-2'
                                body='Join KeepWell Mantras today !!'
                            >
                                <EmailIcon size={32} round={true} />
                            </EmailShareButton>
                        </div>
                    </Col>
                </Row>
            </div>

            <hr className="my-4" />

        </>
    );
};
