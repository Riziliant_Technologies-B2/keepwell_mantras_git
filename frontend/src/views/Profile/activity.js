import React from "react";
import { Card, FormGroup, Row, Col } from "reactstrap";
import moment from "moment";

export default function Activities({
    onActivityFileChange,
    setZoom,
    setActivityInd,
    activity
}) {

    return (
        <>

            {/* Activity */}
            <h6 className="heading-small text-muted mb-4">My Activity</h6>
            <div className="pl-lg-4">
                {
                    activity.map((e, i) => (
                        <Card className='my-2 shadow' key={i}>
                            <Row className='align-items-center'>
                                <Col xs={9} xl={10}>
                                    <div className='px-3 pt-3 pb-2 text-xs text-muted'>
                                        {moment(e.createdAt).format('lll')}
                                    </div>
                                    <div className='px-3 pb-3'>
                                        {
                                            e.event_type === 'WATCH_VIDEO' ?
                                                `You ${e.video_type === 'LIVE' ? 'attended a live session.' : e.video_type === 'RECORDED_LIVE' ? `watched recording of live session organized on ${moment(e?.class_id?.date).format('lll')}` : `watched ${e.video_name} workout video.`}`
                                                :
                                                `You bought: ${e.description}.`
                                        }
                                    </div>
                                </Col>
                                <Col xs={3} xl={2}>
                                    {
                                        e.picture
                                            ?
                                            <div className='p-2 pointer mx-auto' style={{ maxWidth: 100 }} onClick={() => setZoom(e.picture)}>
                                                <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}${e.picture}`} alt='...' className='img img-fluid' />
                                            </div>
                                            :
                                            <FormGroup className='mb-0 text-center'>
                                                <label
                                                    className="btn btn-info btn-sm"
                                                    htmlFor={`input-activity-picture-${i}`}
                                                >
                                                    Upload Picture
                                                </label>
                                                <input
                                                    id={`input-activity-picture-${i}`}
                                                    type="file" hidden accept='image/*'
                                                    multiple={false} onChange={(ev) => {
                                                        setActivityInd(i);
                                                        onActivityFileChange(ev);
                                                    }}
                                                />
                                            </FormGroup>
                                    }
                                </Col>
                            </Row>
                        </Card>
                    ))
                }
            </div>

        </>
    );
};
