import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, CardHeader, CardFooter, Pagination, PaginationItem, PaginationLink, Table, Container, Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import moment from "moment";
import { useHistory, useLocation } from "react-router-dom";
import { toast } from "react-toastify";

import { setDiet, getEnrollments } from "../actions/userActions.js";
import Header from "../components/Headers/Header.js";
import _fetch from '../_fetch';
import Loader from "../components/Loader/loader.js";

function useQuery() {
	return new URLSearchParams(useLocation().search);
}

export default function UserDashboard() {

	const [loading, setLoading] = useState(false);
	const [page, setPage] = useState(0);
	const [totalPages, setTotalPages] = useState(1);
	const [sessions, setSessions] = useState([]);
	const [file, setFile] = useState(null);
	const [uploading, setUploading] = useState(null);

	let dispatch = useDispatch();
	let history = useHistory();
	let invoice_id = useQuery().get('OrderID');

	const { enrollments, diet, _id } = useSelector(st => st.user);

	useEffect(() => {

		console.log(enrollments);

		if (Array.isArray(enrollments)) {
			setTotalPages(Math.ceil(enrollments.length / 5));
		}

	}, [enrollments]);

	useEffect(() => {
		if (invoice_id) {
			_fetch(`${process.env.REACT_APP_API}/order?invoice_id=${invoice_id}`)
				.then(res => {
					if (res.success) {
						if (res.response === 'COMPLETED')
							return toast.success('Payment processed successfully.');
						else return toast.error('Payment failed.');
					} else {
						console.log(res.response);
					}
				})
				.catch(err => {
					console.log(err);
				})
		}
	}, [invoice_id])

	useEffect(() => {

		async function init() {
			try {
				setLoading(true);

				await getEnrollments(dispatch);

				let res = await _fetch(`${process.env.REACT_APP_API}/classes?type=LIVE&limit=5&upcoming=true`);
				if (res.success) {
					console.log(res.response);
					setSessions(res.response);
				}

			} catch (err) {
				console.log(err);
			} finally {
				setLoading(false);
			}
		}

		init();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	async function upload() {
		try {
			setUploading(true);
			let res = await _fetch(`${process.env.REACT_APP_API}/media/file_signed_url`);

			const formData = new FormData();

			let name = file.name;

			let url = `${_id}/diet/${new Date().getMilliseconds()}${name}`

			formData.append('key', url);
			formData.append('acl', 'public-read');

			for (let a in res.response) {
				formData.append(a, res.response[a]);
			}

			formData.append('file', file);
			res = await fetch(`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}`, {
				method: 'POST',
				body: formData
			});

			if (res.status === 204) {
				console.log('res', res);
				try {
					const res = await _fetch(
						`${process.env.REACT_APP_API}/enrollment/files`,
						{
							method: "PATCH",
							body: {
								url: `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/${url}`,
								name
							}
						}
					);
					if (res.success) {
						toast.success('File uploaded successfully');
						dispatch(setDiet(res.response));
					}
				} catch (err) {
					console.log(err);
					toast.error('Some error occurred while uploading file');
				}
				setFile(null);
			}
			setUploading(false);

		} catch (err) {
			console.log(err);
			toast.error('Some error occurred while uploading file');
			setUploading(false);
		}


	}

	if (loading) return (
		<>
			<Header />

			<Container className="mt--7" fluid>
				<Card className="bg-secondary shadow" style={{ height: 400 }}>
					<Loader />
				</Card>
			</Container>
		</>
	);

	return (
		<>
			<Header />
			{/* Page content */}
			<Container className="mt--7" fluid>

				<Card className="shadow">

					<CardHeader className="border-0">
						<h3 className="mb-0">Workout Videos</h3>
					</CardHeader>

					<Table className="align-items-center table-flush" responsive>
						<thead className="thead-light">
							<tr>
								<th scope="col">Class Type</th>
								<th scope="col">Completion</th>
								<th scope="col">Purchase Date</th>
								<th scope="col" />
							</tr>
						</thead>
						<tbody>
							{
								enrollments?.slice(page * 5, (page + 1) * 5).map(e => (
									<tr>
										<th scope="row">
											{e.category_id.name}
										</th>
										<td>{e.classes.filter((e) => e.watch_start && moment().isAfter(moment(e.watch_start).add(1, 'd'))).length}/{e.class_count}</td>
										<td>
											{moment(e.createdAt).format('llll')}
										</td>
										<td>
											<Button
												type='button' color='success' className='mr-auto btn-sm'
												onClick={() => history.push(`/classes/${e._id}`)}
											>
												Watch Now
											</Button>
										</td>
									</tr>
								))
							}
						</tbody>
					</Table>

					<CardFooter className="py-4">
						<nav aria-label="...">
							<Pagination
								className="pagination justify-content-end mb-0"
								listClassName="justify-content-end mb-0"
							>
								<PaginationItem className="pointer">
									<PaginationLink
										href="#"
										onClick={(e) => page === 0 ? '' : setPage(page - 1)}
									>
										<i className="lni lni-chevron-left"></i>
										<span className="sr-only">Previous</span>
									</PaginationLink>
								</PaginationItem>

								<PaginationItem className={page === 0 ? 'active' : ''}>
									<PaginationLink
										href="#"
										onClick={(e) => page === 0 ? '' : setPage(page - 1)}
									>
										{page === 0 ? 1 : page}
									</PaginationLink>
								</PaginationItem>

								{
									(page === 0 ? totalPages > 1 : true) ?
										<PaginationItem className={page === 0 ? '' : 'active'}>
											<PaginationLink
												href="#"
												onClick={(e) => page === 0 ? setPage(1) : ''}
											>
												{page === 0 ? 2 : page + 1}
											</PaginationLink>
										</PaginationItem>
										:
										null
								}

								{
									(page === 0 ? 2 < totalPages : page + 1 < totalPages) ?
										<PaginationItem>
											<PaginationLink
												href="#"
												onClick={(e) => page === 0 ? setPage(2) : setPage(page + 1)}
											>
												{page === 0 ? 3 : page + 2}
											</PaginationLink>
										</PaginationItem>
										:
										null
								}

								<PaginationItem>
									<PaginationLink
										href="#"
										onClick={(e) => page + 1 < totalPages ? setPage(page + 1) : ''}
									>
										<i className="lni lni-chevron-right"></i>
										<span className="sr-only">Next</span>
									</PaginationLink>
								</PaginationItem>
							</Pagination>
						</nav>
					</CardFooter>

				</Card>

				{
					diet &&
					<Card className="shadow mt-5">

						<CardHeader className="border-0 d-flex justify-content-between">
							<h3 className="mb-0">Nutrition Support Plan</h3>
							<div>
								<label
									className="btn btn-primary btn-sm"
									htmlFor="input-profile-picture"
								>
									Upload Files
								</label>
								<input
									id="input-profile-picture"
									type="file" hidden accept='application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
									multiple={false}
									onChange={(ev) => {
										const temp = ev.target.files;

										console.log(temp);

										// push raw photo/video file into arr1
										for (let i = 0; i < temp.length; i++) {
											const file = temp[i];
											return setFile(file);
										}
									}}
								/>
							</div>
						</CardHeader>

						<Table className="align-items-center table-flush" responsive>
							<thead className="thead-light">
								<tr>
									<th scope="col">Diet Plan Files</th>
									<th scope="col">Upload Date</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{
									diet?.files?.map(e => (
										<tr>
											<th scope="row">
												{e.name}
											</th>
											<td>
												{moment(e.date).format('lll')}
											</td>
											<td>
												<Button
													type='button' color='success' className='mr-auto btn-sm'
													onClick={() => window.open(e.url)}
												>
													View
												</Button>
											</td>
										</tr>
									))
								}
							</tbody>
						</Table>

					</Card>
				}

				<Card className="shadow mt-5">

					<CardHeader className="border-0">
						<h3 className="mb-0">Upcoming live Classes</h3>
					</CardHeader>

					<Table className="align-items-center table-flush" responsive>
						<thead className="thead-light">
							<tr>
								<th scope="col">Class</th>
								<th scope="col">Duration</th>
								<th scope="col">Date</th>
								<th scope="col">Type</th>
								<th scope="col">Description</th>
							</tr>
						</thead>
						<tbody>
							{
								sessions.map(e => (
									<tr key={e._id}>
										<th scope="row">
											{e.title}
										</th>
										<td>{e.duration_in_minutes} min.</td>
										<td>
											{moment(e.date).format('llll')}
										</td>
										<td>{e.category.name}</td>
										<td>{e.description}</td>
									</tr>
								))
							}
						</tbody>
					</Table>

				</Card>

				<Modal isOpen={!!(file)} toggle={() => setFile(null)}>
					<ModalHeader toggle={() => setFile(null)}>Upload File</ModalHeader>
					<ModalBody>
						{file?.name}
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={() => upload()} className='mr-2' disabled={uploading}>
							{uploading ? <Loader size={20} /> : 'Upload'}
						</Button>
						<Button color="secondary" onClick={() => setFile(null)} disabled={uploading}>Cancel</Button>
					</ModalFooter>
				</Modal>

			</Container>
		</>
	);
};
