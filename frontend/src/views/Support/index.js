import React, { useEffect, useState } from 'react';
import { Container, Card, CardBody, CardHeader, FormGroup, Form, Input, InputGroupAddon, InputGroupText, InputGroup, FormFeedback, Button, Col, Row } from 'reactstrap';
import { toast } from 'react-toastify';
import { Spinner } from 'reactstrap';

import _fetch from '../../_fetch';
import Header from '../../components/Headers/Header';
import { useSelector } from 'react-redux';


const initState = {
    first_name: '',
    last_name: '',
    message: '',
    email: '',
    mobile: ''
};

export default function Support() {

    const { email, first_name, last_name, mobile } = useSelector(st => st.user);

    const [data, setData] = useState('');

    const [err, setErr] = useState(initState);
    const [sending, setSending] = useState(false);

    const [contact, setContact] = useState(false);

    function submit(ev) {

        ev.preventDefault();
        
        let err = false;

        setErr(initState);

        let obj = {}

        if (!data) {
            obj.message = "Please enter a message for us";
            err = true;
        }

        console.log(obj);

        if (err) {
            setErr(obj);
            return;
        }

        setSending(true);

        _fetch(`${process.env.REACT_APP_API}/user/support_ticket`, {
            method: "POST",
            body: {
                first_name: first_name,
                last_name: last_name,
                message: data,
                email: email,
                phone: mobile
            }
        })
            .then(res => {
                toast.success('Response submitted successfully');
                setData(initState);
                setContact(false);
            })
            .catch(err => {
                console.log(err);
            })
            .finally(() => {
                setSending(false);
            })

        console.log(data);
        return false;
    }

    useEffect(() => {
        setErr(initState);
        setData(initState);
    }, [contact]);


    return (
        <>
            <Header />
            <Container className="mt--7" fluid>
                <Card className="bg-secondary shadow">
                    <CardHeader className="bg-transparent">
                        <h2 className="mb-0">Contact Us</h2>
                    </CardHeader>
                    <CardBody>
                        <Form role="form" onSubmit={submit}>
                            <Row>
                                <Col xs={12}>
                                    <FormGroup>
                                        <InputGroup className="input-group-alternative mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText className='mb-auto'>
                                                    <i className="ni ni-chat-round" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input
                                                placeholder="Message" rows={5}
                                                type="textarea" aria-rowcount={5} name='message'
                                                onChange={(ev) => setData(ev.target.value)} invalid={!!err.message}
                                            />
                                            {err.message && <FormFeedback>{err.message}</FormFeedback>}
                                        </InputGroup>
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Button color='primary' type='submit' onClick={submit} onSubmit={submit}>
                                {sending && <Spinner style={{ width: 20, height: 20 }} />} Submit
                            </Button>

                        </Form>
                    </CardBody>
                </Card>
            </Container>
        </>
    );
}