import React, { useEffect, useState } from 'react';
// import './styles.css';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import { FormGroup, Form, Input, FormFeedback } from 'reactstrap';
import validator from 'validator';
import _fetch from '../../_fetch';
import { toast } from 'react-toastify';
import { Spinner } from 'reactstrap';
import { Link } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';


const initState = {
    first_name: '',
    last_name: '',
    message: '',
    email: '',
    phone_number: ''
};

export default function Contact() {

    const [data, setData] = useState('');

    const [err, setErr] = useState(initState);
    const [sending, setSending] = useState(false);

    const [contact, setContact] = useState(false);

    function onChange(ev) {
        setData({ ...data, [ev.target.name]: ev.target.value });
    }

    function submit(event) {
        event.preventDefault();
        let err = false;

        setErr(initState);

        let obj = {}

        if (!validator.isMobilePhone(data.phone_number)) {
            obj.phone_number = "Please enter a valid mobile number";
            err = true;
        }

        if (!validator.isEmail(data.email)) {
            obj.email = "Please enter a valid email";
            err = true;
        }

        if (data.first_name.length < 3) {
            obj.first_name = "Please enter your name";
            err = true;
        }

        if (data.last_name.length < 3) {
            obj.last_name = "Please enter your name";
            err = true;
        }

        if (!data.message) {
            obj.message = "Please enter a message for us";
            err = true;
        }

        console.log(obj);

        if (err) {
            setErr(obj);
            return;
        }

        setSending(true);

        _fetch(`${process.env.REACT_APP_API}/user/support_ticket`, {
            method: "POST",
            body: {
                first_name: data.first_name,
                last_name: data.last_name,
                message: data.message,
                email: data.email,
                phone: data.phone_number
            }
        })
            .then(res => {
                toast.success('Response submitted successfully');
                setData(initState);
                setContact(false);
            })
            .catch(err => {
                console.log(err);
            })
            .finally(() => {
                setSending(false);
            })

        console.log(data);
    }

    useEffect(() => {
        setErr(initState);
        setData(initState);
    }, [contact]);


    return (
        <>
            <Header2 />

            <div class="contact-bg page-head parallax overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <h3>CONTACT US</h3>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li>
                                    <Link>HOME</Link>
                                </li>
                                <li>।</li>
                                <li>CONTACT US</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contact-area pad90">
                <Container>
                    <Row>
                        <Col md="12">
                            <div class="section-title text-left">
                                <div class="title-bar full-width mb20">
                                    <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/ttl-bar.png`} alt="title-img" />
                                </div>
                                <h3>LEAVE YOUR Message</h3>
                                <p>GET IN TOUCH WITH OUR FITNESS TEAM</p>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md="8">
                            <div class="contact-form mt20">
                                <Form role="form" onSubmit={submit}>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <Input
                                                    placeholder="First Name"
                                                    type="text" name='first_name'
                                                    onChange={onChange} invalid={!!err.first_name}
                                                />
                                                {err.first_name && <FormFeedback>{err.first_name}</FormFeedback>}
                                            </FormGroup>
                                        </Col>
                                        <Col md="12">
                                            <FormGroup>
                                                <Input
                                                    placeholder="Last Name"
                                                    type="text" name='last_name'
                                                    onChange={onChange} invalid={!!err.last_name}
                                                />
                                                {err.last_name && <FormFeedback>{err.last_name}</FormFeedback>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <Input
                                                    placeholder="Email"
                                                    type="email" name='email'
                                                    onChange={onChange} invalid={!!err.email}
                                                />
                                                {err.email && <FormFeedback>{err.email}</FormFeedback>}
                                            </FormGroup>
                                        </Col>
                                        <Col md="12">
                                            <FormGroup>
                                                <Input
                                                    placeholder="Phone Number"
                                                    type="tel" name='phone_number'
                                                    onChange={onChange} invalid={!!err.phone_number}
                                                />
                                                {err.phone_number && <FormFeedback>{err.phone_number}</FormFeedback>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <Input
                                                    placeholder="Message"
                                                    type="textarea" id="text-area" aria-rowcount={2} name='message'
                                                    onChange={onChange} invalid={!!err.message}
                                                />
                                                {err.message && <FormFeedback>{err.message}</FormFeedback>}
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            <div class="bttn full-width">
                                                <button className="btn active full-width btn-primary" onClick={submit}>
                                                    {sending && <Spinner style={{ width: 20, height: 20 }} />} Submit
                                            </button>
                                            </div>
                                        </Col>
                                    </Row>
                                </Form>
                            </div>
                        </Col>

                        <Col md="4">
                            <div class="location mt20">
                                <h4>Contact</h4>
                                <div class="ctc-content">
                                    <i class="lni lni-mobile"></i>
                                    <p>Phone: 07584236848</p>
                                </div>
                                <div class="ctc-content">
                                    <i class="lni lni-map-marker"></i>
                                    <p>LONDON E16 2EQ,
                                    <br /> United Kingdom
                                </p>
                                </div>
                                <div class="ctc-content">
                                    <i class="lni lni-envelope"></i>
                                    <p>prapti.dutt@gmail.com</p>
                                </div>
                            </div>
                            <hr />
                            <div class="opening-time pt20">
                                <h4>Opening Time</h4>
                                <ul>
                                    <li>
                                        <p>Monday <span>06:00 AM - 07:00 PM</span></p>
                                    </li>
                                    <li>
                                        <p>Tuesday <span>06:00 AM - 07:00 PM</span></p>
                                    </li>
                                    <li>
                                        <p>Wednesday <span>06:00 AM - 07:00 PM</span></p>
                                    </li>
                                    <li>
                                        <p>Thursday <span>06:00 AM - 07:00 PM</span></p>
                                    </li>
                                    <li>
                                        <p>Friday <span>06:00 AM - 07:00 PM</span></p>
                                    </li>
                                    <li>
                                        <p>Saturday <span>08:00 AM - 01:00 PM</span></p>
                                    </li>
                                    <li>
                                        <p>Sunday <span>08:00 AM - 01:00 PM</span></p>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>

            <div class="google-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.680857716172!2d0.0362551657697277!3d51.50072377963405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a8644b68d1c7%3A0xc11899c1e05d0a18!2sWards%20Wharf%20Approach%2C%20London%20E16%202EQ%2C%20UK!5e0!3m2!1sen!2sin!4v1632231450247!5m2!1sen!2sin" title="E16 2EQ" width="100%" height="450px" style={{ border: "0" }} allowfullscreen="" loading="lazy"></iframe>
            </div>
            <Footer />
        </>
    );
}