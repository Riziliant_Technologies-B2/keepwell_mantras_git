export const testimon_data = [
    {
        "name": "Shan",
        "content_1": "My 3 months journey from fat to fit. I would say so as 3 months ago Walking up the stairs was a gruesome task for me. But now I can run up and down 😂. All because of you. I have lost 9 kg so far but I have gained stamina which is more important to me. You are doing a fab job for all of us. I am glad I found you 😍😍",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/Shan.jpg`
    },
    {
        "name": "Ritika Bartwal",
        "content_1": "Excellent upper body strength session Excellent upper body strength session. This session had a good mix of core, glutes and pelvic floor exercises. I loved the session :)",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Udita Banerjee",
        "content_1": "Varied classes with lots of flex Prapti's classes are wonderfully varied. You'll never get bored of the same routine as each class is different. She gives options of easy/hard on every exercise which makes it great to tailor to your personal fitness level. The classes can be taken live or recorded so there's flexibility as well. I'd recommend Keepwell if you are looking to make fitness a part of your regular lifestyle.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Vibhuti Vasishth",
        "content_1": "Prapti is an awesome fitness coach - the best I have met! Keep inspiring and motivating people to achieve their fitness goals and adopt a healthy lifestyle. In love with her unique exercises and simple diet!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Vani Pruthi Virmani",
        "content_1": "Ladies highly recommend Keep Well Mantras if you're looking to improve your fitness journey. This community is for ladies/mums who fancy joining a fitness community from the comfort of their home.The programs designed are thoughtful that understands struggles of mums/ladies. All the sessions- Callisthenics have high intensity and low intensity moves for one to follow. Prapti-The trainer (mum of 2) is absolutely INSPIRATIONAL in terms of her workout sessions.She is a  girl next door with her fitness journey. Super organised and hosts the morning evening daily live sessions with great professionalism. The community she has created keeps you motivated throughout the challenge with interesting recipes/water targets/steps etc. I recently finished the 6 week Shape and Shred challenge and was absolutely thrilled with the results.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    
    {
        "name": "Anneli Lingam",
        "content_1": "Feeling strong and motivated! His is the place where I found myself, to get more healthier, strong and motivated. I’m part of the fitness challenge and I experienced that the workouts and consistency helped to come over my shoulder pain. Thanks Prapti, defiantly you are the true fitness star, inspiring women, mother and a great human being!! 👍🏆🧘🏻‍♀️",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Sangrila",
        "content_1": "Prapti is Fantastic and very… Prapti is Fantastic and very professional. She inspired me to start working out 😃. With kids this online sessions have made my life easier. Can’t thank her enough 😍",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Vidya Bakshi",
        "content_1": "Very professional and supportive programme Prapti is very professional in her approach. Her approach to exercises is very supportive as one always have a choice to do higher or lower intensity. The flexibility of her programme whether to do live or recorded is really helpful. All in all she ia brilliant.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Juhi Jani",
        "content_1": "The best thing about keep well mantra… He best thing about keep well mantra is the variety and the community of like minded enthusiasts. You never feel the sense of monotony in the workouts … there’s always something to challenge and keep you on toes.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Tanya Kaju",
        "content_1": "Excellent and effective work outs Excellent and effective work outs ! Prapti is incredibly inspirational and her work outs are just what you need if you want to build stamina and lose weight …! I highly recommend her ..!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Rakhee Bal",
        "content_1": "You really get results in a short space of time! From the moment I saw Prapti's advert on Facebook to being part of her 6 week challenge, I haven't regretted it one bit! She is motivated, passionate and dedicated to what she does. Even though her fitness programme is via Zoom, I still feel that 1-2-1 time with her. She is engaging and ensures that her clients are making the most out of her fitness challenge. I have already lost 3 inches from my waistline plus half a stone with just 2 weeks of being with her! I couldn't recommend her enough. If you want someone who is serious about you wanting to make a change, she can really help!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Nkd",
        "content_1": "Fantastic trainer Fantastic trainer. Very professional and variety of exercises to choose from. Amazing transformations 👍🏼👍🏼👏🏼",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Ramya Kiran",
        "content_1": "Loving the workout sessions Loving the workout sessions. The combination of exercises has given me a boost of energy and got me into a routine. Looking forward to many more sessions. Good job Prapti.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Sanda De Silva",
        "content_1": "Amazing experience! Prapti is a true professional who is able to work towards YOUR fitness goals. Her workouts are gruelling but fun, with no two sessions ever the same! Inspirational lady who is consistently pushing my fitness limits!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Mamta Mishra",
        "content_1": "Prapti dutt is a amazing trainer Prapti dutt is a amazing trainer, knowledgable, experienced.She works well with her clients and give personal attention to everyones needs and requirements. She has changed so many life’s including mine :) by building up healthy eating habits, workouts plans.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Dhruvi jain",
        "content_1": "Excellent PT- highly recommend Excellent PT. Helped me lose 10kgs postpartum through workout and diet. Would highly recommend.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Reetika Narang",
        "content_1": "Highly recommended! Prapti is so dedicated, she really motivates you like none other I've sen before. Truly an inspiration. Her exercises are also really challenging but doable and simultaneously full of variety. Dont know how she does it!!!! Highly highly recommend!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Sweta Shah",
        "content_1": "An angel in disguise 😇 I started Prapti ' s sessions soon after my physiotherapy for neck pain was completed. I could barely hold my planks for a few seconds. I have hated going to the gym. I had decided I am going to exercise but I didn't know how. And next day I came across her page and started by 1:1 and group sessions with her. I enjoyed working out and would look forward to exercising in the evenings. I was shocked at my motivation and slowly I got stronger and fitter. I have built strength and not gotten my neck pain back. I can hold by planks for more than a minute and I have seen 300% improvement in my strength. Prapti is just amazing if you are looking to build strength 💪",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Bijal Gohil",
        "content_1": "One of the most excellent service and… You all feel welcomed and look forward to her sessions. Her advice her motivations are up to the top, from her busy life, kids she never is late for her sessions and always up for it...would highly recommend her for sure",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Bijal Gohil",
        "content_1": "One of the most excellent service and… You all feel welcomed and look forward to her sessions. Her advice her motivations are up to the top, from her busy life, kids she never is late for her sessions and always up for it...would highly recommend her for sure",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Shilpa Jain",
        "content_1": "Prapti and her amazing body has really motivated me keep fit and reduce weight. Her classes are really good. My journey on the platform of fitness has started and it will end when I reach my destination; to get the body like Prapti. I know it's not easy. She has really worked hard to reach there and she is still doing it. She really wants all of us to reach there.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Nirja Kadu Daine",
        "content_1": "Prapti is amazing fitness trainer. She is professional, highly skilled and organised in her regimes. Many got desirable body transformations by following Prapti’s diverse fitness classes. She is motivating, energetic, an inspiration to anyone who wants to see change in themselves.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    
    {
        "name": "Ruchika Gupta",
        "content_1": "For anyone looking to get fitter, I highly recommend Prapti. I have been a part of her fitness family since January. In 4 months, I have achieved: 1. Immense improvement in core and lower body strength 2. Better thyroid levels 3. Consistency in workout 4. Fat and inch loss She is very passionate about her work and her classes are fun and flexible. Join her in the next challenge and you will see amazing changes in your health.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Archana Nishanth",
        "content_1": "Prapti is extremely professional & organised physical trainer. She listens and tailors training programs based on individual needs and life styles . She motivates us really well and is very flexible to our time . I highly recommend Prapti",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Sneha Lamba",
        "content_1": "I have had an amazing transformation after joining the KeepWell Mantra Fitness Routine and Diet Consultations. I managed to hit my weight goal, strengthen my core and increase stamina within the first 6weeks. Prapti is very knowledgeable and is able to help you find what works for you. She is your ultimate WorkOut Buddy and Fitness Pal! If you are looking for a new health routine and New Years resolution, then definitely knock on this door.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Mamta Mishra",
        "content_1": "I highly recommend Prapti’s Workout sessions. They are so intense, lots of variety. No session is the same. I have been doing her sessions since May2020 and i love it. I have improved on my stamina , strength and muscle gain. Thankyou Prapti for all your motivation, you are a wonderful fitness trainer",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Sneha Kamath",
        "content_1": "Prapti is the best mentor you can have to reach your fitness goal..it's been just 3 weeks I started following Prapti's diet chart and her high intensity session and must say it works like charm and I have seen significant weight and Inch loss.... Prapti is just a phone call away and her diets are very easy to follow. Prapti is no less than a magician. Thanks Prapti I can now see myself reaching my fitness goal.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Hella Hoffmann",
        "content_1": "I love Prapti's fitness challenges. Each session had a particular focus (upper, lower, stamina, ...) and is super fun and easy to follow. Goal setting and tracking is mostly self-organized but Prapti is always available for guidance and support. Just ping her your questions. I have been doing the fitness sessions for 4 months and seeing great results. Lost 9kg and 5inches around my core, gained a lot of strength and energy, and am a lot more disciplined about food and water intake. The sessions never get boring, include varying difficulty options and recordings allow me to adapt workout timings to fit my busy schedule. Highly recommend to anyone wanting to get active again!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Sweta Deb",
        "content_1": "Prapti’s one to one sessions are very good and tailor made to suit one’s requirement. She is dedicated and has been very helpful in building my strength and weight loss. I would definitely recommend her 121 sessions",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Noopur Srivastava",
        "content_1": "I highly recommend Prapti. It's been couple of months that I started workout with her online and I would like to mention, she is amazing. I am kind of one who doesn't easily invest and continue with workouts but Prapti is such a motivation and the way she explain things is so clear. All her workouts are killing, I mean you will feel good pain in that section of your body which she is concentrating on, which is brilliant and I always look forward to her sessions. Even price wise she has made it very reasonable and actually that what attracted me at very first to give it a try and I am thankful I did. She is too good in what she is doing lots of love and blessings Noopur",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Mayura Atre",
        "content_1": "This recommendation is for Prapti for the super awesome workout sessions that she conducts on daily basis. I must say these are just not any random session that we see online, these are power packed sessions that focuses on entire body. Honestly i am still trying to get to the rhythm but at the end of each session you feel it's worth it.. You feel so good about it. Thank you Prapti, you are such sweetheart",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Chethana Suhas",
        "content_1": "Prapti is absolutely fantastic !!! She is one of the good things that happened to me during Lockdown. I have been doing her sessions since July and I just love it. Her sessions are not the same, as there’s something different everyday. She is very knowledgeable, always eager to help and most importantly incredibly passionate about making an impact in people's lives. She has helped me massively in regards to both developing the ability to workout effectively from home, and also by rethinking my diet to account for my goals. My fitness has dramatically improved since I started working with her, increasing muscle mass and reducing body fat. This is not to say that her sessions have been easy. They haven’t. But I've gotten up everyday and I've tried my best. I haven't been perfect but I think I am being consistent. I'm immensely pleased with the slow but steady progress(I'm still a work in progress), however have full confidence that I will achieve the goal I have set for myself. If you follow her, you will surely get results. Can't recommend her enough!!! Thanks Prapti for all your support and motivation. All the best",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Archudha Durairaj",
        "content_1": "I have joined her sessions she is amazing very structured workout sessions where all types of people can join. In love with the morning sessions where you can burn 300-400 cal easily. Highly recommend her",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Prajakta Yevle Badhan",
        "content_1": "Prapti's passion for fitness is infectious and her own transformation is inspiring. She offers a variety of options to train with her: zoom sessions, 1-2-1, group sessions, buggyfit sessions in the park, etc.. so you can choose your best fit and get some hand holding if you are taking first steps towards your fitness goals like I was. She is always up for chat if you wish and has helped me get into the mindset of building strength and increasing my stamina rather that focusing on inch loss/weight loss which then came as a sweet reward. I started seeing inchloss in the first 8-12 weeks but what was more impressive to me is that she helped me form habits and the knowledge sharing translated to managable lifestyle changes.I believe these will now stay with me as its been almost 6months of me practicing the learnings by myself. Thanks Prapti for sharing your mantras...keepwell always",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Pooja Srivastava",
        "content_1": "Prapti’s fitness regimes are just the perfect start to my mornings. Not an early riser but her beautifully balanced workouts have been something I have started looking forward to everyday... after initial sweet soreness, it’s like your muscles wake up to this disciplined feeling and as a result my stamina has increased manifold. Prapti is patient and always encouraging with carefully tailored circuits and also suggests a diet plan after understanding your body type... And where you falter, she is like that strict yet gentle teacher who never gives up on her students. Cannot recommend her nutritious meals suggestions enough...",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Aanchal Gumbeer",
        "content_1": "Prapti is a fantastic fitness instructor. Her sessions are so varied and fun. She is motivating and she always helps to push me to do my best. Prapti's online fitness sessions were a god sent to me in the lockdown period because of which I have kept myself fit and strong.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Tara Avdesh Verma",
        "content_1": "Amazing full body online sessions. Could feel the pain(good one!) after each session. You are great Prapti so as your style of training. All the best..",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Mahak Agarwal",
        "content_1": "Prapti is really professional and organises structured fitness sessions.. I like the personal touch in the sessions even when doing them virtually.. once you begin working out with her you would feel guilty on missing out a session.. Highly recommended!!!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Smriti Vig",
        "content_1": "It’s only been less than 2 weeks since I first started with Prapti’s fitness sessions but it has completely changed my perspective about fitness. I might not be very regular but whatever sessions I have done so far have been excellent and have got me into that fitness and exercise routine. Since then I have lost 1 kg. So a big thank you to Prapti and keep up with the good work. Looking forward to more workout sessions",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Smitha Bs",
        "content_1": "Thank you Prapti for consistently designing and training us on our fitness missions. In just 6 weeks, I believe I can achieve a better body despite my medical conditions with your guided diet plans and motivating fitness programs. You’ve truly made us realise that getting healthy is not a goal; but a way of living. Keep shining",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Shahanara Rohman",
        "content_1": "Coming out of Prapti fitness classes I feel energised, happy and ready to start my day. She really pushes you and focuses on the core body areas. I would highly recommend Prapti.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Karishma Mehe",
        "content_1": "I am fairly new to the sessions & I keep coming for more. I love the stretches & can feel the burn . Prapti is an amazing , kind & gentle mentor . I like the way she motivates you to go that extra mile & challenge yourself. I have always been a gym cardio person ,never thought I would do home exercises and still burn calories . My positive outcome of the lockdown is you “Prapti “,so glad to have found you. Looking forward for more sessions & hoping to pen down my transformation story in future.",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Divya Garg",
        "content_1": "I have recently joined Prapti’s fitness classes and really I am not one of those self motivated people who do regular excercises. But whenever I attend the class which is normally 2-3 times a week it’s worth as a new muscle of my body gets soar which I was till date unaware even existed . It assures me that my full body is impacted by unexpectedly creative excercises. I can’t speak about results yet but I am sure that it will follow once I will be more sincere with my eating and excercise till than I am enjoying energetic living",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
    {
        "name": "Rebecca Peacock",
        "content_1": "I’ve now had 4 sessions with Prapti- she is great! She knows exactly how hard to push me and I am so pleased I signed up to it. I look forward to starting the weekend classes too, now that all my weekend plans have cancelled!",
        "img": `${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/person1.jpg`
    },
];