import React from 'react';
import Slider from "react-slick";
// import './style.css';
import { slider_img } from '../ClientTransfomation/slider_img';
import { testimon_data } from './testimon_data';
import Header2 from '../../components/Headers/Header2';
import Footer from '../../components/Footers/AuthFooter';
import { Link } from "react-router-dom";
import CustomPagination from './CustomPagination';


export default function Testimonial({ slides, className }) {

    var settings = {
        infinite: true,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };



    return (
        <>
            <Header2 />

            <div class="blog-bg page-head parallax overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title text-center">
                                <h3>Testimonials</h3>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li><Link>home</Link></li>
                                <li>।</li>
                                <li>Testimonials</li>
                            </ol>
                        </div>

                    </div>

                </div>

            </div>


            <div class="blog-area news pad90">
                <div class="container no-container">
                    <CustomPagination pageSize={30} pageCount={2} currentPage={0} data={testimon_data} />
                </div>
            </div>

            {/* <Slider
                dots={false} infinite={true} slidesToScroll={1} speed={500} autoplay={true} autoplaySpeed={3000}
                slidesToShow={4} pauseOnHover={true}
                prevArrow={<div><i className="lni lni-chevron-left"></i></div>}
                nextArrow={<div><i className="lni lni-chevron-right"></i></div>}
            >
                {
                    fitness_star_data.map(e => (
                        <div key={e.name}>
                            <img src={e.banner} alt="" className='img img-fluid' />
                        </div>
                    ))
                }
            </Slider> */}
            <div class="testimonial-down">
                <Slider {...settings}>
                    {

                        slider_img.map(e => (
                            <div>
                                <img src={e.img} alt="" className='img img-fluid' />
                            </div>
                        ))
                    }
                </Slider>
            </div>
            <Footer />
        </>
    )

}