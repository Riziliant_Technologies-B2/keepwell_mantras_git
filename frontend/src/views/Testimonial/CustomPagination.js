import React, { Component } from "react";
import Masonry from 'react-masonry-css'
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

class CustomPagination extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: props.currentPage
    };
  }

  handleClick = (e, index) => {
    e.preventDefault();
    this.setState({
      currentPage: index
    });
    document.body.scrollTop = 100;
    document.documentElement.scrollTop = 100;
  };

  render() {
    const { pageSize, pageCount, data } = this.props;
    const { currentPage } = this.state;

    let pageNumbers = [];
    for (let i = 0; i < pageCount; i++) {
      pageNumbers.push(
        <PaginationItem key={i} active={currentPage === i ? true : false}>
          <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
            {i + 1}
          </PaginationLink>
        </PaginationItem>
      );
    }
    const paginatedData = data.slice(
      currentPage * pageSize,
      (currentPage + 1) * pageSize
    );

    return (
      <React.Fragment>
        <Masonry
          breakpointCols={{
            default: 3,
            1100: 3,
            700: 2,
            500: 1
          }}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {
            paginatedData.map((datum, i) => (
              <div key={i}>
                <div class="blog-box  mb30">
                  <div class="test-img">
                    <img src={datum.img} alt="Local Img" />
                  </div>
                  <div class="blog-content">
                    <div class="blog-title">
                      <p>{datum.name}</p>
                    </div>
                    <div class="news-introtxt">
                      <p>{datum.content_1}</p>
                    </div>
                  </div>
                </div>
              </div>

            ))
          }
        </Masonry>
        <React.Fragment>
          <div class="container">
          <Pagination className="justify-content-center-2 Page navigation example">
            <PaginationItem>
              <PaginationLink
                onClick={e => this.handleClick(e, currentPage - 1)}
                previous
                href="#"
              />
            </PaginationItem>
            {pageNumbers}

            <PaginationItem disabled={currentPage >= pageCount - 1}>
              <PaginationLink
                onClick={e => this.handleClick(e, currentPage + 1)}
                next
                href="#"
              />
            </PaginationItem>
          </Pagination>
          </div>
        </React.Fragment>
      </React.Fragment>
    );
  }
}

export default CustomPagination;
