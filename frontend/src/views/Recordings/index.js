import React, { useEffect, useState } from "react";
import { Container, Button, Card, CardBody, CardHeader, CardTitle, CardText, CardImg, CardFooter, Collapse } from "reactstrap";

import _fetch from '../../_fetch';
import Header from "../../components/Headers/Header.js";
import './styles.scss';
import { useHistory } from "react-router-dom";
import { useCookies } from 'react-cookie';
import Loader from "../../components/Loader/loader";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import NumericInput from 'react-numeric-input';

const CategoryCard = ({ e, setQty, qty, AddToCart }) => {
    const [hover, setHover] = useState(false);

    return (
        <div className='col-xs-12 col-md-6 my-3 col-lg-4 my-3'>
            <Card className='overflow-hidden shadow'>
                <CardImg src={e.thumbnail} />
                <CardBody>
                    <CardTitle tag="h5" style={{ fontSize: 18 }} className='mb-2'>
                        {e.name} <span className='text-muted'>({e.duration?.min}-{e.duration?.max}mins)</span>
                    </CardTitle>
                    <CardText className='text-primary pointer d-flex justify-content-between' onClick={() => setHover(!hover)}>
                        <span>{hover ? 'Show Less' : 'Show More'}</span>
                        {
                            hover ?
                                <i className="lni lni-chevron-up-circle text-lg"></i>
                                :
                                <i className="lni lni-chevron-down-circle text-lg"></i>
                        }
                    </CardText>
                    <Collapse isOpen={hover}>
                        <CardText id='card-desc' className={`text-sm ${hover ? 'card-desc-show' : 'card-desc-hidden'}`}>
                            {e.description}
                        </CardText>
                    </Collapse>
                </CardBody>
                <CardFooter className='align-items-center'>
                    <NumericInput className='card-total-class'
                        max={e.classes.length} min={0} step={1}
                        onChange={(val) =>  setQty({ ...qty, [e._id]: val })}
                        value={qty[e._id]}
                    />
                    <Button color='primary' className='ml-3'
                        onClick={AddToCart}
                    >
                        Add To Cart
                    </Button>
                </CardFooter>
            </Card>
        </div>
    )

}

export default function Recordings() {

    const [categories, setCategories] = useState([]);
    const [qty, setQty] = useState({});
    const [loading, setLoading] = useState(false);
    const [cookie, setCookie] = useCookies(['cart']);
	const [recorded_classes_count, setRecordedClassesCount] = useState(0);
	const { total_recorded_classes, watched_recorded_classes } = useSelector(st => st.user);

    let history = useHistory();

    useEffect(() => {
        if (cookie.cart) {
            let obj = {};
            let qty = cookie.cart.split(',').map(e => e.split(':'));
            qty.forEach(e => {
                console.log(e);
                let quantity = parseInt(e[1], 10);
                obj[e[0]] = isNaN(quantity) ? 0 : quantity;
            });
            setQty(obj);
        }
    }, [cookie.cart]);

    useEffect(() => {

        async function init() {
            try {
                setLoading(true);
                let res = await _fetch(`${process.env.REACT_APP_API}/category`)
                if (res.success && Array.isArray(res.response)) {
                    setCategories(res.response);
                    setQty(val => {
                        let obj = {};
                        res.response.forEach(element => {
                            obj[element._id] = val[element._id] ? val[element._id] : 0;
                        });
                        return obj;
                    });
                }

            } catch (err) {
                console.log(err);
            } finally {
                setLoading(false);
            }
        }

        init();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

	useEffect(() => {
		if (!isNaN(total_recorded_classes) && !isNaN(watched_recorded_classes))
			setRecordedClassesCount(total_recorded_classes - watched_recorded_classes)
	}, [total_recorded_classes, watched_recorded_classes]);

    function AddToCart() {

        let str = '';

        for (let key in qty) {
            console.log(key, qty[key]);
            if (qty[key] > 0) {
                str += `${key}:${qty[key]},`;
            }
        }

        str = str.substr(0, str.length - 1);

        setCookie('cart', str, { maxAge: 2592000, sameSite: true, path: '/' });
        toast.success('Successfully added to cart ');
    }

    if (loading) return (
        <>
            <Header />

            <Container className="mt--7" fluid>
                <Card className="bg-secondary shadow" style={{ height: 400 }}>
                    <Loader />
                </Card>
            </Container>
        </>
    );

    return (
        <>
            <Header />

            {/* Page content */}
            <Container className="mt--7" fluid>
                <Card className="bg-secondary shadow">

                    <CardHeader className="bg-white border-0">
                        <div className='d-flex flex-column flex-md-row justify-content-between'>
							<div>
								<div className='text-lg font-weight-600'>Available Workout Videos: {recorded_classes_count}</div>
								<p> Buy your workouts for 6 weeks </p>
							</div>
                            <div>
                                <Button color='secondary'
                                    onClick={() => history.push('/my-classes')}
                                    className='mr-2 my-2 my-md-0 my-recorded-class'
                                >
                                    View My Classes
                                </Button>
                                <Button color='primary'
                                    onClick={() => {
                                        AddToCart();
                                        history.push('/buy-classes')
                                    }}
                                    className='my-2 my-md-0 recorded-class-checkout'
                                >
                                    Checkout Cart
                                </Button>
                            </div>
                        </div>
                    </CardHeader>

                    <CardBody>

                        <div className='row align-items-stretch cat-card' style={{minHeight: 500}}>
                            {
                                categories.map((e, i) => (
                                    <CategoryCard e={e} qty={qty} setQty={setQty} AddToCart={AddToCart} key={i} />
                                ))
                            }
                        </div>

                        <div>
                            <Button color='primary'
                                onClick={() => {
                                    AddToCart();
                                    history.push('/buy-classes')
                                }}
                            >
                                Checkout Cart
                            </Button>
                        </div>

                    </CardBody>
                </Card>
            </Container>
        </>
    );
};
