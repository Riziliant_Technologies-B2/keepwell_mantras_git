import React, { useState } from "react";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import {
	Button,
	Card,
	CardHeader,
	CardBody,
	FormGroup,
	Form,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Row,
	Col,
} from "reactstrap";
import validator from "validator";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';

import _fetch from "../../_fetch";
import Loader from '../../components/Loader/loader';
import { LoginAction } from "../../actions/userActions";

function useQuery() {
	return new URLSearchParams(useLocation().search);
}

export default function Register() {

	const [first_name, setFirstName] = useState('');
	const [last_name, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [err, setErr] = useState('');
	const [loading, setLoading] = useState(false);
	const [disabled, setDisabled] = useState(true);
	const history = useHistory();
	const dispatch = useDispatch();

	let referral_code = useQuery().get('referrer');

	function register(ev) {
		ev.preventDefault();

		setErr('');

		if (!email || !validator.isEmail(email))
			return setErr('Please enter a valid email id');
		else if (!password || !validator.isStrongPassword(password))
			return setErr('Password must be at least 8 character long, and must have at least 1 lower case letter, 1 upper case letter, 1 number and 1 special symbol.');

		setLoading(true);

		_fetch(process.env.REACT_APP_API + '/user', { method: 'POST', body: { first_name, last_name, email, password, referral_code } })
			.then(res => {
				if (res.success) {
					toast.success(res.response.message);
				} else {
					toast.error(res.response);
				}
			})
			.catch(err => {
				console.log(err);
				toast.error('Some error occurred please try again later.')
			})
			.finally(() => {
				setLoading(false);
			});

		return false;
	}

	function facebookLogin(response) {
		console.log(response);
		if(response.accessToken)
			_fetch(process.env.REACT_APP_API + '/user/facebook', { method: "POST", body: { access_token: response.accessToken } })
				.then(res => {
					if (res.success) {
						dispatch(LoginAction(res.response));
						history.push('/');
					} else {
						toast.error(res.response);
					}
				})
				.catch(err => {
					console.log(err);
					toast.error('Some error occurred please try again later.')
				})
				.finally(() => {
					setLoading(false);
				});
		else toast.error('Some error occurred please try again');
	}

	function googleLogin(response) {
		console.log(response);
		if(response.accessToken)
			_fetch(process.env.REACT_APP_API + '/user/google', { method: "POST", body: { id_token: response.tokenObj.id_token } })
				.then(res => {
					if (res.success) {
						dispatch(LoginAction(res.response));
						history.push('/');
					} else {
						toast.error(res.response);
					}
				})
				.catch(err => {
					console.log(err);
					toast.error('Some error occurred please try again later.')
				})
				.finally(() => {
					setLoading(false);
				});
	}

	function googleLoginError(error) {
		if(error.error === 'idpiframe_initialization_failed') return;
		toast.error('Some error occurred please try again later');
	}

	return (
		<>
			<Col lg="6" md="8">
				<Card className="bg-secondary shadow border-0">

					<CardHeader className="bg-transparent pb-5">
						<div className="text-muted text-center mt-2 mb-4">
							<small>Sign up with</small>
						</div>
						<div className="text-center">

						<GoogleLogin
								clientId={process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID}
								buttonText="Login"
								onSuccess={googleLogin}
								onFailure={googleLoginError}
								cookiePolicy={'single_host_origin'}
								scopes='profile'
								render={renderProps => (
									<Button
										className="btn-neutral btn-icon"
										color="default"
										onClick={renderProps.onClick}
									>
										<span className="btn-inner--icon">
											<img
												alt="Register With Google"
												src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/google.svg`}
											/>
										</span>
										<span className="btn-inner--text">Google</span>
									</Button>
								)}
								autoLoad={false}
							/>

							<FacebookLogin
								appId={process.env.REACT_APP_FACEBOOK_APP_ID}
								callback={facebookLogin}
								render={renderProps => (
									<Button
										className="btn-neutral btn-icon"
										color="default"
										onClick={renderProps.onClick}
									>
										<span className="btn-inner--icon">
											<img
												alt="Register With Facebook"
												src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/facebook.png`}
											/>
										</span>
										<span className="btn-inner--text">Facebook</span>
									</Button>
								)}
								cookie={false}
								autoLoad={false}
							/>

						</div>
					</CardHeader>

					<CardBody className="px-lg-5 py-lg-5">

						<div className="text-center text-muted mb-4">
							<small>Or sign up with credentials</small>
						</div>

						<Form role="form" onSubmit={register}>

							<FormGroup>
								<InputGroup className="input-group-alternative mb-3">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-circle-08" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="First Name"
										type="text"
										onChange={ev => setFirstName(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							<FormGroup>
								<InputGroup className="input-group-alternative mb-3">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-hat-3" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Last Name"
										type="text"
										onChange={ev => setLastName(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							<FormGroup>
								<InputGroup className="input-group-alternative mb-3">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-email-83" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Email"
										type="email"
										onChange={ev => setEmail(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							<FormGroup>
								<InputGroup className="input-group-alternative">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-lock-circle-open" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Password"
										type="password"
										onChange={ev => setPassword(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							<Row className="my-4">
								<Col xs="12">
									<div className="custom-control custom-control-alternative custom-checkbox">
										<input
											className="custom-control-input"
											id="customCheckRegister"
											type="checkbox"
											onChange={(ev) => setDisabled(!ev.target.checked)}
										/>
										<label
											className="custom-control-label"
											htmlFor="customCheckRegister"
										>
											<span className="text-muted">
												I agree with the{" "}
												<a href="#pablo" onClick={(e) => e.preventDefault()}>
													Privacy Policy
												</a>
											</span>
										</label>
									</div>
								</Col>
							</Row>

							{err ? <p className='text-red text-xs font-weight-400'>{err}</p> : null}

							<div className="text-center">
								<Button className="mt-4" color="primary" type="submit" disabled={disabled} onSubmit={register}>
									{loading ? <Loader size={20} /> : 'Create account'}
								</Button>
							</div>

						</Form>
					</CardBody>
				</Card>
				<Row className="mt-3">
					<Col xs="6">
						<Link
							className="text-light"
							to='/forgot-password'
						>
							<small>Forgot password?</small>
						</Link>
					</Col>
					<Col className="text-right" xs="6">
						<Link
							className='text-light'
							to='/login'
						>
							<small>Already have an account?</small>
						</Link>
					</Col>
				</Row>
			</Col>
		</>
	);
}
