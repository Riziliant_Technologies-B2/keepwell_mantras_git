import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import {
	Button,
	Card,
	CardHeader,
	CardBody,
	FormGroup,
	Form,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Col,
} from "reactstrap";
import validator from "validator";
import Loader from "../../components/Loader/loader";

import _fetch from "../../_fetch";

function useQuery() {
	return new URLSearchParams(useLocation().search);
}

export default function Reset() {

	const history = useHistory();
	const query = useQuery();

	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [err, setErr] = useState('');
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		if (!query.get('tok')) {
			history.replace('/login');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	function reset(ev) {
		console.log(ev);
		ev.preventDefault();

		if(password !== confirmPassword) return setErr('Password and confirm password do not match');

		if (!password || !validator.isStrongPassword(password))
			return setErr('Password must be at least 8 character long, and must have at least 1 lower case letter, 1 upper case letter, 1 number and 1 special symbol.');


		setLoading(true);

		_fetch(process.env.REACT_APP_API + '/user/forgot_password', { method: "PATCH", body: { password, token: query.get('tok') } })
			.then(res => {
				if (res.success) {
					toast.success('Password reset successfully');
					history.push('/login');
				} else toast.error(res.response || 'Some error occurred please try again');
			})
			.catch(err => {
				console.log(err);
				toast.error('Some error occurred please try again later.')
			})
			.finally(() => {
				setLoading(false);
			});

		return false;
	}

	return (
		<>
			<Col lg="5" md="7">
				<Card className="bg-secondary shadow border-0">

					<CardHeader className="bg-transparent pb-5">
						<div className="text-muted text-center mt-2 mb-3">
							<small>Reset Password</small>
						</div>
					</CardHeader>

					<CardBody className="px-lg-5 py-lg-5">

						<Form role="form" onSubmit={reset}>

							<FormGroup>
								<InputGroup className="input-group-alternative">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-lock-circle-open" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Password"
										type="password"
										required={true}
										onChange={(ev) => setPassword(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							<FormGroup>
								<InputGroup className="input-group-alternative">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-lock-circle-open" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Confirm Password"
										type="password"
										required={true}
										onChange={(ev) => setConfirmPassword(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							{err ? <p className='text-danger text-xs font-weight-400'>{err}</p> : null}

							<div className="text-center">
								<Button className="my-4" color="primary" type="submit" onSubmit={reset}>
									{loading ? <Loader size={20} /> : 'Reset Password'}
								</Button>
							</div>

						</Form>
					</CardBody>
				</Card>
			</Col>
		</>
	);
};
