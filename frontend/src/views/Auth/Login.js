import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { Link, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import {
	Button,
	Card,
	CardHeader,
	CardBody,
	FormGroup,
	Form,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Row,
	Col,
} from "reactstrap";
import validator from "validator";
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';

import { LoginAction } from "../../actions/userActions";
import Loader from "../../components/Loader/loader";

import _fetch from "../../_fetch";

function useQuery() {
	return new URLSearchParams(useLocation().search);
}

export default function Login() {

	const history = useHistory();
	const dispatch = useDispatch();
	const query = useQuery();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [err, setErr] = useState('');
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		if (query.get('verified')) {
			toast.success('Email Id verified successfully.');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	function login(ev) {
		console.log(ev);
		ev.preventDefault();

		if (!email || !validator.isEmail(email) || !password)
			return setErr('Please enter valid email id and password');

		setLoading(true);

		_fetch(process.env.REACT_APP_API + '/user/login', { method: "POST", body: { email, password } })
			.then(res => {
				if (res.success) {
					if (res.response.verified) {
						dispatch(LoginAction(res.response));
						history.push('/dashboard');
					} else return setErr('Please verify you email id first.')
				} else {
					toast.error(res.response);
				}
			})
			.catch(err => {
				console.log(err);
				toast.error('Some error occurred please try again later.')
			})
			.finally(() => {
				setLoading(false);
			});

		return false;
	}

	function facebookLogin(response) {
		console.log(response);
		if(response.accessToken)
			_fetch(process.env.REACT_APP_API + '/user/facebook', { method: "POST", body: { access_token: response.accessToken } })
				.then(res => {
					if (res.success) {
						dispatch(LoginAction(res.response));
						history.push('/dashboard');
					} else {
						toast.error(res.response);
					}
				})
				.catch(err => {
					console.log(err);
					toast.error('Some error occurred please try again later.')
				})
				.finally(() => {
					setLoading(false);
				});
		else toast.error('Some error occurred please try again');
	}

	function googleLogin(response) {
		console.log(response);
		if(response.accessToken)
			_fetch(process.env.REACT_APP_API + '/user/google', { method: "POST", body: { id_token: response.tokenObj.id_token } })
				.then(res => {
					if (res.success) {
						dispatch(LoginAction(res.response));
						history.push('/dashboard');
					} else {
						toast.error(res.response);
					}
				})
				.catch(err => {
					console.log(err);
					toast.error('Some error occurred please try again later.')
				})
				.finally(() => {
					setLoading(false);
				});
	}

	function googleLoginError(error) {
		if(error.error === 'idpiframe_initialization_failed') return;
		toast.error('Some error occurred please try again later');
	}

	return (
		<>
			<Col lg="5" md="7">
				<Card className="bg-secondary shadow border-0">

					<CardHeader className="bg-transparent pb-5">
						<div className="text-muted text-center mt-2 mb-3">
							<small>Sign in with</small>
						</div>
						<div className="btn-wrapper text-center">

							<GoogleLogin
								clientId={process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID}
								buttonText="Login"
								onSuccess={googleLogin}
								onFailure={googleLoginError}
								cookiePolicy={'single_host_origin'}
								scopes='profile'
								render={renderProps => (
									<Button
										className="btn-neutral btn-icon"
										color="default"
										onClick={renderProps.onClick}
									>
										<span className="btn-inner--icon">
											<img
												alt="Login With Google"
												src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/google.svg`}
											/>
										</span>
										<span className="btn-inner--text">Google</span>
									</Button>
								)}
								autoLoad={false}
							/>

							<FacebookLogin
								appId={process.env.REACT_APP_FACEBOOK_APP_ID}
								callback={facebookLogin}
								render={renderProps => (

									<Button
										className="btn-neutral btn-icon"
										color="default"
										onClick={renderProps.onClick}
									>
										<span className="btn-inner--icon">
											<img
												alt="Login With Facebook"
												src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/facebook.png`}
											/>
										</span>
										<span className="btn-inner--text">Facebook</span>
									</Button>
								)}
								cookie={false}
								autoLoad={false}
							/>

						</div>
					</CardHeader>

					<CardBody className="px-lg-5 py-lg-5">

						<div className="text-center text-muted mb-4">
							<small>Or sign in with credentials</small>
						</div>

						<Form role="form" onSubmit={login}>

							<FormGroup className="mb-3">
								<InputGroup className="input-group-alternative">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
										<i class="lni lni-envelope"></i>
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Email"
										type="email"
										required={true}
										onChange={(ev) => setEmail(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							<FormGroup>
								<InputGroup className="input-group-alternative">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-lock-circle-open" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Password"
										type="password"
										required={true}
										onChange={(ev) => setPassword(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							{err ? <p className='text-danger text-xs font-weight-400'>{err}</p> : null}

							<div className="text-center">
								<Button className="my-4" color="primary" type="submit" onSubmit={login}>
									{loading ? <Loader size={20} /> : 'Sign in'}
								</Button>
							</div>

						</Form>
					</CardBody>
				</Card>
				<Row className="mt-3">
					<Col xs="6">
						<Link
							className="text-light"
							to='/forgot-password'
						>
							<small>Forgot password?</small>
						</Link>
					</Col>
					<Col className="text-right" xs="6">
						<Link
							className="text-light"
							to='/register'
						>
							<small>Create new account</small>
						</Link>
					</Col>
				</Row>
			</Col>
		</>
	);
};
