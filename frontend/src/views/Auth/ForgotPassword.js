import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Link, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import {
	Button,
	Card,
	CardHeader,
	CardBody,
	FormGroup,
	Form,
	Input,
	InputGroupAddon,
	InputGroupText,
	InputGroup,
	Col,
	Row
} from "reactstrap";
import validator from "validator";
import Loader from "../../components/Loader/loader";

import _fetch from "../../_fetch";

function useQuery() {
	return new URLSearchParams(useLocation().search);
}

export default function ForgotPassword() {

	const history = useHistory();
	const query = useQuery();

	const [email, setEmail] = useState('');
	const [err, setErr] = useState('');
	const [loading, setLoading] = useState(false);

	useEffect(()=>{
		if(query.get('verified')){
			toast.success('Email Id verified successfully.');
		}
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	function forgot(ev) {
		ev.preventDefault();

		if (!email || !validator.isEmail(email))
			return setErr('Please enter valid email id and password');

		setLoading(true);

		_fetch(process.env.REACT_APP_API + `/user/forgot_password?email=${email}`)
			.then(res => {
				if (res.success) {
					toast.success('Please check your email for password reset instructions.');
					history.push('/login');
				} else {
					toast.error(res.response);
				}
			})
			.catch(err => {
				console.log(err);
				toast.error('Some error occurred please try again later.')
			})
			.finally(() => {
				setLoading(false);
			});

		return false;
	}

	return (
		<>
			<Col lg="5" md="7">
				<Card className="bg-secondary shadow border-0">

					<CardHeader className="bg-transparent">
						<h4>Enter registered email address to reset password</h4>
					</CardHeader>

					<CardBody className="px-lg-5 py-lg-5">

						<Form role="form" onSubmit={forgot}>

							<FormGroup className="mb-3">
								<InputGroup className="input-group-alternative">
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="ni ni-email-83" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Email"
										type="email"
										required={true}
										onChange={(ev) => setEmail(ev.target.value)}
									/>
								</InputGroup>
							</FormGroup>

							{err ? <p className='text-danger text-xs font-weight-400'>{err}</p> : null}

							<div className="text-center">
								<Button className="my-4" color="primary" type="submit" onSubmit={forgot}>
									{loading ? <Loader size={20} /> : 'Reset Password'}
								</Button>
							</div>

						</Form>
					</CardBody>
				</Card>
				<Row className="mt-3">
					<Col xs="6">
						<Link
							className="text-light"
							to='/login'
						>
							<small>Login</small>
						</Link>
					</Col>
					<Col className="text-right" xs="6">
						<Link
							className="text-light"
							to='/register'
						>
							<small>Create new account</small>
						</Link>
					</Col>
				</Row>
			</Col>
		</>
	);
};
