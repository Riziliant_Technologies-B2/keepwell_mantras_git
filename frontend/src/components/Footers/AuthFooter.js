import React from "react";
import { Link } from 'react-router-dom';
import { Container, Col, Row } from "reactstrap";
import Slider from "react-slick";
import { footer_slide_data } from './footer_slide_data';

export default function Footer() {
	return (
		<footer>
			<section class="footer-area bg3 parallax overlay pt90">
				<Container>
					<Row>
						<Col md="4" sm="12">
							<div class="footer-logo footer-content">
								<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo2.png`} alt="footer logo"></img>
							</div>
							<p>Prapti Dutt, the owner of KeepWell Mantras is a certified level three Personal Trainer, a group Fitness instructor and an advanced clinical weight loss practitioner with a decade full of experience in fitness and wellness.</p>
							<div class="add-info">
								<p><Link><i class="lni lni-map-marker" aria-hidden="true"></i></Link>London</p>
								<p><Link><i class="lni lni-envelope" aria-hidden="true"></i></Link>prapti.dutt@gmail.com</p>
								<p class="mb-0"><Link><i class="lni lni-mobile" aria-hidden="true"></i></Link>07584236848</p>
							</div>
						</Col>

						<Col md="4" sm="12">
							<div class="news-info open-hrs ftr-algn">
								<div class="footer-title footer-content">
									<h3>opening hours</h3>
								</div>
								<ul>
									<li>Monday <span>06:00 AM - 07:00 PM</span></li>
									<li>tuesday <span>06:00 AM - 07:00 PM</span></li>
									<li>wednesday <span>06:00 AM - 07:00 PM</span></li>
									<li>thursday <span>06:00 AM - 07:00 PM</span></li>
									<li>friday <span>06:00 AM - 07:00 PM</span></li>
									<li>saturday <span>08:00 AM - 01:00 PM</span></li>
									<li>sunday <span>08:00 AM - 01:00 PM</span></li>
								</ul>
							</div>
						</Col>

						<Col md="4" sm="12">
							<div class="news-info ftr-algn">
								<div class="footer-title footer-content">
									<h3>Contact</h3>
								</div>
								<p> Scan QR</p>
								<img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/qr-code.jpg`} alt="QR-img" />
								<div class="footer-social">
									<ul>
										<li>
											<Link to={{ pathname: "https://www.facebook.com/KeepWellMantras/"}} target="_blank" rel="noreferrer">
												<i class="lni lni-facebook-filled"></i>
											</Link>
										</li>
										<li>
											<Link to={{ pathname: "https://www.instagram.com/Keepwell_mantras"}} target="_blank" rel="noopener noreferrer">
												<i class="lni lni-instagram-original"></i>
											</Link>
										</li>
										<li>
											<Link to={{ pathname: "https://www.youtube.com/channel/UCaNDYGA8MyjJuvD-NELWKRQ"}} target="_blank" rel="noopener noreferrer">
												<i class="lni lni-youtube"></i>
											</Link>
										</li>
									</ul>
								</div>
							</div>
						</Col>

					</Row>

					<Row>
						<Col lg="12" md="12" sm="12" xs="12">
							<div class="instra pad60">
								<h4><span>Instagram</span></h4>
							</div>
						</Col>
					</Row>
				</Container>

			</section>
<div class="custom_slid">
			<Slider
					dots={false} infinite={true} slidesToScroll={1} speed={500} autoplay={true} autoplaySpeed={3000}
					slidesToShow={6} pauseOnHover={true}
					prevArrow={<div><i className="lni lni-chevron-left"></i></div>}
					nextArrow={<div><i className="lni lni-chevron-right"></i></div>}
				>
					{
						footer_slide_data.map(e => (
							<div key={e.name}>
								<img src={e.banner} alt="" className='img img-fluid' />
							</div>
						))
					}
				</Slider>
</div>
			<div class="copyright pad30">
				<h4>Copyright@2021</h4>
			</div>
		</footer>
	);
};