import React, { useEffect, useState } from 'react';
import './new_styles.scss';
import './responsive.scss';
import { NavLink as NavLinkRRD, Link } from "react-router-dom";
import { useSelector } from 'react-redux';
import { Collapse, Nav, NavItem, NavLink, Container } from 'reactstrap';

export default function Header2() {

    const { _id } = useSelector(st => st.user);
    const [collapseOpen, setCollapseOpen] = useState();
    // const { home_headline } = useSelector(st => st.app);

    useEffect(() => console.log(_id), [_id]);

    const toggleCollapse = () => {
        setCollapseOpen((data) => !data);
    };

    // closes the collapse
    const closeCollapse = () => {
        setCollapseOpen(false);
    };

    return (
        <header className="header fixed-top">
            <nav className="navbar navbar-expand-lg navbar-dark">
                <Container >
                    <div className="navbar-header">
                        <Link className="navbar-brand" to='/'>
                            <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo-keepwell.png`} alt="" />
                        </Link>
                        <button
                            className="navbar-toggler"
                            type="button"
                            onClick={toggleCollapse}
                        >
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>

                    <Collapse navbar isOpen={collapseOpen}>

                        {/* Navigation */}
                        <Nav navbar className='mx-auto'>
                            <NavItem>
                                <NavLink
                                    to={'/home'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Home
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/about'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    About
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/testimonials'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Testimonials
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/packages'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Packages
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/services'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Services
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/classes'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Classes
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/client-transformation'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Client Transformation
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/contact'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="active"
                                >
                                    Contact Us
                                </NavLink>
                            </NavItem>
                            {
                                _id ?
                                    <NavItem>
                                        <NavLink
                                            to={'/dashboard'}
                                            tag={NavLinkRRD}
                                            onClick={closeCollapse}
                                            activeClassName="header2-link-active"
                                        >
                                            Dashboard
                                        </NavLink>
                                    </NavItem>
                                    :
                                    <React.Fragment>
                                        <NavItem>
                                            <NavLink
                                                to={'/login'}
                                                tag={NavLinkRRD}
                                                onClick={closeCollapse}
                                                activeClassName="header2-link-active"
                                            >
                                                Login
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                to={'/register'}
                                                tag={NavLinkRRD}
                                                onClick={closeCollapse}
                                                activeClassName="header2-link-active"
                                            >
                                                Register
                                            </NavLink>
                                        </NavItem>
                                    </React.Fragment>
                            }
                        </Nav>

                    </Collapse>

                </Container>
            </nav>

            {/* <Ticker speed={3}>
				{() => (
					<h3
						className='d-inline pointer' style={{ whiteSpace: "nowrap" }}
						onClick={() => _id ? history.push('/dashboard') : history.push('/register')}
					>
						{home_headline.map(e => <span>{e}&emsp;·&emsp;</span>)}
					</h3>
				)}
			</Ticker> */}
        </header>
    );
}