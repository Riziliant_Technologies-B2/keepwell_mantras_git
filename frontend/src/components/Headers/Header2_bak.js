import React, { useEffect, useState } from 'react';
import './styles.scss';
import { NavLink as NavLinkRRD, Link, useHistory } from "react-router-dom";
import { useSelector } from 'react-redux';
import { Col, Collapse, Nav, NavItem, Row, NavLink, Navbar, Container, NavbarBrand, NavbarToggler } from 'reactstrap';
import Ticker from 'react-ticker'

export default function Header2() {

    const { _id } = useSelector(st => st.user);
    const [collapseOpen, setCollapseOpen] = useState();
	const { home_headline } = useSelector(st => st.app);
	let history = useHistory();

    useEffect(() => console.log(_id), [_id]);

    const toggleCollapse = () => {
        setCollapseOpen((data) => !data);
    };

    // closes the collapse
    const closeCollapse = () => {
        setCollapseOpen(false);
    };

    return (
        <header className='w-100 d-flex flex-column overflow-visible position-relative'>
            <Container fluid className="my-3 d-flex flex-row">
                <a href='https://www.facebook.com/keepwellmantras' className="header-social-item ml-auto mr-2" target="_blank" rel="noopener noreferrer">
                    <i className="lni lni-facebook"></i>
                </a>
                <a href='https://www.instagram.com/keepwell_mantras' className="header-social-item" target="_blank" rel="noopener noreferrer">
                    <i className="lni lni-instagram"></i>
                </a>
            </Container>
            <Container fluid className='align-items-center justify-content-center mb-3 d-none d-md-flex'>
                <div className="header-logo">
                    <Link to='/'>
                        <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo-keepwell.png`} alt="" />
                    </Link>
                </div>
            </Container>
            <Navbar
                className="navbar-horizontal fixed-left navbar-light"
                expand="md"
                id="sidenav-main"
            >
                <Container fluid>

                    <NavbarBrand tag={NavLinkRRD} to='/' className="mr-auto d-md-none">
                        <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo-keepwell.png`} className='img img-fluid' alt="" />
                    </NavbarBrand>
                    <NavbarToggler className="mr-2" tag={() => <span className="navbar-toggler-icon d-md-none" onClick={toggleCollapse} />} />

                    {/* Collapse */}

                    <Collapse navbar isOpen={collapseOpen}>

                        {/* Collapse header */}
                        <div className="navbar-collapse-header d-md-none">
                            <Row>
                                <Col className="collapse-brand" xs="6">
                                    <Link to='/'>
                                        <img src={`${process.env.REACT_APP_AWS_BUCKET_PUBLIC}/kwm_web/logo-keepwell.png`} alt="" />
                                    </Link>
                                </Col>
                                <Col className="collapse-close" xs="6">
                                    <button
                                        className="navbar-toggler"
                                        type="button"
                                        onClick={toggleCollapse}
                                    >
                                        <span />
                                        <span />
                                    </button>
                                </Col>
                            </Row>
                        </div>

                        {/* Navigation */}
                        <Nav navbar className='mx-auto'>
                            <NavItem>
                                <NavLink
                                    to={'/home'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="header2-link-active"
                                >
                                    Home
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/about'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="header2-link-active"
                                >
                                    My Journey
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/fitness-stars'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="header2-link-active"
                                >
                                    Fitness Stars
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/fitness-makeover'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="header2-link-active"
                                >
                                    Fitness Makeover
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/fitness-challenges'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="header2-link-active"
                                >
                                    Fitness Challenges
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    to={'/contact'}
                                    tag={NavLinkRRD}
                                    onClick={closeCollapse}
                                    activeClassName="header2-link-active"
                                >
                                    Contact Me
                                </NavLink>
                            </NavItem>
                            {
                                _id ?
                                    <NavItem>
                                        <NavLink
                                            to={'/dashboard'}
                                            tag={NavLinkRRD}
                                            onClick={closeCollapse}
                                            activeClassName="header2-link-active"
                                        >
                                            Dashboard
                                        </NavLink>
                                    </NavItem>
                                    :
                                    <React.Fragment>
                                        <NavItem>
                                            <NavLink
                                                to={'/login'}
                                                tag={NavLinkRRD}
                                                onClick={closeCollapse}
                                                activeClassName="header2-link-active"
                                            >
                                                Login
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                to={'/register'}
                                                tag={NavLinkRRD}
                                                onClick={closeCollapse}
                                                activeClassName="header2-link-active"
                                            >
                                                Register
                                            </NavLink>
                                        </NavItem>
                                    </React.Fragment>
                            }
                        </Nav>

                    </Collapse>

                </Container>
            </Navbar>
            <Ticker speed={3}>
				{() => (
					<h3
						className='d-inline pointer' style={{ whiteSpace: "nowrap" }}
						onClick={() => _id ? history.push('/dashboard') : history.push('/register')}
					>
						{home_headline.map(e => <span>{e}&emsp;·&emsp;</span>)}
					</h3>
				)}
			</Ticker>
        </header>
    )
}