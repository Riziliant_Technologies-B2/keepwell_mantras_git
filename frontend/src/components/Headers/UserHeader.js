import React from "react";
import { useSelector } from "react-redux";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

const UserHeader = () => {

	const { first_name } = useSelector(st => st.user);

	return (
		<>
			<div
				className="header pb-5 pt-sm-8 d-flex align-items-center"
				style={{
					minHeight: "450px",
					backgroundImage: "url(https://exerciseprofile.s3.eu-west-2.amazonaws.com/kwm_web/profile-cover.jpg)",
					backgroundSize: "cover",
					backgroundPosition: "center top",
					paddingTop: 230
				}}
			>
				{/* Mask */}
				<span className="mask bg-gradient-default opacity-8" />
				{/* Header container */}
				<Container className="d-flex align-items-center" fluid>
					<Row>
						<Col lg="7" md="10">
							<h1 className="display-2 text-white">Hello {first_name} </h1>
							<p className="text-white mt-0 mb-5">
								This is your profile page. You can update the progress you've made
								with your work here.
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		</>
	);
};

export default UserHeader;
